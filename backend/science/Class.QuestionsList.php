<?php


class QuestionsList{


	private static function buildTree(array $elements, $parentId = 0) {
		$branch = array();

		foreach ($elements as $element) {
			if ($element['parent_id'] == $parentId) {
				$children = self::buildTree($elements, $element['question_id']);
				if ($children) {
					$children_name = ($children[0]['type_id'] == '5') ? 'options' : 'items';
					$element[$children_name] = $children;
				}
				$branch[] = $element;
			}
		}
		return $branch;
	}




	private static function normalize(array $data){

		$result = array();

		foreach($data as $item){
			$result["{$item['section_id']}"] = array(
				'name' => $item['section_name'],
				'description' => $item['section_description'],
				//'description' => $item['description'],
				'questions' => array()
			);
		}
		$tree = self::buildTree($data);
		foreach($tree as $item){
			$result[$item['section_id']]['questions']["{$item['question_id']}"] = $item;
		}
		return $result;
	}

	public static function getSections(PDO $db, $period_id = null, $normalize = true){
		if ($period_id == null || $period_id == 'current'){
			$period_var = '(SELECT id FROM science_periods WHERE NOW() BETWEEN start_date AND finish_date)';
			$period_arr = array();
		}else{
			$period_var = ':period_id';
			$period_arr = array(
				':period_id' => $period_id
			);
		}
		$q_get_sections = 'SELECT DISTINCT section_id, section_max_points, section_description, section_name FROM view_science WHERE period_id = ' . $period_var . ' ORDER BY question_id';
		$p_get_sections = $db->prepare($q_get_sections);
		$p_get_sections->execute($period_arr);
		if ($p_get_sections === FALSE) throw new DBQueryException('SCIENCE_LIST_ERROR', $db);
		if ($normalize){
			$result = array();
			$arr = $p_get_sections->fetchAll();
			foreach($arr as $section){
				$result[$section['section_id']] = $section;
			}
		}else{
			$result = $p_get_sections->fetchAll();
		}
		return new Result(true,'', $result);


	}

	public static function getFullList(PDO $db, $period_id = null, $normalize = true){
		if ($period_id == null || $period_id == 'current'){
			$period_var = '(SELECT id FROM science_periods WHERE NOW() BETWEEN start_date AND finish_date)';
			$period_arr = array();
		}else{
			$period_var = ':period_id';
			$period_arr = array(
				':period_id' => $period_id
			);
		}
		$p_get_questions = $db->prepare('SELECT * FROM view_science WHERE period_id = ' . $period_var . ' ORDER BY question_id ');
		$p_get_questions->execute($period_arr);
		if ($p_get_questions === FALSE) throw new DBQueryException('SCIENCE_LIST_ERROR', $db);
		$result = $normalize ? self::normalize($p_get_questions->fetchAll()) : $p_get_questions->fetchAll();
		return new Result(true,'', $result);

	}
}