<?php 
 $CFG=$GLOBALS['CFG']=array (
  'installed' => true,
  'debug_output' => 1,
  'irb_connection_type' => 'iserver64',
  'irb64_arm' => 'R',
  'irb64_host' => '192.168.6.20',
  'irb64_port' => '6666',
  'irb64_user' => '1',
  'irb64_password' => '1',
  'irb64_timeout' => 90,
  'irb64_format_base' => 'IBIS',
  'id' => '126',
  'mysql_host' => 'localhost',
  'mysql_user' => 'root',
  'mysql_password' => 'jirbis2',
  'mysql_database' => 'jirbis2',
  'mysql_database_prefix' => 'jos_',
  'mysql_single_wait_time' => 30,
  'update_password' => '',
  'update_login' => '',
  'max_results_renew_time' => 10000,
  'min_results_renew_time' => 5000,
  'sleep_time' => 100000,
  'log_file_path' => 'log.log',
  'max_process_count' => 5,
  'max_process_live_time' => 5000000,
  'max_broad_live_time' => 60000000,
  'max_sorted_records' => 300,
  'max_result_for_printing' => 2000,
  'optimal_records_for_process' => 5,
  'bad_connection_pause' => 3,
  'bad_connections_for_mail' => 5,
  'bad_connections_mailing_divisor' => 10,
  'cache_relevance_time' => 100,
  'default_timeout' => 200,
  'error_log' => 'jirb2_php_errors.log',
  'min_req_length' => 1,
  'keyup_delay' => 1,
  'permanent_search' => 1,
  'permanent_output' => 1,
  'jsession_lock_wait_timeout' => 100,
  'mail_admin' => 'library@guu.ru',
  'mail_enable' => true,
  'mail_from' => 'library@guu.ru',
  'mail_user_name' => '',
  'mail_password' => '',
  'mail_host' => 'localhost',
  'mail_port' => '25',
  'mail_mailer' => 'smtp',
  'cover_width' => 152,
  'cover_height' => 196,
  'covers_download_enable' => true,
  'ji_path_htdocs_local' => 'C:/jirbis2_server/htdocs',
  'ji_dir_jirbis' => 'jirbis2',
  'ji_path_pft' => 'C:/IRBIS64/DATAI/deposit',
  'update_timeout' => 600,
  'ed_net_access' => 'free',
  'ed_net_access_record_priority' => true,
  'ed_external_same' => true,
  'ed_local_access_record_priority' => true,
  'ed_local_users_mask' => '192.168.*.*',
  'ed_local_access' => 'auth',
  'ed_static_name' => 'Электронная версия ',
  'ed_ignore_covers' => true,
  'ed_path_type' => 'prf',
  'ed_path' => 'C:/irbiswrk',
  'ed_manipulators_categories' => 'сотрудник',
  'ed_avalable_extensions' => '*.html; *.txt; *.pdf; *.zip; *.rar; *.doc; *.rtf; *.ppt; *.jpg; *.gif; *.jpeg; *.png; *.bmp; *.ppsx; *.pptx; *.pptm',
  'ed_upload_file_limit' => 100000000,
  'ed_pdf_view_enable' => true,
  'ed_pdf_view_text_enable' => true,
  'ed_pdf_view_password' => '',
  'ed_pdf_view_only_enable' => false,
  'order_max_count' => 40,
  'order_debtor_deny' => 0,
  'books_comments_enable' => true,
  'books_raeting_enable' => true,
  'print_ko_special_enable' => true,
  'ko_search_vuz_enable' => true,
  'resources_cache_time' => 36000,
  'show_libraries' => false,
  'show_full_lib_name' => true,
  'port_defaults' => 
  array (
    'iwi_ex' => 80,
    'iwi' => 80,
    'iserver64' => 6666,
  ),
  'variables_not_for_saving' => 
  array (
    0 => 'req_using',
    1 => 'req_source',
  ),
  'variables_only_for_request' => 
  array (
    0 => 'selected_search_flag',
    1 => 'portion',
    2 => 'portion_output',
    3 => 'portion_output_normalized',
    4 => 'first_number',
    5 => 'last_number',
    6 => 'results_pages_changes',
    7 => 'first_number_normalized',
  ),
  'file_formats_names_and_icons' => 
  array (
    'doc' => 
    array (
      'title' => 'Документы Microsoft Word',
      'image' => 'doc.gif',
    ),
    'docx' => 
    array (
      'title' => 'Документы Microsoft Word 2007',
      'image' => 'doc.gif',
    ),
    'docm' => 
    array (
      'title' => 'Документы Microsoft Word  2007 с макросами',
      'image' => 'doc.gif',
    ),
    'rtf' => 
    array (
      'title' => 'Документы Rich Text Format',
      'image' => 'doc.gif',
    ),
    'htm' => 
    array (
      'title' => 'Документы HTML',
      'image' => 'html.gif',
    ),
    'html' => 
    array (
      'title' => 'Документы HTML',
      'image' => 'html.gif',
    ),
    'pdf' => 
    array (
      'title' => 'Документы Adobe Acrobat Reader',
      'image' => 'pdf.gif',
    ),
    'txt' => 
    array (
      'title' => 'Текстовые документы без форматирования',
      'image' => 'txt.gif',
    ),
    'rar' => 
    array (
      'title' => 'Архивы RAR',
      'image' => 'rar.gif',
    ),
    'zip' => 
    array (
      'title' => 'Архивы ZIP',
      'image' => 'zip.gif',
    ),
    'ppt' => 
    array (
      'title' => 'Презентации Power Point',
      'image' => 'ppt.gif',
    ),
    'other' => 
    array (
      'title' => 'Неизвестный формат',
      'image' => 'other_doc.gif',
    ),
  ),
  'bo_types_names' => 
  array (
    0 => 'full',
    1 => 'brief',
    2 => 'info',
  ),
  'rec_view_profiles' => 
  array (
    'full' => 
    array (
      'description' => 'Полный',
      'formats_profile' => 
      array (
        'document_form' => 
        array (
          'format' => '@jdocument_form',
          'type' => 'bo',
        ),
        'place_code' => 
        array (
          'format' => '@jplace_code',
          'type' => 'bo',
        ),
        'full' => 
        array (
          'format' => '@jfull',
          'type' => 'bo',
        ),
        'access_points' => 
        array (
          'format' => '@jaccess_points',
          'type' => 'bo',
        ),
        'udk_bbk' => 
        array (
          'format' => '@judk_bbk',
          'type' => 'bo',
        ),
        'keywords' => 
        array (
          'format' => '@jkeywords',
          'type' => 'bo',
        ),
        'rubrics' => 
        array (
          'format' => '@jrubrics',
          'type' => 'bo',
        ),
        'annotation' => 
        array (
          'format' => '@jannotation',
          'type' => 'bo',
        ),
        'contents' => 
        array (
          'format' => '@jcontents',
          'type' => 'bo',
        ),
        'ed' => 
        array (
          'format' => '@jed',
          'type' => 'bo',
        ),
        'ko' => 
        array (
          'format' => '@jko',
          'type' => 'bo',
        ),
        'owner' => 
        array (
          'format' => '@jowner',
          'type' => 'bo',
        ),
      ),
      'presentation_profile' => 
      array (
        'document_form' => 
        array (
          'title' => 'Вид документа',
          'type' => 0,
          'indicators' => 
          array (
            0 => 200,
            1 => 451,
          ),
          'request' => 'task=show_quick_format&format=jdocument_form',
        ),
        'place_code' => 
        array (
          'title' => 'Полочный шифр',
          'type' => 1,
          'indicators' => 
          array (
            0 => 906,
            1 => 908,
            2 => 903,
          ),
          'request' => 'task=show_quick_format&format=jplace_code',
        ),
        'full' => 
        array (
          'title' => 'Библиографическое описание',
          'type' => 0,
          'indicators' => 
          array (
            0 => 200,
            1 => 451,
          ),
          'request' => 'task=show_quick_format&format=jfull',
        ),
        'udk_bbk' => 
        array (
          'title' => 'Систематические индексы',
          'type' => 1,
          'indicators' => 
          array (
            0 => 621,
            1 => 675,
          ),
          'request' => 'task=show_quick_format&format=judk_bbk',
        ),
        'rubrics' => 
        array (
          'title' => 'Рубрики',
          'type' => 1,
          'indicators' => 
          array (
            0 => 606,
            1 => 607,
          ),
          'request' => 'task=show_quick_format&format=jrubrics',
        ),
        'keywords' => 
        array (
          'title' => 'Ключевые слова',
          'type' => 1,
          'indicators' => 
          array (
            0 => 610,
          ),
          'request' => 'task=show_quick_format&format=jkeywords',
        ),
        'access_points' => 
        array (
          'title' => 'Точки доступа',
          'type' => 1,
          'indicators' => 
          array (
            0 => 701,
            1 => 702,
            2 => 925,
            3 => 922,
            4 => 961,
            5 => 509,
            6 => 972,
            7 => 754,
          ),
          'request' => 'task=show_quick_format&format=jaccess_points',
        ),
        'annotation' => 
        array (
          'title' => 'Аннотация',
          'type' => 1,
          'indicators' => 
          array (
          ),
          'request' => 'task=show_quick_format&format=jannotation',
        ),
        'contents' => 
        array (
          'title' => 'Оглавление',
          'type' => 1,
          'indicators' => 
          array (
            0 => 330,
            1 => 922,
          ),
          'request' => 'task=show_quick_format&format=jcontents',
        ),
        'ko' => 
        array (
          'title' => 'Книгообеспеченность',
          'type' => 1,
          'indicators' => 
          array (
            0 => 691,
          ),
          'request' => 'task=show_quick_format&format=jko',
        ),
        'ko_disc' => 
        array (
          'title' => 'Учебное назначение',
          'type' => 2,
          'indicators' => 
          array (
            0 => 691,
          ),
          'request' => 'task=show_quick_format&format=jko_disc',
        ),
        'exemp' => 
        array (
          'title' => 'Экземпляры и бронирование',
          'type' => 2,
          'indicators' => 
          array (
            0 => 910,
            1 => 933,
          ),
          'request' => 'task=show_exemp',
        ),
        'ed' => 
        array (
          'title' => 'Электронные версии',
          'type' => 2,
          'indicators' => 
          array (
            0 => 951,
            1 => 952,
          ),
          'request' => 'task=ed',
        ),
        'numbers' => 
        array (
          'title' => 'Зарегистрированные поступления',
          'type' => 2,
          'indicators' => 
          array (
            0 => 909,
          ),
          'request' => 'task=show_quick_format&format=jnumbers',
        ),
        'owner' => 
        array (
          'title' => 'Держатели документа',
          'type' => 2,
          'indicators' => 
          array (
            0 => 902,
          ),
          'request' => 'task=show_owners',
        ),
      ),
    ),
    'brief' => 
    array (
      'description' => 'Краткий',
      'formats_profile' => 
      array (
        'document_form' => 
        array (
          'format' => '@jdocument_form',
          'type' => 'bo',
        ),
        'place_code' => 
        array (
          'format' => '@jplace_code',
          'type' => 'bo',
        ),
        'brief' => 
        array (
          'format' => '@jbrief',
          'type' => 'bo',
        ),
        'access_points' => 
        array (
          'format' => '@jaccess_points',
          'type' => 'bo',
        ),
        'udk_bbk' => 
        array (
          'format' => '@judk_bbk',
          'type' => 'bo',
        ),
        'keywords' => 
        array (
          'format' => '@jkeywords',
          'type' => 'bo',
        ),
        'rubrics' => 
        array (
          'format' => '@jrubrics',
          'type' => 'bo',
        ),
        'annotation' => 
        array (
          'format' => '@jannotation',
          'type' => 'bo',
        ),
        'contents' => 
        array (
          'format' => '@jcontents',
          'type' => 'bo',
        ),
        'ed' => 
        array (
          'format' => '@jed',
          'type' => 'bo',
        ),
        'ko' => 
        array (
          'format' => '@jko',
          'type' => 'bo',
        ),
        'owner' => 
        array (
          'format' => '@jowner',
          'type' => 'bo',
        ),
        'licence' => 
        array (
          'format' => '@jlicence',
          'type' => 'bo',
        ),
      ),
      'presentation_profile' => 
      array (
        'document_form' => 
        array (
          'title' => 'Вид документа',
          'type' => 0,
          'indicators' => 
          array (
            0 => 200,
            1 => 451,
          ),
          'request' => 'task=show_quick_format&format=jdocument_form',
        ),
        'place_code' => 
        array (
          'title' => 'Полочный шифр',
          'type' => 1,
          'indicators' => 
          array (
            0 => 906,
            1 => 908,
            2 => 903,
          ),
          'request' => 'task=show_quick_format&format=jplace_code',
        ),
        'brief' => 
        array (
          'title' => 'Библиографическое описание',
          'type' => 0,
          'indicators' => 
          array (
            0 => 200,
            1 => 461,
          ),
          'request' => 'task=show_quick_format&format=jfull',
        ),
        'udk_bbk' => 
        array (
          'title' => 'Систематические индексы',
          'type' => 1,
          'indicators' => 
          array (
            0 => 621,
            1 => 675,
          ),
          'request' => 'task=show_quick_format&format=judk_bbk',
        ),
        'rubrics' => 
        array (
          'title' => 'Рубрики',
          'type' => 1,
          'indicators' => 
          array (
            0 => 606,
            1 => 607,
          ),
          'request' => 'task=show_quick_format&format=jrubrics',
        ),
        'keywords' => 
        array (
          'title' => 'Ключевые слова',
          'type' => 1,
          'indicators' => 
          array (
            0 => 610,
          ),
          'request' => 'task=show_quick_format&format=jkeywords',
        ),
        'access_points' => 
        array (
          'title' => 'Точки доступа',
          'type' => 1,
          'indicators' => 
          array (
            0 => 701,
            1 => 702,
            2 => 925,
            3 => 922,
            4 => 961,
            5 => 509,
            6 => 972,
            7 => 754,
          ),
          'request' => 'task=show_quick_format&format=jaccess_points',
        ),
        'annotation' => 
        array (
          'title' => 'Аннотация',
          'type' => 1,
          'indicators' => 
          array (
          ),
          'request' => 'task=show_quick_format&format=jannotation',
        ),
        'contents' => 
        array (
          'title' => 'Оглавление',
          'type' => 1,
          'indicators' => 
          array (
            0 => 330,
            1 => 922,
          ),
          'request' => 'task=show_quick_format&format=jcontents',
        ),
        'ko' => 
        array (
          'title' => 'Книгообеспеченность',
          'type' => 1,
          'indicators' => 
          array (
            0 => 691,
          ),
          'request' => 'task=show_quick_format&format=jko',
        ),
        'ko_disc' => 
        array (
          'title' => 'Учебное назначение',
          'type' => 2,
          'indicators' => 
          array (
            0 => 691,
          ),
          'request' => 'task=show_quick_format&format=jko_disc',
        ),
        'exemp' => 
        array (
          'title' => 'Экземпляры и бронирование',
          'type' => 2,
          'indicators' => 
          array (
            0 => 910,
            1 => 933,
          ),
          'request' => 'task=show_exemp',
        ),
        'ed' => 
        array (
          'title' => 'Электронные версии',
          'type' => 2,
          'indicators' => 
          array (
            0 => 951,
            1 => 952,
          ),
          'request' => 'task=ed',
        ),
        'numbers' => 
        array (
          'title' => 'Зарегистрированные поступления',
          'type' => 2,
          'indicators' => 
          array (
            0 => 909,
          ),
          'request' => 'task=show_quick_format&format=jnumbers',
        ),
        'owner' => 
        array (
          'title' => 'Держатели документа',
          'type' => 2,
          'indicators' => 
          array (
            0 => 902,
          ),
          'request' => 'task=show_owners',
        ),
      ),
    ),
    'info' => 
    array (
      'description' => 'С пояснениями',
      'formats_profile' => 
      array (
        'document_form' => 
        array (
          'format' => '@jdocument_form',
          'type' => 'bo',
        ),
        'place_code' => 
        array (
          'format' => '@jplace_code',
          'type' => 'bo',
        ),
        'info' => 
        array (
          'format' => '@jinfo',
          'type' => 'bo',
        ),
        'access_points' => 
        array (
          'format' => '@jaccess_points',
          'type' => 'bo',
        ),
        'udk_bbk' => 
        array (
          'format' => '@judk_bbk',
          'type' => 'bo',
        ),
        'keywords' => 
        array (
          'format' => '@jkeywords',
          'type' => 'bo',
        ),
        'rubrics' => 
        array (
          'format' => '@jrubrics',
          'type' => 'bo',
        ),
        'annotation' => 
        array (
          'format' => '@jannotation',
          'type' => 'bo',
        ),
        'contents' => 
        array (
          'format' => '@jcontents',
          'type' => 'bo',
        ),
        'ed' => 
        array (
          'format' => '@jed',
          'type' => 'bo',
        ),
        'ko' => 
        array (
          'format' => '@jko',
          'type' => 'bo',
        ),
        'owner' => 
        array (
          'format' => '@jowner',
          'type' => 'bo',
        ),
        'licence' => 
        array (
          'format' => '@jlicence',
          'type' => 'bo',
        ),
      ),
      'presentation_profile' => 
      array (
        'document_form' => 
        array (
          'title' => 'Вид документа',
          'type' => 0,
          'indicators' => 
          array (
            0 => 200,
            1 => 451,
          ),
          'request' => 'task=show_quick_format&format=jdocument_form',
        ),
        'place_code' => 
        array (
          'title' => 'Полочный шифр',
          'type' => 1,
          'indicators' => 
          array (
            0 => 906,
            1 => 908,
            2 => 903,
          ),
          'request' => 'task=show_quick_format&format=jplace_code',
        ),
        'info' => 
        array (
          'title' => 'Библиографическое описание',
          'type' => 0,
          'indicators' => 
          array (
            0 => 200,
            1 => 451,
          ),
          'request' => 'task=show_quick_format&format=jfull',
        ),
        'udk_bbk' => 
        array (
          'title' => 'Систематические индексы',
          'type' => 1,
          'indicators' => 
          array (
            0 => 621,
            1 => 675,
          ),
          'request' => 'task=show_quick_format&format=judk_bbk',
        ),
        'rubrics' => 
        array (
          'title' => 'Рубрики',
          'type' => 1,
          'indicators' => 
          array (
            0 => 606,
            1 => 607,
          ),
          'request' => 'task=show_quick_format&format=jrubrics',
        ),
        'keywords' => 
        array (
          'title' => 'Ключевые слова',
          'type' => 1,
          'indicators' => 
          array (
            0 => 610,
          ),
          'request' => 'task=show_quick_format&format=jkeywords',
        ),
        'access_points' => 
        array (
          'title' => 'Точки доступа',
          'type' => 1,
          'indicators' => 
          array (
            0 => 701,
            1 => 702,
            2 => 925,
            3 => 922,
            4 => 961,
            5 => 509,
            6 => 972,
            7 => 754,
          ),
          'request' => 'task=show_quick_format&format=jaccess_points',
        ),
        'annotation' => 
        array (
          'title' => 'Аннотация',
          'type' => 1,
          'indicators' => 
          array (
          ),
          'request' => 'task=show_quick_format&format=jannotation',
        ),
        'contents' => 
        array (
          'title' => 'Оглавление',
          'type' => 1,
          'indicators' => 
          array (
            0 => 330,
            1 => 922,
          ),
          'request' => 'task=show_quick_format&format=jcontents',
        ),
        'ko' => 
        array (
          'title' => 'Книгообеспеченность',
          'type' => 1,
          'indicators' => 
          array (
            0 => 691,
          ),
          'request' => 'task=show_quick_format&format=jko',
        ),
        'ko_disc' => 
        array (
          'title' => 'Учебное назначение',
          'type' => 2,
          'indicators' => 
          array (
            0 => 691,
          ),
          'request' => 'task=show_quick_format&format=jko_disc',
        ),
        'exemp' => 
        array (
          'title' => 'Экземпляры и бронирование',
          'type' => 2,
          'indicators' => 
          array (
            0 => 910,
            1 => 933,
          ),
          'request' => 'task=show_exemp',
        ),
        'ed' => 
        array (
          'title' => 'Электронные версии',
          'type' => 2,
          'indicators' => 
          array (
            0 => 951,
            1 => 952,
          ),
          'request' => 'task=ed',
        ),
        'numbers' => 
        array (
          'title' => 'Зарегистрированные поступления',
          'type' => 2,
          'indicators' => 
          array (
            0 => 909,
          ),
          'request' => 'task=show_quick_format&format=jnumbers',
        ),
        'owner' => 
        array (
          'title' => 'Держатели документа',
          'type' => 2,
          'indicators' => 
          array (
            0 => 902,
          ),
          'request' => 'task=show_owners',
        ),
      ),
    ),
  ),
  'max_portion_profiles' => 
  array (
    'iwi' => 
    array (
      'seach_only' => 70,
      'full' => 30,
      'brief' => 5,
      'sort_avhead' => 1,
      'sort_year' => 0.1,
      'sort_dp' => 0.1,
    ),
    'iserver64' => 
    array (
      'seach_only' => 1000,
      'full' => 900,
      'brief' => 50,
      'sort_avhead' => 10,
      'sort_year' => 10,
      'sort_dp' => 10,
    ),
    'iz39' => 
    array (
      'seach_only' => 40,
      'full' => 30,
      'brief' => 5,
      'sort_avhead' => 1,
      'sort_year' => 0.1,
      'sort_dp' => 0.1,
    ),
  ),
  'sort_types' => 
  array (
    'author' => 
    array (
      'description' => 'По автору и заглавию',
      'format' => '@jsort_author',
      'type' => 'sort',
    ),
    'year' => 
    array (
      'description' => 'По году издания',
      'format' => '@jsort_year',
      'type' => 'sort',
    ),
    'arrival_date' => 
    array (
      'description' => 'По дате поступления',
      'format' => '@jsort_arrival_date',
      'type' => 'sort',
    ),
  ),
  'forms_profile' => 
  array (
    'easy' => 
    array (
      'main' => 'easy_default',
      'search' => 'easy_default',
      'description' => 'Простой поиск',
      'reduce_afrer_search' => false,
    ),
    'easy_all' => 
    array (
      'main' => 'easy_all',
      'search' => 'easy_all',
      'description' => 'Простой поиск по свободному запросу',
      'reduce_afrer_search' => false,
    ),
    'easy_fulltext' => 
    array (
      'main' => 'easy_fulltext',
      'search' => 'easy_fulltext',
      'description' => 'Простой поиск по полному тексту',
      'reduce_afrer_search' => false,
    ),
    'extended' => 
    array (
      'main' => 'extended_default',
      'search' => 'easy_default',
      'description' => 'Расширенный поиск',
      'reduce_afrer_search' => false,
    ),
    'extended_fulltext' => 
    array (
      'main' => 'extended_fulltext',
      'search' => 'easy_fulltext',
      'description' => 'Расширенный поиск по полному тексту',
      'reduce_afrer_search' => false,
    ),
    'prof' => 
    array (
      'main' => 'professional_default',
      'search' => 'easy_default',
      'description' => 'Профессиональный поиск',
      'reduce_afrer_search' => false,
    ),
    'subject' => 
    array (
      'main' => 'subject_default',
      'search' => 'subject_default',
      'description' => 'Новые поступления',
      'reduce_afrer_search' => true,
    ),
    'grnti' => 
    array (
      'main' => 'grnti_default',
      'search' => 'grnti_default',
      'description' => 'Поиск по ГРНТИ',
      'reduce_afrer_search' => false,
    ),
    'bbk' => 
    array (
      'main' => 'bbk_default',
      'search' => 'bbk_default',
      'description' => 'Поиск по ББК',
      'reduce_afrer_search' => false,
    ),
    'mesh' => 
    array (
      'main' => 'mesh_default',
      'search' => 'mesh_default',
      'description' => 'Поиск по MESH',
      'reduce_afrer_search' => false,
    ),
    'udk' => 
    array (
      'main' => 'udk_default',
      'search' => 'udk_default',
      'description' => 'Поиск по УДК',
      'reduce_afrer_search' => false,
    ),
    'ko' => 
    array (
      'main' => 'ko_default',
      'search' => 'ko_default',
      'description' => 'Отбор по учебному назначению',
      'reduce_afrer_search' => false,
    ),
  ),
  'fp' => 
  array (
    'author' => 
    array (
      'title' => 'Автор',
      'slave' => false,
      'prefix' => 'A=',
      'logic' => '*',
      'break_on_parts' => true,
      'truncation' => true,
      'morfology' => 'morfology',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'author',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'person' => 
    array (
      'title' => 'Персоналия',
      'slave' => false,
      'prefix' => 'P=',
      'logic' => '*',
      'break_on_parts' => true,
      'truncation' => true,
      'morfology' => 'morfology',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'author',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'title' => 
    array (
      'title' => 'Заглавие',
      'slave' => false,
      'prefix' => 'T=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'publishing' => 
    array (
      'title' => 'Издательство',
      'slave' => false,
      'prefix' => 'O=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'owner' => 
    array (
      'title' => 'Держатель документа',
      'slave' => false,
      'prefix' => 'XX=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'keywords' => 
    array (
      'title' => 'Ключевые слова',
      'slave' => false,
      'prefix' => 'K=',
      'logic' => '*',
      'break_on_parts' => true,
      'truncation' => false,
      'morfology' => 'morfology',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'keywords',
      'field_type' => '',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'keywords_cvalif' => 
    array (
      'title' => 'Квалификаторы поля Ключевые слова',
      'slave' => true,
      'prefix' => '',
      'logic' => '',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'isbn' => 
    array (
      'title' => 'ISBN',
      'slave' => false,
      'prefix' => 'B=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'isbn',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'year1' => 
    array (
      'title' => 'Год издания c:',
      'slave' => false,
      'prefix' => 'G=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'year1',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => true,
      'dic_function' => '',
    ),
    'year2' => 
    array (
      'title' => 'Год издания по:',
      'slave' => true,
      'prefix' => 'G=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'year1',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => true,
      'dic_function' => '',
    ),
    'grnti' => 
    array (
      'title' => 'ГРНТИ индекс',
      'slave' => false,
      'prefix' => 'R=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'grnti_multiselect' => 
    array (
      'title' => 'ГРНТИ индексы',
      'slave' => false,
      'prefix' => 'R=',
      'logic' => '+',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'grnti_multiselect',
      'field_type' => '',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'subject' => 
    array (
      'title' => 'Предмет',
      'slave' => false,
      'prefix' => 'SUBJ=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'ed' => 
    array (
      'title' => 'Электронная версия',
      'slave' => false,
      'prefix' => 'TEK=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'ed',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'all' => 
    array (
      'title' => 'Все данные',
      'slave' => false,
      'prefix' => 'DS=',
      'logic' => '*',
      'break_on_parts' => true,
      'truncation' => true,
      'morfology' => 'morfology',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'keywords',
      'field_type' => '',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'disc' => 
    array (
      'title' => 'Дисциплина',
      'slave' => false,
      'prefix' => 'DISC=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_disc',
    ),
    'disc_multiselect' => 
    array (
      'title' => 'Дисциплины',
      'slave' => false,
      'prefix' => 'DISC=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => 'disc_multiselect',
      'field_type' => 'multiselect',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_disc_multiselect',
    ),
    'spec' => 
    array (
      'title' => 'Специальность',
      'slave' => false,
      'prefix' => 'SPEC=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_spec',
    ),
    'purpose' => 
    array (
      'title' => 'Учебная роль',
      'slave' => false,
      'prefix' => 'TL=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_purpose',
    ),
    'sem' => 
    array (
      'title' => 'Семестр',
      'slave' => false,
      'prefix' => 'SEM=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => true,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_sem',
    ),
    'kafch' => 
    array (
      'title' => 'Читающая кафедра',
      'slave' => false,
      'prefix' => 'KAFCH=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_kafch',
    ),
    'vo' => 
    array (
      'title' => 'Уровень подготовки',
      'slave' => false,
      'prefix' => 'VO=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_vo',
    ),
    'title_article' => 
    array (
      'title' => 'Название статьи',
      'slave' => false,
      'prefix' => 'TI=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'title_periodical' => 
    array (
      'title' => 'Название журнала',
      'slave' => false,
      'prefix' => 'TJ=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'issue_periodical' => 
    array (
      'title' => 'Выпуск',
      'slave' => false,
      'prefix' => 'NM=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => true,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'value_periodical' => 
    array (
      'title' => 'Том',
      'slave' => false,
      'prefix' => 'NV=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => true,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'document_form' => 
    array (
      'title' => 'Вид документа',
      'slave' => false,
      'prefix' => 'V=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_document_form',
    ),
    'document_type' => 
    array (
      'title' => 'Характер документа',
      'slave' => false,
      'prefix' => 'HD=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_document_type',
    ),
    'sigla' => 
    array (
      'title' => 'Сигла держателя документа',
      'slave' => false,
      'prefix' => 'X=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'author_z' => 
    array (
      'title' => 'Автор',
      'slave' => false,
      'prefix' => 'A=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'keywords_z' => 
    array (
      'title' => 'Ключевые слова',
      'slave' => false,
      'prefix' => 'K=',
      'logic' => '*',
      'break_on_parts' => true,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => true,
      'number_extraction' => false,
      'function' => '',
      'field_type' => '',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
    'work_place' => 
    array (
      'title' => 'Место работы',
      'slave' => false,
      'prefix' => 'MR=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'preload',
    ),
    'document_language' => 
    array (
      'title' => 'Язык документа',
      'slave' => false,
      'prefix' => 'J=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => false,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'combobox',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => 'get_document_language',
    ),
    'subject_rubric' => 
    array (
      'title' => 'Предметная рубрика',
      'slave' => false,
      'prefix' => 'S=',
      'logic' => '*',
      'break_on_parts' => false,
      'truncation' => true,
      'morfology' => '',
      'word_extraction' => false,
      'number_extraction' => false,
      'function' => '',
      'field_type' => 'autocomplete',
      'dic_values' => 
      array (
      ),
      'dic_only' => false,
      'dic_function' => '',
    ),
  ),
  'show_order' => true,
  'show_free_ex' => true,
  'show_total_ex' => true,
  'show_only_avalable_exemp' => false,
  'user_levels_table' => 
  array (
    0 => 
    array (
      'irbis' => 'школьник',
      'joomla' => 2,
    ),
    1 => 
    array (
      'irbis' => 'студент',
      'joomla' => 2,
    ),
    2 => 
    array (
      'irbis' => 'аспирант',
      'joomla' => 2,
    ),
    3 => 
    array (
      'irbis' => 'стажер',
      'joomla' => 2,
    ),
    4 => 
    array (
      'irbis' => 'преподаватель',
      'joomla' => 2,
    ),
    5 => 
    array (
      'irbis' => 'ВП',
      'joomla' => 2,
    ),
    6 => 
    array (
      'irbis' => 'СБ',
      'joomla' => 6,
    ),
    7 => 
    array (
      'irbis' => 'сотрудник',
      'joomla' => 6,
    ),
  ),
  'user_password_tag' => 30,
  'search_fali_link' => '<a target=\\"_blank\\" href=\\"http://ya.ru\\">Выполнить поиск в Yandex</a>',
  'frontend_theme' => 'smoothness',
  'adm_panel_theme' => 'smoothness',
  'like_vkontakte_id' => '',
  'like_twiter_id' => '',
  'like_facebook_id' => '',
  'svk_enable' => false,
  'svk_conditions' => 
  array (
    'year' => 
    array (
      'title' => 'Год издания',
      'full' => 20,
      'partial' => 0,
      'no' => -100,
      'mistake' => 0,
      'function' => 'year',
    ),
    'isbn' => 
    array (
      'title' => 'ISBN',
      'full' => 80,
      'partial' => 20,
      'no' => -100,
      'mistake' => 256,
      'function' => 'isbn',
    ),
    'ws' => 
    array (
      'title' => 'Рабочий лист',
      'full' => 1,
      'partial' => 0,
      'no' => -100,
      'mistake' => 0,
      'function' => 'ws',
    ),
    'value' => 
    array (
      'title' => 'Том',
      'full' => 10,
      'partial' => 0,
      'no' => -100,
      'mistake' => 0,
      'function' => 'value',
    ),
    'part' => 
    array (
      'title' => 'Часть',
      'full' => 10,
      'partial' => 0,
      'no' => -100,
      'mistake' => 0,
      'function' => 'part',
    ),
    'author' => 
    array (
      'title' => 'Первый автор',
      'full' => 20,
      'partial' => 15,
      'no' => -100,
      'mistake' => 256,
      'function' => 'author',
    ),
    'title' => 
    array (
      'title' => 'Заглавие',
      'full' => 30,
      'partial' => 20,
      'no' => -100,
      'mistake' => 256,
      'function' => 'title',
    ),
    'pages' => 
    array (
      'title' => 'Количество страниц',
      'full' => 20,
      'partial' => 5,
      'no' => -60,
      'mistake' => 5,
      'function' => 'pages',
    ),
  ),
  'svk_merged_fields' => 
  array (
    0 => 
    array (
      'title' => 'Ссылка на ЭД',
      'field' => 951,
      'subfield' => '',
      'operation' => 0,
      'function' => '',
    ),
    1 => 
    array (
      'title' => 'Сигла хранения',
      'field' => 902,
      'subfield' => '',
      'operation' => 0,
      'function' => '',
    ),
  ),
  'svk_threshold' => 84,
  'svk_base' => 'SVK',
  'svk_portion' => 300,
  'svk_quick_mode' => false,
  'rec_filter_useful_fields' => 
  array (
    0 => 10,
    1 => 11,
    2 => 12,
    3 => 19,
    4 => 100,
    5 => 101,
    6 => 102,
    7 => 106,
    8 => 110,
    9 => 115,
    10 => 123,
    11 => 125,
    12 => 126,
    13 => 130,
    14 => 135,
    15 => 140,
    16 => 141,
    17 => 200,
    18 => 205,
    19 => 210,
    20 => 215,
    21 => 225,
    22 => 230,
    23 => 300,
    24 => 314,
    25 => 320,
    26 => 327,
    27 => 328,
    28 => 330,
    29 => 331,
    30 => 337,
    31 => 391,
    32 => 395,
    33 => 396,
    34 => 397,
    35 => 421,
    36 => 422,
    37 => 423,
    38 => 430,
    39 => 440,
    40 => 451,
    41 => 452,
    42 => 454,
    43 => 46,
    44 => 461,
    45 => 463,
    46 => 470,
    47 => 481,
    48 => 488,
    49 => 503,
    50 => 510,
    51 => 517,
    52 => 541,
    53 => 600,
    54 => 601,
    55 => 675,
    56 => 621,
    57 => 60,
    58 => 605,
    59 => 606,
    60 => 607,
    61 => 610,
    62 => 629,
    63 => 686,
    64 => 690,
    65 => 700,
    66 => 701,
    67 => 702,
    68 => 710,
    69 => 711,
    70 => 900,
    71 => 901,
    72 => 903,
    73 => 906,
    74 => 909,
    75 => 911,
    76 => 912,
    77 => 915,
    78 => 916,
    79 => 919,
    80 => 920,
    81 => 922,
    82 => 923,
    83 => 924,
    84 => 925,
    85 => 926,
    86 => 929,
    87 => 930,
    88 => 931,
    89 => 933,
    90 => 934,
    91 => 935,
    92 => 936,
    93 => 937,
    94 => 938,
    95 => 939,
    96 => 950,
    97 => 951,
    98 => 953,
    99 => 961,
    100 => 962,
    101 => 963,
    102 => 964,
    103 => 965,
    104 => 970,
    105 => 971,
    106 => 972,
    107 => 981,
    108 => 982,
    109 => 993,
  ),
  'rec_filter_deleted_subfields' => 
  array (
    700 => 'Y',
    701 => 'Y',
    702 => 'Y',
    961 => 'Y',
    926 => 'Y',
    600 => 'Y',
    601 => 'Y',
    330 => 'N',
  ),
  'joomla_present' => true,
);
 ?>
