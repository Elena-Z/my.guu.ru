<?php
// Здесь должен быть не jirbis_configuration.php, а jirbis_configuration_template.php

$GLOBALS['mode']='install';
require_once( 'jirbis_configuration.php' );
require_once( 'jirbis_link_libraries.php' );
require_once( JI_PATH_MODES_LOCAL.'/install/ji_install.php' );
require_once( JI_PATH_MODES_LOCAL.'/install/ji_install_show.php' );




ini_set('error_log',JI_PATH_DEBUG_LOCAL.'/'.JI_FILE_PHP_ERRORS_LOG);
//Переключить на OFF в рабочем режиме! 
ini_set('display_errors','Off');
ini_set('max_execution_time','100');
ji_ilog::i()->debug(true);


$task=mosGetParam( $_REQUEST, 'task', 'setup' );

$answer=array();
$answer['success']=true;



if ($task==='setup'){

	
	$answer['pft_installed']=false;
	
	$user_id=$CFG['id']=mosGetParam( $_REQUEST, 'id', '' );
	$irb64_host=$CFG['irb64_host']=mosGetParam( $_REQUEST, 'irb64_host', 'localhost' );
	$irb64_port=$CFG['irb64_port']=mosGetParam( $_REQUEST, 'irb64_port', 6666 );
	$irb64_user=$CFG['irb64_user']=mosGetParam( $_REQUEST, 'irb64_user', '1' );
	$irb64_password=$CFG['irb64_password']=mosGetParam( $_REQUEST, 'irb64_password', '1' );
	$irb64_format_base=$CFG['irb64_format_base']=mosGetParam( $_REQUEST, 'irb64_format_base', 'IBIS' );;
	$shot_name=mosGetParam( $_REQUEST, 'shot_name', '' );
	$full_name=mosGetParam( $_REQUEST, 'full_name', '' );
	$email=$CFG['mail_from']=$CFG['mail_admin']=mosGetParam( $_REQUEST, 'email', '' );
	
		
	
	
	/*============================== ШАГ 1==========================================================================================*/
	
	
	
	//SHIFT+DELETE -- удаление строки
	
	define('MHR','mhr.mnu');
	define('MHRKV','mhrkv.mnu');
	define('KV','kv.mnu');
	
	
	/* @var $c jwrapper */
	try{
		//$db= new database( $CFG['mysql_host'], $CFG['mysql_user'], $CFG['mysql_password'], $CFG['mysql_database'], $CFG['mysql_database_prefix'] ,false);
		idb::i()->_debug=false;
		
		$sqlrez = idb::i()->getErrorNum();
		if ($sqlrez <> 0 && $sqlrez <> 1007) 
			ji_ilog::i()->w('Ошибка подключения к MySQL серверу: '.idb::i()->getErrorMsg().' Убедитесь, что сервере MySQL установлен как служба.',I_ERROR,I_ERROR_LEVEL_CRITIICALY,idb::i()->getErrorNum());				
		
		$c=new jwrapper(true);
		$c->Connect();
	
		
		if ($c->is_errors()) 	
			ji_ilog::i()->w('Ошибка доступа к ИРБИС64 TCP/IP серверу: '.$c->get_last_error_message(),I_ERROR,I_ERROR_LEVEL_CRITIICALY,$c->get_last_error_code());
			//print_r($c->connection->cfg);
		
		if (!$c->connection->cfg)
			ji_ilog::i()->w('Не получен irbis_server.ini. Возможно, текущая регистрация не является первой.',I_ERROR,I_ERROR_LEVEL_CRITIICALY,0);
		
		if 	(empty($c->connection->cfg['MAIN']['DBNNAMECAT']))
				ji_ilog::i()->w('В параметре DBNNAMECAT INI файла не указан файл dbnamX.par',I_ERROR,I_ERROR_LEVEL_CRITIICALY,0);		
			
			ji_ilog::i()->w('РЕГИСТРАЦИЯ СЕРВЕРА И БАЗ',I_INFO);	
			//===============РЕГИСТРАЦИЯ СЕРВЕРА И БД=======================================
			$dbnam=u::win_utf($c->GetFile(APATH_DATAI,'',$c->connection->cfg['MAIN']['DBNNAMECAT']));		
			// Всё-таки мы не удаляем технические базы данных, так как в dbnam3.mnu их быть не должно. 
			$dbnam_array=ji_install::get_mnu_txt_as_array($dbnam);
			if (!$dbnam_array)
				ji_ilog::i()->w('Не удалось получить файл dbnamXX.mnu, или он имеет некоректную структуру',I_ERROR,I_ERROR_LEVEL_CRITIICALY);
	
			$bases_req_array=ji_install::get_bases_array_mnu_as_req_array($dbnam_array);					
			if (!ji_install::add_sql_data('#__bases',$bases_req_array,true))
				ji_ilog::i()->w('Ошибка SQL при добавлении названий баз данных: '.idb::i()->getErrorMsg(),I_ERROR,I_ERROR_LEVEL_CRITIICALY,idb::i()->getErrorNum());	
				
					
			$defaultdb=isset($c->connection->cfg['MAIN']['DEFAULTDB']) ?  $c->connection->cfg['MAIN']['DEFAULTDB'] : ji_install::get_first_assoc_array_key($dbnam_array);		
			$CFG['irb64_format_base']=$defaultdb;
				// 	Подразумевается LIB_ID=1
				$server_req_array=array(
					array(
					'connection_type'=>'iserver64',
					'url'=>$irb64_host,
					'port'=>$irb64_port,
					'login'=>$irb64_user,
					'password'=>$irb64_password,
					'full_name'=>$full_name,
					'shot_name'=>$shot_name,
					'email'=>$email,
					'lib_raeting'=>100
					)
				);
			
			if(!ji_install::add_sql_data('#__libraries',$server_req_array,true))
				ji_ilog::i()->w('Ошибка SQL при добавлении данных сервера: '.idb::i()->getErrorMsg(),I_ERROR,I_ERROR_LEVEL_CRITIICALY,idb::i()->getErrorNum());
			
			//\===============РЕГИСТРАЦИЯ СЕРВЕРА И БД======================================
			ji_ilog::i()->w('КОПИРОВАНИЕ ФОРМАТОВ',I_INFO);	
			//===============КОПИРОВАНИЕ ФОРМАТОВ======================================		
			$irbis_server=u::win_utf($c->GetFile(APATH_SYS,'','irbis_server.ini'));		
			$irbis_server_array=u::ini_txt_read($irbis_server);
			
			if (!$irbis_server_array)
				ji_ilog::i()->w('Не удалось получить файл irbis_server.ini',I_ERROR,I_ERROR_LEVEL_CRITIICALY);
			if (empty($irbis_server_array['MAIN']['DATAPATH']))
				ji_ilog::i()->w('Не удалось найти параметр [MAIN][DATAPATH] в irbis_server.ini',I_ERROR,I_ERROR_LEVEL_CRITIICALY);
			
			$deposit_path=u::sla(trim($irbis_server_array['MAIN']['DATAPATH'],'\\')).'/deposit';
			
			if (!file_exists($deposit_path)){
				ji_ilog::i()->w('Директория Deposit не найдена',I_INFO);
			}else{
	
				@file_put_contents($deposit_path.'/ji_install_test.pft','\'TRUE\'');	
				
				$result=$c->RecReadAndFormat($defaultdb, 1, 0, '@ji_install_test');
				if (trim($result)==='TRUE'){
					
					ji_install::copy_pft_and_mnu($deposit_path);				
					$CFG['ji_path_pft']=$deposit_path;
					$answer['pft_installed']=true;
					
				}else{ 			
					ji_ilog::i()->w('Директория Deposit не находится на данном сервере',I_INFO);			
				}
				@unlink($deposit_path.'/ji_install_test.pft');
			}
			
			//\===============КОПИРОВАНИЕ ФОРМАТОВ======================================		
			ji_ilog::i()->w('ДОБАВЛЕНИЕ МЕСТ ХРАНЕНИЯ',I_INFO);	
			//===============МЕСТА ХРАНЕНИЯ=================================================
			$mhr=u::win_utf($c->GetFile(APATH_DB,$defaultdb,MHR));
			$mhr_array=ji_install::get_mnu_txt_as_array($mhr);		
			
			if ($mhr_array){
				
				
				$kv=u::win_utf($c->GetFile(APATH_DB,$defaultdb,KV));
				$kv_array=ji_install::get_mnu_txt_as_array($kv);
				
				$mhrkv=u::win_utf($c->GetFile(APATH_DB,$defaultdb,MHRKV));
				$mhrkv_array_keys=ji_install::get_mnu_mhrkv_txt_as_arrays($mhrkv,true);
				$mhrkv_array_values=ji_install::get_mnu_mhrkv_txt_as_arrays($mhrkv,false);
				
				$mhr_req_array=ji_install::build_mhr_data($mhr_array,$kv_array,$mhrkv_array_keys,$mhrkv_array_values);
				ji_install::add_sql_data('#__mhr',$mhr_req_array,true);
			}else {
				ji_ilog::i()->w('Не удалось получить содержимое файла mhr.mnu: '.$mhr,I_ERROR,I_ERROR_LEVEL_ORDINARY);
			}
			
			//\===============ПУТИ В ФАЙЛЕ configuration.php=================================================
			
			$joomla_cfg=@file_get_contents(JI_PATH_HTDOCS_LOCAL.'/'.JI_DIR_JIRBIS.'/'.'configuration.php');
			$joomla_cfg=str_ireplace(array('c:\\jirbis2_server\\htdocs\\jirbis2\\','C:\\xampp\\htdocs\\jirbis2\\'),JI_PATH_HTDOCS_LOCAL.'/'.JI_DIR_JIRBIS,$joomla_cfg);
			
			if (@file_put_contents($joomla_cfg)===false)
				ji_ilog::i()->w('Не удалось изменить пути в файле configuration.php. Возможно, он не доступен для записи',I_ERROR);
			
			//===============ПУТИ В ФАЙЛЕ configuration.php=================================================
			
			$CFG['installed']=true;
			//ji_ilog::i()->w('Не удалось получить содержимое файла mhr.mnu: '.$mhr,I_ERROR,I_ERROR_LEVEL_ORDINARY);
	
			if (!u::cfg_write(JI_PATH_HTDOCS_LOCAL.'/'.JI_DIR_JIRBIS.'/'.JI_DIR_COMPONENT.'/jirbis_configuration.php',$CFG,'CFG'))
				ji_ilog::i()->w('Не удалось открыть на запись файл jirbis_configuration.php',I_ERROR,I_ERROR_LEVEL_CRITIICALY,0);
			
	}catch (Exception $e){
		ji_ilog::i()->w('Критическая ошибка: '.$e->getMessage(),I_ERROR,0,$e->getCode());
		$answer['success']=false;
	}	
	
	

	$answer['errors']=errors::is_errors() ? errors::get_objects_array() : array();
	
	answer::jsone($answer);
	//	errors::is_critical_errors() ? false : true;
		
	$c->Disconnect();
	//$recs=$this->connection->find_jrecords($this->bns,$this->req,'',$this->first_number,$this->portion,$this->profile);
	
}elseif($task==='registration'){

	try{
		
		echo ji_update::post(JI_PATH_REQUEST_UPDATE_REGISTRATION,80,$CFG['default_timeout'],$_REQUEST);
	}catch (Exception $e){
		// Исключение может инициироваться только ошибкой передачи запроса на сервер регистрации		
		ji_ilog::i()->w('Критическая ошибка при отправке данных регистрационных данных. Возможно, у сервера нет права на запросы к внешним серверам. '.$e->getMessage(),I_ERROR,0,$e->getCode());
		$answer['success']=false;
		$answer['errors']=errors::is_errors() ? errors::get_objects_array() : array();
		answer::jsone($answer);		
	}

	
}



?>
