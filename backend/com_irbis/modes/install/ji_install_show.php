<?php

class ji_install_show{

	public  static function show_js_css(){
		?>
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/<?php echo u::ga($GLOBALS['CFG'],'adm_panel_theme','smoothness')?>/jquery-ui.min.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jquery.treeview.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/pagination.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jirbis.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/install.css" rel="stylesheet" />
	
	
	    <script src="<?php echo JI_PATH_JS_NET ?>/jquery-1.10.2.min.js" type="text/javascript"></script>
	    <script src="<?php echo JI_PATH_JS_NET ?>/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.corner.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.cookie.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.validate.min.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.form.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jirbis_utils.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/install.js" type="text/javascript"></script>	
	<?
	}	
	
	public static function errors_output($messages_array){
		if (!$messages_array) return;
		
		
		
		ob_start();	
		echo '<table>';
		foreach($messages_array as $c){
		?>
		<tr>
						
			<td>
				<?php echo $c; ?>
			</td>			
			
		</tr>
		<?php

		}
		echo '</table>';
		
		return ob_get_clean(); 
	}
	
	static public function template(){
		$content=file_get_contents(JI_PATH_HTDOCS_LOCAL.'/'.JI_DIR_JIRBIS.'/'.JI_DIR_COMPONENT.'/modes/install/'.JI_FILE_INSTALL_FORM);
		$content=explode('<!-- DELIMITER -->',$content);
			eval( '?>'.$content[1]);
	}	
	
	public function get_js_settings(){	
		global $CFG;
		
		?>
		<script type="text/javascript">
		
		var st={	
			"images_net_path":"<?php echo JI_PATH_IMAGES_NET ?>",
			"jirbis_dir":"<?php echo JI_DIR_JIRBIS ?>",						
			"ip_local":"<?php echo ji_install::get_client_ip() ?>"			
		};
		
		
		</script>
		<?php
	}	
	
}

?>