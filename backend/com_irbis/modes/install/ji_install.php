<?php
class ji_install{
	
	public  static function output($params_array=array(),$search_mode=''){	
		ji_install_show::get_js_settings();
		ji_install_show::show_js_css();
		ji_install_show::template();	
	}	
	
	static public function get_client_ip(){
		if (empty($_SERVER['HTTP_CLIENT_IP'])==FALSE) //"расшаренный"
		  $r=$_SERVER['HTTP_CLIENT_IP'];
		 elseif (empty($_SERVER['HTTP_X_FORWARDED_FOR'])==FALSE) //если прокси
		  $r=$_SERVER['HTTP_X_FORWARDED_FOR'];
		 else
		  $r=$_SERVER['REMOTE_ADDR'];
		 return $r;	
	}
	
	// Возвращает массив для записи в таблицу данных о подразделениях
	static public function build_mhr_data($mhr_array,$kv_array,$mhrkv_array_key,$mhrkv_array_value){
		
		$recs=array();

		foreach ($mhr_array as $mhr_key=>$mhr_value){
			$rec=array('mhr_shot_name'=>$mhr_key,'mhr_full_name'=>$mhr_value,'kaf_shot_name'=>'','kaf_full_name'=>'');
			$is_result=false;
			if ($mhrkv_array_key && $kv_array){			
				for ($i=0;$i<count($mhrkv_array_key);$i++){
					if ($mhr_key===$mhrkv_array_key[$i] && isset($kv_array[$mhrkv_array_value[$i]])){
						$recs[]=array_merge($rec,array('kaf_shot_name'=>$mhrkv_array_value[$i],'kaf_full_name'=>$kv_array[$mhrkv_array_value[$i]]));
						$is_result=true;				
					}
					 
				}
			}
			
			if (!$is_result)		
				$recs[]=$rec;
		}
		return $recs;
	}
	
	
	public static function add_sql_data($table,$recs=array(),$replace=false){
		global $__db;
		
		$sql_parts=array();
		$array_keys=array_keys($recs[0]);
		
		foreach($recs as $rec){
			$sql_record=array();
			foreach ($rec as $field){
				$sql_record[]=idb::i()->Quote($field);
			}
			// Обеспечиваем корректность по количеству полей. 
			//$sql_record=array_merge(array_fill(0,count($array_keys),''),$sql_record);
			$sql_parts[]="(".implode(',',$sql_record).")";
		}
		$operator=($replace) ? 'REPLACE' : 'INSERT';
		$sql = "$operator INTO $table (".implode(',',$array_keys).") VALUES ";
		$sql.=implode(',',$sql_parts);	
		idb::i()->setQuery($sql);				
		idb::i()->query();
		
		if (idb::i()->getErrorNum()) 
			ji_ilog::i()->w('Ошибка в запросе MySQL: '.idb::i()->getErrorMsg(),I_ERROR,I_ERROR_LEVEL_CRITIICALY,idb::i()->getErrorNum());
			
		return true;
		 
	}	
	
	
	public static function get_mnu_txt_as_array($mnu_txt=array()){
		if (!$mnu_txt) return array();
		
		$readed_mnu=array();
		$keys=explode("\n",$mnu_txt);
		
		for($i=0;$i<count($keys)-1;$i+=2){
			if (trim($keys[$i])==='*****') break;
			$readed_mnu[trim($keys[$i])]=trim($keys[$i+1]);			
		}		
		return $readed_mnu;
	}

	
	public static function get_mnu_mhrkv_txt_as_arrays($mnu_txt=array(),$keys=true){
		if (!$mnu_txt) return array();
		
		$keys=explode("\n",$mnu_txt);
		$readed_mnu=array();
		for($i=0;$i<count($keys)-1;$i+=2){
			if (trim($keys[$i])==='*****') break;
			if ($keys)
				$readed_mnu[]=trim($keys[$i]);			
				else 				
				$readed_mnu[]=trim($keys[$i+1]);			
		}		
		return $readed_mnu;
	}

	public static function get_bases_array_mnu_as_req_array($bases_mnu_array){
		if (!$bases_mnu_array) return array();
		
		foreach($bases_mnu_array as $key=>$value){
				$req_array[]=array('bns'=>trim($key,'- '),'bnf'=>$value,'lib_id'=>1);
		}
		
		return $req_array;
	}
	
	public static function get_first_assoc_array_key($assoc_array){
		if (!$assoc_array) return '';
		foreach($assoc_array as $key=>$value){
			return $key;
		}
	}
	
	public static function copy_pft_and_mnu($deposit_path){
			$pft_path_temp=JI_PATH_TMP_LOCAL.'/'.JI_DIR_UPDATE;
		foreach (glob($pft_path_temp."/*.*") as $filename){
		    	if (!file_exists($deposit_path.'/'.basename($filename)) || stripos(basename($filename),'.mnu')=== false){
		    		if (!@copy($filename,$deposit_path.'/'.basename($filename)))
		    			ji_ilog::i()->w('Не удалось выполнить копирвание файла: '.basename($filename).' в папку '.$deposit_path,I_ERROR,I_ERROR_LEVEL_CRITIICALY,100);			    			
		    	
			}
		}
	}
	
	
	

}



?>