<?php


$mode='adm_panel';
require_once('../../jirbis_link_libraries.php' );


require_once(JI_PATH_INCLUDES_LOCAL.'\table_editor.php');
require_once(JI_PATH_INCLUDES_LOCAL.'\ji_table_editor.php');
require_once(JI_PATH_COMPONENT_LOCAL."/modes/$mode/ji_adm_panel.php" );
require_once(JI_PATH_COMPONENT_LOCAL."/modes/$mode/ji_adm_panel_show.php" );
require_once(JI_PATH_COMPONENT_LOCAL."/modes/$mode/adm_panel_structures.php" );
require_once(JI_PATH_COMPONENT_LOCAL."/modes/$mode/ji_cfg_description.php" );


idb::i()->debug(0);
ji_table_editor::$self_net_path=JI_PATH_COMPONENT_NET.'/modes/adm_panel/'.JI_FILE_AJAX_PROVIDER;

$structures_names=u::ga( $_REQUEST,'structures',array(mosGetParam( $_REQUEST, 'structure', '' )));


$operation= mosGetParam( $_REQUEST, 'oper', '' );
//token =md5(date('Ydm').self::$solt) -- код проверки подлинности запроса 
$token= mosGetParam( $_REQUEST, 'token', '' );
//2. name -- название таблицы или ассоциативного массива ($structure->name)  
$name= mosGetParam( $_REQUEST, 'name', '' );
//Запись, которая вставляется или редактируется. Фильтруем ненужные поля после. 
$record= $_REQUEST;
//3. index -- идентификатор записи или элемента массива (когда операция касается конкретной записи) 
$index= mosGetParam( $_REQUEST, 'id', 0 );
//4. field -- название поля, поля в которое встроен вложенный  массив (нужно только в том случае, если объект редакции -- сериализованный интегрированный масив) 
$field= mosGetParam( $_REQUEST, 'field', '' );
//5. int_index -- индекс записи во вложенном массиве  (нужно только в том случае, если объект редакции -- сериализованный интегрированный масив)
$parent_id= mosGetParam( $_REQUEST, 'parent_id', '' );

$filter= mosGetParam( $_REQUEST, 'filter', '' );

// Если это запрос на получение JSON данных 
if ($operation || $name){
	try{	
	// Цикл в сущности используется только для операции сохранения.... 	
		foreach ($structures_names as $structure_name){						
			$t=new ji_table_editor($structure_name);
			$res=$t->execute_operation($operation,$token,$name,$record,$index,$field,$parent_id,$filter);
			// Если возвращается массив. Но ведь может возвращаться и сообщение об ошибке... 
		}
		answer::jsone(array());
	}catch(Exception $e){  
		ji_log::i()->w($e->getMessage(),I_ERROR,'',$e->getCode());
		ji_adm_panel_show::error_messages_json();
	}
}else {
	// Если это запрос на получение стандартного набора таблиц
		
	try{
		foreach ($structures_names as $structure_name){		
			$t=new ji_table_editor($structure_name);			
			$t->draw_tables();
		}
	}catch(Exception $e){  
		ji_log::i()->w($e->getMessage(),I_ERROR,'',$e->getCode());
		ji_adm_panel_show::error_messages_html();
	}	
	
	
	?>
	
	<!--// TODO: Заменить путь на реальный-->
	
	
	<form enctype="Multipart/form-data" action="<?php echo ji_table_editor::get_save_url($structures_names) ?>" class="jirbis_admin_form" method="POST">
	<div class="log"></div>
	<div class='table_wrapper'>	
		
	    <button type="submit"  class="button">Сохранить</button>
	    <button type="button" class="button reload">Отказаться от изменений</button>
	</div>
	
	</form>
	<?php	
}




?>