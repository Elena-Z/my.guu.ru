<?php
class ji_svk_show{
	public  static function js_css(){
		?>
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/<?php echo u::ga($GLOBALS['CFG'],'frontend_theme','smoothness')?>/jquery-ui.min.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jquery.treeview.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jquery-ui-combobox.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/ui.jqgrid.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jirbis.css" rel="stylesheet" />
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-1.10.2.min.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.timers.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.hotkeys.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.cookie.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.form.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/crc32.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-ui-combobox.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/i18n/grid.locale-ru.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.jqGrid.min.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jirbis_svk.js" type="text/javascript"></script>	
		<?php	
	}
	public  static function js_settings($st){	
		?>		
		<script type="text/javascript">		
			var st=<?php echo json_encode($st) ?>;
		</script>
		<?php
	}	
	
	public static function template($template_path,$data){
		
		if (!($content=@file_get_contents($template_path))){
			echo 'Не найден файл шаблона по адресу: '.$template_path;
			return;
		}	
		
		$content=explode('<!-- DELIMITER -->',$content);
		
		if (!is_array($content) or count($content)!=3) {
			echo  'Неправильная разметка страницы! Код шаблона не обрамлён двумя разделителями <!-- DELIMITER --> !';
			return ;
		}		
		
		eval( '?>'.$content[1]);
	}	
	
	public  static function debts_accordion($accordion_array){
		ob_start();
		?>
			

					
		<?php	
		return ob_get_clean(); 
	}

	private function debt_list($bo_list){
		if (!is_array($bo_list) or !$bo_list){
			echo '<h3>Нет данных</h3>';
			return;
		}
		
		echo '<ol class="debts_list">';
			foreach($bo_list as $bo){
				echo "<li>{$bo['bo']}</li>";			
			}
		echo '</ol>';
	}
	
	
}
?>