<?php

// Отвечает за интерфейсные решения
define( 'DEBUG', $CFG['debug_output']);
define( 'DEBUG_BROAD', 0);
define( 'DEBUG_SINGLE', 0);
define( 'DEBUG_REFORMAT', 0);
define( 'DEBUG_FREE', 0);
define( 'DEBUG_TERMS', 0);
define( 'DEBUG_TERMS_SINGLE', 0);
define( 'DEBUG_RESULTS', 0);

define( 'DEBUG_BO_GRID', 0);
define( 'DEBUG_BO_PRINT', 0);

define( 'DEBUG_SEARCH_FORM', 0);



//Блокировка сессии
define('JI_LOCK_FULL',0);
define('JI_LOCK_BEFORE_UPDATE',1);
define('JI_LOCK_NO',2);


// Пути
define('JI_FILE_AJAX_PROVIDER','ajax_provider.php');
define('JI_FILE_INSTALL','jirbis_install.php');
define('JI_FILE_UPLOAD','upload.php');
define('JI_FILE_NO_COVER_IMAGE_NAME','no_cover');
define('JI_FILE_DEFAULT_COVER_IMAGE_NAME','no_cover');
define('JI_FILE_PHP_ERRORS_LOG','php_errors.txt');

define('JI_FILE_UPDATE','update.zip');
define('JI_FILE_PFT_ARCHIVE','pft.zip');
define('JI_FILE_NO_UPDATE_FILES_LIST','no_update_list.txt');
define('JI_FILE_CRYPT_FILES_LIST','crypt_list.txt');
define('JI_FILE_CRYPT_SOFT_LIST','crypt_list_soft.txt');
define('JI_FILE_PRIVATE_FILES_LIST','private_list.txt');
define('JI_FILE_PUBLIC_FILES_LIST','public_list.txt');
define('JI_FILE_PFT_MNU_LIST','pft_mnu_list.txt');
define('JI_FILE_INSTALL_FORM','install_form.htm');


define('JI_FILE_SPEC_MNU','spec.mnu');
define('JI_FILE_KAF_MNU','KAFCH.MNU');
define('JI_FILE_PURPOSE_MNU','691G.MNU');
define('JI_FILE_VO_MNU','VO.MNU');

define('JI_FILE_JUSERS_CSV','jusers.csv');



define('JI_DIR_COMPONENT','components/com_irbis');
define('JI_DIR_FORMS','search_forms');
define('JI_DIR_IMAGES','images');
define('JI_DIR_COVERS','covers');
define('JI_DIR_TMP','tmp');
define('JI_DIR_DEBUG','debug');
define('JI_DIR_INCLUDES','includes');
define('JI_DIR_JS','js');
define('JI_DIR_CSS','css');
define('JI_DIR_UPDATE','update');
define('JI_DIR_UPDATE_SCRIPTS','update_scripts');
define('JI_DIR_INSTALL','install');
define('JI_DIR_MODES','modes');
define('JI_DIR_PDF_VIEW','pdf_view');



define('JI_PATH_AUTO_REQUESTS_LOCALHOST','http://localhost'.((isset($_SERVER['SERVER_PORT']) and $_SERVER['SERVER_PORT']!=80 ) ? ':'.$_SERVER['SERVER_PORT'] : ''));
define('JI_PATH_AUTO_REQUESTS_GLOBAL',''); // Корневая директория





//-----------ОСНОВНЫЕ ПУТИ---------------------------------------
define('JI_PATH_HTDOCS_LOCAL',$CFG['ji_path_htdocs_local']);
define('JI_DIR_JIRBIS',$CFG['ji_dir_jirbis']);
//define('JI_DIR_JIRBIS_DISTR',JI_DIR_JIRBIS.'_distr');


define('JI_PATH_CMS_LOCAL',JI_PATH_HTDOCS_LOCAL.'/'.JI_DIR_JIRBIS);
define('JI_PATH_CMS_NET',JI_PATH_AUTO_REQUESTS_GLOBAL .'/'.JI_DIR_JIRBIS);
define('JI_PATH_CMS_DISTR_LOCAL','c:/jirbis2_server/htdocs/jirbis2');


define('JI_PATH_COMPONENT_LOCAL',JI_PATH_CMS_LOCAL.'/'.JI_DIR_COMPONENT);

define('JI_PATH_COMPONENT_NET',JI_PATH_CMS_NET.'/'.JI_DIR_COMPONENT);

//=================ЛОКАЛЬНЫЕ ПУТИ========================================
//-----------------ПУТИ CMS------------------
define('JI_PATH_DEBUG_LOCAL',JI_PATH_CMS_LOCAL .'/'.JI_DIR_DEBUG);
define('JI_PATH_TMP_LOCAL',JI_PATH_CMS_LOCAL .'/'.JI_DIR_TMP );
define('JI_PATH_TEMP_UPDATE_LOCAL',JI_PATH_CMS_LOCAL .'/'.JI_DIR_TMP.'/'.JI_DIR_UPDATE);
//-----------------ПУТИ CMS------------------

//-----------------ПУТИ ДЛЯ ПАПКИ КОМПОНЕНТА------------------
define('JI_PATH_IMAGES_LOCAL',JI_PATH_COMPONENT_LOCAL.'/'.JI_DIR_IMAGES); 
define('JI_PATH_COVERS_LOCAL',JI_PATH_COMPONENT_LOCAL.'/'.JI_DIR_IMAGES.'/'.JI_DIR_COVERS); 
define('JI_PATH_FORMS_LOCAL',JI_PATH_COMPONENT_LOCAL.'/'.JI_DIR_FORMS);
define('JI_PATH_INCLUDES_LOCAL',JI_PATH_COMPONENT_LOCAL.'/'.JI_DIR_INCLUDES);
define('JI_PATH_UPDATE_SCRIPTS_LOCAL',JI_PATH_COMPONENT_LOCAL.'/'.JI_DIR_UPDATE_SCRIPTS);
define('JI_PATH_MODES_LOCAL',JI_PATH_COMPONENT_LOCAL.'/'.JI_DIR_MODES);
define('JI_PATH_PDF_VIEW_LOCAL',JI_PATH_COMPONENT_LOCAL.'/'.JI_DIR_PDF_VIEW);
//\-----------------ПУТИ ДЛЯ ПАПКИ КОМПОНЕНТА------------------


//=================СЕТЕВЫЕ ПУТИ========================================
//-----------------ПУТИ ДЛЯ ПАПКИ КОМПОНЕНТА------------------
define('JI_PATH_COVERS_NET',JI_PATH_COMPONENT_NET.'/'.JI_DIR_IMAGES.'/'.JI_DIR_COVERS);
define('JI_PATH_IMAGES_NET',JI_PATH_COMPONENT_NET.'/'.JI_DIR_IMAGES);
define('JI_PATH_CSS_NET',JI_PATH_COMPONENT_NET.'/'.JI_DIR_CSS);
define('JI_PATH_JS_NET',JI_PATH_COMPONENT_NET.'/'.JI_DIR_JS);
define('JI_PATH_PDF_VIEW_NET',JI_PATH_COMPONENT_NET.'/'.JI_DIR_PDF_VIEW);
//\-----------------ПУТИ ДЛЯ ПАПКИ КОМПОНЕНТА------------------


define('JI_PATH_PFT_LOCAL',$CFG['ji_path_pft']);

define('JI_FILE_PATH_AUTO_REQUESTS_NET_LOCALHOST_PROVIDER',JI_PATH_AUTO_REQUESTS_LOCALHOST.'/'.JI_DIR_JIRBIS.'/'.JI_DIR_COMPONENT.'/'.JI_FILE_AJAX_PROVIDER);
define('JI_FILE_PATH_AUTO_REQUESTS_NET_GLOBAL_PROVIDER',JI_PATH_COMPONENT_NET.'/'.JI_FILE_AJAX_PROVIDER);

// ===================ПУТЬ К СЕРВЕРУ ОБНОВЛЕНИЙ И ДИСТРИБУТИВОВ=========
define('JI_PATH_UPDATE_HOST_NET','http://vlibrarynew.gpntb.ru/jirbis2/'.JI_DIR_COMPONENT);
define('JI_PATH_REQUEST_UPDATE_REGISTRATION',JI_PATH_UPDATE_HOST_NET.'/jirbis_update_registration.php');
define('JI_PATH_GET_DISTRIB',JI_PATH_UPDATE_HOST_NET.'/get_distrib.php');


//Семь минут
ini_set('max_execution_time',60*7);
ini_set('error_log',JI_PATH_DEBUG_LOCAL.'/'.JI_FILE_PHP_ERRORS_LOG);
//ini_set('session.save_path', $CFG['open_temp_local_path']);
//ini_set('session.gc_maxlifetime', 365*24*60*60);
//ini_set('session.cookie_lifetime',  365*24*60*60);
ini_set('display_errors','On');
//ini_set('session.gc_probability','0');
ini_set('magic_quotes_gpc','Off');
//ini_set('magic_quotes_gpc','Off');
date_default_timezone_set('Europe/Moscow');
//ini_set('session.name','PHPSESSID');

?>