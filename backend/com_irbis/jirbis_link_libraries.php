<?php

define( '_VALID_JIRBIS', 1 );
setlocale(LC_ALL, 'ru_RU.UTF-8'); 
define("JI_JOOMLA_CONFIG",1);


$base_path='';
if (defined('_JEXEC')) 
	$base_path=str_replace(array('/administrator','\administrator'),'',JPATH_BASE.'/components/com_irbis/');

require_once( $base_path.'jirbis_configuration.php' );
require_once( $base_path.'jirbis_joomla_integration.php' );
require_once( $base_path.'jirbis_defaults.php' );
require_once( $base_path.'jirbis_constants.php' );
require_once( JI_PATH_INCLUDES_LOCAL.'/common.php' );



spl_autoload_register('jirbis_autoload');

function jirbis_autoload($classname){

 	//echo $classname;
	if (file_exists(JI_PATH_INCLUDES_LOCAL."/$classname.php")){
		require_once( JI_PATH_INCLUDES_LOCAL."/$classname.php");
	}elseif(file_exists(JI_PATH_INCLUDES_LOCAL."/class.$classname.php")){ 	
		require_once( JI_PATH_INCLUDES_LOCAL."/class.$classname.php");
	}elseif(defined('JI_MODE') and file_exists(JI_PATH_MODES_LOCAL."/".JI_MODE."/$classname.php")){
		require_once( JI_PATH_MODES_LOCAL."/".JI_MODE."/$classname.php");
	}

}


?>