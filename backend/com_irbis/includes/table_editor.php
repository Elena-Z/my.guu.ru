<?php

// Тип объекта
define('JI_SRC_MYSQL',0);
define('JI_SRC_ARRAY',1);

define('JI_SRC_ARRAY_INTEGRATED',2);


class table_editor{
	public $structure_params=null;
	public $structure_name='';
	// Храним кэш в течение часа 
	public static $cache_live_time=12000;
	public static $timeout=200;
	// Путь для запросов данных 
	public static $self_net_path='!tables_ajax.php';
	public static $solt='PASSWORD';
	protected static $token='';
	const TEMP_INDEX='temp_index';
	const TEMP_VALUE='temp_value';
	const ABSENT='-';
	// Инициализируем путем установки базовой таблицы
	public function __construct($structure_name){
		// Вот это всё нужно перенести в код формирования таблицы.
		$this->structure_name=$structure_name;
		$this->structure_params=u::ga($GLOBALS,$this->structure_name,'');
		if (!$this->structure_params)
			throw new Exception("Структура {$this->structure_name} не найдена! ",999);						
		self::$token=self::get_token();
	}
	
	
	
	public static function get_token(){
		return md5(date('Ydm').self::$solt);
	}
	//Функция, выполняющая генерацию кода таблиц
	//Функция должна получать как минимум 2 параметра -- ребенка и родителя.  Данные родителя могут понадобится для отрисовки.  
	//Если запускается первый раз, получает $structure_params=$this->structure_params 
	
	//int_field -- иденти
	public function execute_operation($operation,$token='',$name='',$record=array(),$index='',$field='',$parent_id='',$filter=''){
		
		// Проверка кода
		if ($token!==self::$token)
			throw new Exception('Не верный код защиты!'.$name,666);
		
		// Если запрос не относится к элементу этой структуры, то ничего не делаем
		if ($name && !in_array($name,$this->get_independent_elements_list()))
			return;
		
		$res=array();
	
		
		switch ($operation){			
			// Получение таблицы
			case  'get':{
				if (!$field){
					$res=$this->get_cache_data($name,$filter);
					$res=$this->restruct_cache_data_for_output($res,$name,$filter);
				}else{
					$res=$this->get_field_integrated_table_cache($name,$field,$parent_id);
					$res=$this->restruct_cache_data_for_output($res,$field,$filter);
				}		
				
				$this->output_json_table_data($res);
				//return $res;
			}
			break;		
			
			case  'edit':{
				//$record=$this->get_record_from_post();
				if (!$field){	
					$record=$this->post_elements_filter($name,$record);
					$this->set_cached_record($name,$index,$record);
				}else {
					$record=$this->post_elements_filter($field,$record);
					$this->set_field_integrated_record_cache($name,$field,$parent_id,$index,$record);
				}
						
			}
			break;
						
			case  'add':{
				
				if (!$field){	
					$record=$this->post_elements_filter($name,$record);
					$this->ins_cached_record($name,$record,$index);
				}else{
					$record=$this->post_elements_filter($field,$record);
					$this->ins_field_integrated_record_cache($name,$field,$parent_id,$record,$index);
				}	
				
			}
			break;			
			
			case  'del':{
				if (!$field){	
					$this->del_cached_record($name,$index);
				}else{
					$this->del_field_integrated_record_cache($name,$field,$parent_id,$index);
				}	
			}
			break;
			// Запись кэша в оригинальные таблицы
			case  'update_source':{
				$this->update_source_tables();
				// Ответ может быть или положительный, или может быть сгенерирован эксепшен
				
			}
			break;			
		}
		
		
	}
	
	public function draw_tables(){
		// JS к
		$js_result_obj=null;

		#echo "<pre>";print_r($this->structure_params);
		//echo serialize($this->structure_params)."\n";
		///echo json_encode($this->structure_params)."\n";
		
		// Кэшируем используемые таблицы и массивы 		
		$this->cache_abstract_structure($this->structure_params);		
		//******** Отрисовываем <DIV> и настроечные данные  для таблиц и советов. ДЛЯ КОНКРЕТНОЙ таблицы и интегрированных в неё масссивов *********
		
		$this->draw_js($this->structure_params);
		$this->draw_html($this->structure_params);
		
	}
	// Передаёт данные таблицы в браузер в формате JSON	
	public function output_json_table_data($array){
		$out=array();
		$out['records']=count($array);
		//for($i=0;$i<count($array);$i++){
		
		foreach($array as $key=>$value){
			if (!empty($value))
				$out['rows'][]=array('id'=>$key,'cell'=>array_values( $value));
		}
		$out['page']=1;
		$out['total']=1;
		answer::jsone($out);
	}

	public function output_messages_html($messages){
		answer::jsone($out);
	}
	
	
	//===============ОБНОВЛЕНИЕ КЭШИРОВАННЫХ ДАННЫХ================================================
	// Обновляет запись в кэше . Спец операции для встроенных сериализованных массивов
	public function set_cached_record($name,$index,$record){
		
		$elements_list=array_diff($this->get_elements_list($name),$this->get_serialized_elements_list($name));
		// В отосланных данных могут быть встроенные массивы, поэтому нужно считывать заново
		$array=$this->get_cache_data($name);		

		// Обновляем все элементы, кроме сериализованных, потому, что вместо сериализованных будет гиперссылка или пустота	
		// За основу берём не $record, потому, что там $_REQUEST целиком
		foreach ($elements_list as $field_name){
			if (isset($record[$field_name]))
				$array[$index][$field_name]=$record[$field_name];
		}

		$this->set_cache_data($name,$array);
	}

	public function ins_cached_record($name,$record,$index){	
		
		$elements_list=$this->get_elements_list($name);
		// В отосланных данных могут быть встроенные массивы, поэтому нужно считывать заново
		$array=$this->get_cache_data($name);		

		
		$new_element=array();
		// Обновляем все элементы, кроме сериализованных, потому, что вместо сериализованных будет гиперссылка или пустота	
		// За основу берём не $record, потому, что там $_REQUEST целиком
		foreach ($elements_list as $field_name){			
				$new_element[$field_name]=u::ga($record,$field_name,'');
		}
		
		$array[$index]=$new_element;
		$this->set_cache_data($name,$array);		
	}

	public function del_cached_record($name,$index){
		$array=$this->get_cache_data($name);		
		$array[$index]=null;
		$this->set_cache_data($name,$array);		
	}

		//------ РАБОТА С ИНТЕГРИРОВАННЫМИ МАССИВАМИ --- поскольку изменения происходят только в кэше, то код не специфичен для J-ИРБИС и его можно вынести в базовый класс
	// Извлечение интегрированных массивов из КЭШИРОВАННОЙ таблицы.
	//Cобственного уникального имени у них нет -- только имя родитея, родительское поле и индекс в нём
	// Ключи (если они есть) должны записываться в несуществующее поле index. 
	protected function get_field_integrated_table_cache($parent_table_name,$parent_field_name,$parent_index){
		$array=$this->get_cache_data($parent_table_name);
		if (!isset($array[$parent_index][$parent_field_name]))
			throw new Exception("Попытка добавления интегрированных данных в не сохранённую строку. Сохраните новую строку! Таблица: $parent_table_name, поле: $parent_field_name, ID записи: $parent_index",40);					
		$res=$array[$parent_index][$parent_field_name];		
		return is_array($res) ? $res : array();
	}	
	
	// Обновление интегрированных массивов в КЭШИРОВАННОЙ  базовой таблице
	protected function set_field_integrated_record_cache($parent_table_name,$parent_field_name,$parent_index,$int_index,$record){
		$array=$this->get_cache_data($parent_table_name);	
		$elements_list=$this->get_elements_list($parent_field_name);

		foreach ($elements_list as $field_name){
			if (isset($record[$field_name])){
				// Номер записи родителя, имя поля родителя, номер записи интегрированной таблицы, имя поля в записи
				$array[$parent_index][$parent_field_name][$int_index][$field_name]=$record[$field_name];
			}	
		}
		
		
		
		$this->set_cache_data($parent_table_name,$array);		
	}

	// Обновление интегрированных массивов в КЭШИРОВАННОЙ  базовой таблице
	protected function ins_field_integrated_record_cache($parent_table_name,$parent_field_name,$parent_index,$record){
		$new_element=array();
		$array=$this->get_cache_data($parent_table_name);	
		$elements_list=$this->get_elements_list($parent_field_name);

		foreach ($elements_list as $field_name){
			$new_element[$field_name]=u::ga($record,$field_name,'');

		}
		
		$array[$parent_index][$parent_field_name][]=$new_element;
		$this->set_cache_data($parent_table_name,$array);		
	}


	protected function del_field_integrated_record_cache($parent_table_name,$parent_field_name,$parent_index,$int_index){
		$array=$this->get_cache_data($parent_table_name);		
		$array[$parent_index][$parent_field_name][$int_index]=null;
		$this->set_cache_data($parent_table_name,$array);		
	}	
	//\------ РАБОТА С ИНТЕГРИРОВАННЫМИ МАССИВАМИ

	
	//\===============ОБНОВЛЕНИЕ КЭШИРОВАННЫХ ДАННЫХ================================================
	
	// Размещает элементы таблицы в том порядке, в котором они находятся в структуре.
	//  Заменяет интегрированные массивы на ссылки
	
	// TODO: в качестве фильтра выступает ID родительской таблицы. 
	// Порядок решения: 
	/*А) Определяем родительскую -- написать функцию 
	Б) Берём её кэш 
	В) Определяем какое значение поля-идентификатора родительской соответствует этому ID в кэше.
	Г) Фильтруем локальную таблицу по этому полю. 
	*/
	protected function restruct_cache_data_for_output($array,$name,$filter=''){		
		$array_restructured=array();
        //print_r($array);
        //die();
		$serialized=$this->get_serialized_elements_list($name);
		// Получаем полный список элементов таблицы из структуры.
		$elements_list=$this->get_elements_list($name);
		
		if ($array){
			// Перебор записей
			$i=0;
			foreach ($array as $key=>$value){
				
				// Удалённые элементы мы не выводим. 
				if (empty($value))
					continue;
					
				if (count($value)!=count($elements_list))
					throw new Exception("Количество элементов model и таблицы $name не совпадает! Необходимо проверить model структуры!",10);		

				$array_restructured[$key]=array();
				// Перебор полей-элементов. За основу берём структуру и обеспечиваем идентичный ей порядок элементов. 
				foreach ($elements_list as $field_name){					
					// Если текущий элемент не является временным индексом
					//$field_name!==self::TEMP_INDEX &&&& !is_null($value[$field_name]) 
					// null -- возможное значение в данном случае для некоторых SQL полей в старой версии 
					if (!isset($value[$field_name]) && !is_null($value[$field_name])) 
						throw new Exception("Среди полей таблицы $name не найден элемент структуры $field_name",11);		
					
					// Если внутри сериализованный массив, очищаем элемент. Если нет -- записываем оригинальное значение	
					if (in_array($field_name,$serialized)){
						$array_restructured[$key][$field_name]='';
					}else{
						$array_restructured[$key][$field_name]=$value[$field_name];
					}
				}
				$i++;
			}
			return $array_restructured;
		}
		return array();
	}
	
	
	// Отрисовка JS основе параметров структуры. Рекурсивная
	protected function draw_js($structure_params){
		
		// JQUERY GRID. Фактически это не массив, а объект 
		$jg=new stdClass();
		// ------Статичные праметры-----
		$jg->mtype='POST';
		$jg->datatype='json';
		$jg->rowNum=2000;
		$jg->viewrecords=true;
		$jg->search=false;
		$jg->pager='#'.$structure_params->name.'_pager';
		$jg->rowList=array();
		$jg->pgbuttons=false;
		$jg->pgtext=0;
		
		$jg->autowidth=true;
		$jg->shrinkToFit= false;
				
		// ------Статичные праметры-----
		
		if ($structure_params->sortname){
			$jg->sortorder='asc';
			$jg->sortname=$structure_params->sortname;
		}
		
		$jg->caption=$structure_params->caption;		

		$jg->height=property_exists($structure_params,'height') ? $structure_params->height : 150;
		// index поля в родительской таблице должен подставляться динамически!!!!!!		
		// Если эта таблица является дочерней
		if (property_exists($structure_params,'parent_name')){		
			$jg->url=self::$self_net_path.'?structure='.$this->structure_name.'&oper=get&name='.$structure_params->parent_name.'&field='.$structure_params->name.'&token='.self::$token;
			$jg->editurl=self::$self_net_path.'?structure='.$this->structure_name.'&name='.$structure_params->parent_name.'&field='.$structure_params->name.'&token='.self::$token;
		}else{		
			$jg->url=self::$self_net_path.'?structure='.$this->structure_name.'&oper=get&name='.$structure_params->name.'&token='.self::$token;
			$jg->editurl=self::$self_net_path.'?structure='.$this->structure_name.'&name='.$structure_params->name.'&token='.self::$token;
		}	
		
		$jg->colNames=array();
		$jg->colModel=array();
		if (!is_array($structure_params->model))
			throw new Exception('Model не является массивом! ПОстроение таблицы не возможно',1);
		
		//$jg->serializeRowData="*function(postdata){return {'record': JSON.stringify(postdata)}}*";
 	
		foreach ($structure_params->model as $val) {
		if (empty($val['name']) || empty($val['label']))
			throw new Exception("Отсутствует необходимый элемент массивов в описании таблицы {$structure_params->name} model -- ".var_export($val,true),1);

			$jg->colNames[] = $val['label'];
			$field_params=new stdClass();
			
			$field_params->editable=isset($val['structure']) ? false :  (u::ga($val,'editable',true) ?  true : false );									
			$field_params->classes=isset($val['structure']) ? 'edit-in-table' :'';
						
			//-------Обязательные элементы---------
			$field_params->name=$field_params->index=$val['name'];			
			// Приблизительный размер одного знака -- 8 пикселей. 
			$field_params->width=u::ga($val,'width',mb_strlen($val['label'],'UTF-8')*8);
			$field_params->search=u::ga($val,'search','false');			
			// Не факт, что до этого свойства удастся добраться,но всё же попробуем... 	
			$field_params->tip=u::ga($val,'tip','');
			//\-------Обязательные элементы---------
			
			//-------Не обязательные элементы---------			
			$field_params->edittype=u::ga($val,'edittype','text');
            $field_params->editrules=u::ga($val,'editrules',array());
			$field_params->editoptions=u::ga($val,'editoptions','');

			// Здесь выводим структуру интегрированной таблицы					
			if (isset($val['structure'])){ 	
				$val['structure']->parent_name=$structure_params->name;				
				self::draw_js($val['structure']);
			}
			//\-------Не обязательные элементы---------	
			
			$jg->colModel[] = $field_params;
		}
		
		// В случае, если мы имеем дело со связанными тблицами, draw_js должна выводить структуру связанной таблицы в 
		if (isset($structure_params->ref_table)){			
			$url=self::$self_net_path.'?oper=get&structure='.$this->structure_name.'&name='.$structure_params->ref_table->name.'&token='.self::$token;
			$jg->onSelectRow="*function(filter) { $('#{$structure_params->ref_table->name}').jqGrid('setGridParam',{url:'$url&filter='+filter}).trigger('reloadGrid');}*";
			// Для автоматической редакции:  if(filter && filter!==lastsel){ $('#{$structure_params->ref_table->name}').jqGrid('restoreRow',lastsel); $('#{$structure_params->ref_table->name}').jqGrid('editRow',filter,true); lastsel=filter; }
			$this->draw_js($structure_params->ref_table);					
		}
		
		
			
		
		$json=json_encode($jg);
		//Выполняем замену на функцию..........
		// Удаляем кавычки, которые автоматически добавяются при конвертировании кода функции onSelectRow
		$json=str_replace(array('"*','*"'),'',$json);	
		
		//echo "<pre>";print_r($jg);echo "</pre>";

        echo "
        <script type='text/javascript'>
        jQuery(function($){\r\n
            App.InitGrid." . ($structure_params->show_default ? "create" : "save") . "({\r\n
                name: '{$structure_params->name}',\r\n
                data: {$json}\r\n
            });
        });
        </script>";

        /*
         $('#{$structure_params->name}').jqGrid(\r\n$json\r\n);\r\n\r\n
		          $('#{$structure_params->name}').jqGrid('navGrid','{$jg->pager}',{edit:false,add:false,del:true,search:false});
		          $('#{$structure_params->name}').jqGrid('inlineNav','{$jg->pager}');
		});
         */
		
	}
	
	
	
	// Отрисовка HTML на основе параметров структуры. Рекурсивная
	protected function draw_html($structure_params=null){
		// Если отсутствует structure_params 
		if (!$structure_params){			
			$structure_params=$this->structure_params;		
		}
		
		if (!property_exists($structure_params,'show_default'))
			throw new Exception("Отсутствует флаг необходимости вывода таблицы show_default в таблице {$structure_params->name}:  ".var_export($structure_params,true),3);
		
		$display=($structure_params->show_default) ? '' : "style='display:none'";
		// Примерный код.... Выводим слои для таблиц и подсказок.
		echo "
		<div class='table_wrapper' id='{$structure_params->name}_wrapper' $display>
			<table class='table' id='{$structure_params->name}'></table>
			<div  id='{$structure_params->name}_pager'></div>
			<div class='tip' id='{$structure_params->name}_tip'></div>
		</div>
			";
		
		
		// Рекурсия для интегрированных массивов
		foreach($structure_params->model as $s){
			if (isset($s['structure'])){
				
				$this->draw_html($s['structure']);
			}
		}
		// Рекурсия для связанных таблиц. 
		if (isset($structure_params->ref_table))
			$this->draw_html($structure_params->ref_table);			
	}

	protected function get_serialized_elements_list($name,$structure_params=null){
		$serialized_list=array();
		if (!$structure_params){
			$structure_params=$this->structure_params;			
			
		}
		
		// Если нас интересует текущий элемент, возвращаем результаты по его сериализованным полям	
		
		if ($structure_params->name===$name){
			$serialized_list=array();
			foreach ($structure_params->model as $key=>$value){
				if (isset($value['structure'])){
					$serialized_list[]=$value['name'];
				}	
			}
			// Если нас интересует вложенная таблица -- берём её данные
		}elseif(isset($structure_params->ref_table)){
			$serialized_list=$this->get_serialized_elements_list($name,$structure_params->ref_table);			
		}
		
		return $serialized_list;
			
	}
	//Получение списка элементов структуры. Выполняет поиск нужной структуры по названию  
	protected function get_elements_list($name,$structure_params=null){
		$elements_list=array();
		if (!$structure_params){
			$structure_params=$this->structure_params;			
		}
		
		// Просматриваем сериализованные массивы
		foreach ($structure_params->model as $key=>$value){
			if (isset($value['structure'])){
				if (($elements_list=$this->get_elements_list($name,$value['structure']))){
					return $elements_list;
				}
			}	
		}
		// Если найдено совпадение имени с текущей структурой -- прерываем операцию и возвращаем данные
		if ($structure_params->name===$name){			
			foreach ($structure_params->model as $element){
				if (empty($element['name']))
					throw new Exception("Для элемента model таблицы {$structure_params->name} не найден элемент name");
				$elements_list[]=	$element['name'];				
			}
			return 	$elements_list;
			// Если в связанной таблице найдено нужное поле -- так же прерываем операцию. 
		}elseif(isset($structure_params->ref_table)){
			if (($elements_list=$this->get_elements_list($name,$structure_params->ref_table))){
				return $elements_list;		
			}	
		}
		

		return $elements_list;	
	}	
	// Получение списка  реальных, не интегрированных таблиц
	protected function get_independent_elements_list($structure_params=null){
		if (!$structure_params){
			$res=array();
			$structure_params=$this->structure_params;
		}
		$res[]=$structure_params->name;
		if(isset($structure_params->ref_table)){
			$res=array_merge($this->get_independent_elements_list($structure_params->ref_table),$res);
		}
		return $res;	
	}
	
	
	
	protected function post_elements_filter($name,$post_array){
		$filtred_array=array();
		$elements_list=$this->get_elements_list($name);		
		foreach ($elements_list as $element_name){
			if (isset($post_array[$element_name]))
				$filtred_array[$element_name]= $post_array[$element_name];//u::ga($post_array,$element_name,'');
		}
		return $filtred_array;
	}



	//--------РЕАЛИЗАЦИЯ РАБОТЫ С КЭШЕМ (сессией)
	//Кэширование ассоциативного массива
	public function set_cache_data($name,$array){
		if (($array=u::set_cache('table',self::$cache_live_time,$name,$array))<0)
			throw new Exception('Не удаётся выполнить кэширование элемента:'.$name,1);		
	}
	// Чтение кэшированного ассоциативного массива, полученного из таблицы или массива
	public function get_cache_data($name){
		if (($array=u::get_cache('table',self::$cache_live_time,$name))<0)
			throw new Exception('Не удаётся получить доступ к файлу с кэшированным ресурсом: '.$name,2);
		return $array;
	}
	

	protected function assoc_array_index_replace($array){
		$new_array=array();
		$keys=array_keys($array);
		
		$is_assoc_array=$this->is_assoc_array($array);
		
		for($i=0;$i<count($keys);$i++){
			
			$index_field=$is_assoc_array ? array(self::TEMP_INDEX =>$i) : array();
			
			
			// 
				if (is_array($array[$keys[$i]]) && $is_assoc_array){
					// Если это ассоциативный массив, превращаем его в простой
					$new_array[]=array_merge($index_field,array(self::TEMP_INDEX=>$keys[$i]),$array[$keys[$i]]);	
				}elseif (is_array($array[$keys[$i]]) && !$is_assoc_array) {
					// Если это список, который включает ассоциативные массивы
					$new_array[]=$array[$i];
				}else {
					// Если это список, который не включает ассоциативные массивы формируем условный ключ
					$new_array[]=array_merge($index_field,array(self::TEMP_VALUE=>$array[$keys[$i]]));				
				}
				
			
		}
		return $new_array;
	}
	
	protected function assoc_array_index_back($array){
		$new_array=array();
		$keys=array_keys($array);
		
		
		foreach($array as $key=>&$record){
			
			// Запись может быть удалённой, поэтому проверяем, не является ли она пустой. 
			if ($record){
				// Если это простой массив, который требуется представить как ассоциативный
				if (isset($record[self::TEMP_VALUE])){					
					//Если искусственно были созданы временные поля
					$new_array[]=$record[self::TEMP_VALUE];
				}elseif (isset($record[self::TEMP_INDEX])){					
					// Самый распространённый случай, когда ключи приходится маскировать как элемент массива
					$new_array[$record[self::TEMP_INDEX]]=array_diff_key($record,array(self::TEMP_INDEX=>''));
				}else{					
					// Редкий случай, когда был LIST и остался LIST				
					$new_array[]=$record;
				}
			}
		}
		return $new_array;
	}	
	
   public function is_assoc_array(array $array)
    {
        // Keys of the array
        $keys = array_keys($array);

        // If the array keys of the keys match the keys, then the array must
        // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        return array_keys($keys) !== $keys;
    }	
    
   public static function get_save_url($structures_names){
		$structures_names_strung='';
		$integrator='';
		foreach ($structures_names as $structure_name){
			$structures_names_strung.=$integrator.'structures[]='.$structure_name;	
			$integrator='&';
		}
		return self::$self_net_path.'?'.$structures_names_strung.'&oper=update_source&token='.self::get_token(); 
   }   

    
}
?>