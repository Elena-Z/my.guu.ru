<?php

class ji_files {
	
	
	public function output_file($fa){

		while(@ob_end_clean());
		if(ini_get('zlib.output_compression'))  {
			ini_set('zlib.output_compression', 'Off');
		}
		
		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Expires: 0");			
		header("Content-Transfer-Encoding: binary");
		header('Content-Disposition: attachment;'
			. ' filename="' . $fa['file_name'] . '";'
			. ' modification-date="' . date('r', filemtime( u::utf_win($fa['file_path_real']) ) ) . '";'
			. ' size=' . $fa['size'] .';'
			); 
		header('Content-type: '.$this->get_mime($fa['extension']));		
		
		header('Content-Length: '.$fa['size']);					
		//readfile(u::utf_win($fa['file_path_real']));
		$contents=file_get_contents(u::utf_win($fa['file_path_real']));
		echo $contents;
		die(); 		
	}
}
?>