<?php 


define('I_ERROR',-1);
define('I_INFO',0);
define('I_INFO_HEADER',1);
define('I_ERROR_LEVEL_ABORT',2);
define('I_ERROR_LEVEL_CRITIICALY',1);

define('I_ERROR_LEVEL_ORDINARY',0);

//include('u.php');
class ilog {
	
	// Используются только внутри класса
    public static $debug_enable=false;
    public static $debug_log_enable=false;
    public static $debug_path='';
    public static $debug_path_save_dir='';
    public static $log_name='log.txt';
    public static $log_permanent_writing=false;
    public static $log_content='';
    public static $errors_dir='!errors';    
    public static $display_text=false; 
    protected static $start_microtime=0;   
    public static $log_header="microtime;class;function;type;client_id;line;msg";
    
    
	protected static $instance;  // object instance
	
	public function debug($debug=false){
		
		if ($debug) 
			self::$debug_enable = true;
			
		return self::$debug_enable;
	}
	
	public function set_debug_path($path){
		self::$debug_path=$path;
	}

	public function set_log_file_name($log_file_name){
		self::$log_name=$log_file_name;
	}	
// Запись протокола  
	public function w($msg='',$type=0,$client_id=''){		
		if (!self::$debug_enable && !self::$debug_log_enable) return;
		
		$debug_info=debug_backtrace();
		$line=isset($debug_info[2]['line']) ? $debug_info[2]['line']: '';
		$class=isset($debug_info[2]['class']) ? $debug_info[2]['class']: '';
		$function=isset($debug_info[2]['function']) ? $debug_info[2]['function']: '';
		$msg=u::clean_cr_lf($msg);
		
		self::$log_content.=u::get_microtime().";$class;$function;$type;$client_id;$line;$msg"."\n";
		
		if (self::$display_text) 
			$this->output_string_text($class,$function,$type,$client_id,$line,$msg);
		if (self::$log_permanent_writing)
			$this->flush_txt();
	}

	public function flush_txt(){
		if (!self::$debug_enable  && !self::$debug_log_enable) return;
		$content=!file_exists(self::$debug_path."/".self::$log_name)? self::$log_header."\n".self::$log_content : self::$log_content;
			
		@$file=fopen(self::$debug_path."/".self::$log_name,'a+');
		@flock($file,LOCK_EX);
		@fwrite($file,$content);		
		@fclose($file);
		
		self::$log_content='';
	}
	
	
	public function output_string_text($class='',$function='',$type='',$client_id='',$line='',$msg=''){	
			echo "$class, $function, Тип: $type, ID: $client_id, Line: $line| Сообщение: $msg\r\n";
			flush();
		}
			
	// Чтение протокола
	public function get_txt_as_array(){
		
		function sort_log($a,$b) 
		{ 
		 return $a['microtime']<$b['microtime']?-1:1; 
		}
		
		$log_array=u::read_csv(self::$debug_path."/".self::$log_name);		
		
		if (!$log_array) 
			return array();
			
		usort($log_array,'sort_log');		
		
		return $log_array;
	}


	public function sql($file='undefined',$sql='',$result=array()){
		if (!self::$debug_enable) return;		
		
		$result_comment=($result && is_array($result)) ? "\n\n/* RESULT: ".count($result)." */" : '';			
		@file_put_contents(self::$debug_path."/$file.txt","$sql$result_comment");
		
	}

	public function packet($file='undefined',$content=''){
		if (!self::$debug_enable) return;		
		
		$timestamp=u::get_microtime();	
		
		@file_put_contents(self::$debug_path."/$timestamp $file.txt",$content);		
	}
	
	public function arr($file='undefined',$array=array()){
		if (!self::$debug_enable) return;		
		
		$timestamp=u::get_microtime();	
		
		@file_put_contents(self::$debug_path."/$timestamp $file.txt",var_export($array,true));		
	}

	
	public function clean_txt(){
		if (!self::$debug_enable) return;		
		@@unlink(self::$debug_path."/".self::$log_name);		
	}
	
	public function replace_debugs($error=false){
		if (!self::$debug_enable) return;		
		
		if ($error){			
			$dir_path=self::$debug_path.'/'.self::$errors_dir;
			@mkdir($dir_path);	
		}
			else {
				$dir_path=self::$debug_path;
		}
		
		$dir_name=self::$debug_path_save_dir ? $debug_path_save_dir : 'd'.u::get_microtime();
		@mkdir($dir_path.'/'.$dir_name);
		
		foreach (glob(self::$debug_path."/*.txt") as $filename){
		    @copy($filename,$dir_path.'/'.$dir_name.'/'.basename($filename));
		    if (!$error) 
		    	@unlink($filename);
		}
		
	}

	
	public function __destruct(){
		if (self::$debug_enable && !self::$log_permanent_writing)	
			$this->flush_txt();
	}
	
 
    /**
     * Защищаем от создания через new Singleton
     *
     * @return ilog
     */
    private function __construct() { 
    	
    	if (!self::$debug_path)
    		self::$debug_path=u::get_temp();
     }
 
    /**
     * Защищаем от создания через клонирование
     *
     * @return ilog
     */
    private function __clone() { /* ... */ }
 
    /**
     * Защищаем от создания через unserialize
     *
     * @return Singleton
     */
    private function __wakeup() { }
 
    /**
     * Возвращает единственный экземпляр класса
     *
     * @return ilog
     */
    public static function i() {
        if ( is_null(self::$instance) ) {
            self::$instance = new ilog;
        }
        return self::$instance;
    }
 
 
}


/* @var ilog::i() ilog */
/* @var ilog::i ilog */
/*jlog::$debug_enable=true;
jlog::$debug_path='C:/temp';
jlog::i()->w('main','6666','Message',I_INFO);
jlog::i()->w('temp','888','MEssage',I_INFO);
jlog::i()->formated_output(jlog::i()->get_txt_as_array());
jlog::i()->sql('SQL','sssss',array('f'));
jlog::i()->packet('PACKET','dfsdfsdf');
jlog::i()->replace_debugs();
*/

?>