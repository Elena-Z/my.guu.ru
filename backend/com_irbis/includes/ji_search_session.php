<?php

/*$CFG['jsession_lock_wait_timeout']=10;
require_once('../jirbis_configuration.php');
require_once('../jirbis_defaults.php');
require_once('session.php');
require_once('ji_log.php');
*/
class ji_search_session extends session {
	
	private static $rec_id_client='';	
	private static $ji_lock=JI_LOCK_FULL;
	private static $instance;  // object instance
	
	public static function read($rec_id_client='',$ji_lock=JI_LOCK_FULL){
		global $CFG;
		self::$rec_id_client=$rec_id_client;
		self::$ji_lock=$ji_lock;
		// Здесь как и в синглтоне экземепляр текщего объекта
		self::$instance = new ji_search_session(self::$ji_lock,'BROAD','ssession_',JI_PATH_TMP_LOCAL,$CFG['jsession_lock_wait_timeout'],false);
		// Ожидание -- 5 секунд
		self::$instance->jsession_lock_wait_timeout=50;
		return self::$instance->get_session();
		
	}

	public static function write($search_session_array=array()){
		if ($search_session_array) 
			self::$instance->session=$search_session_array;			
		self::$instance->write_close();
		self::free();
	}

	public static function free(){
		//ji_log::i()->w("Прекращена работа с сессией ".self::$instance->get_session_path(),I_INFO);	
		self::$instance=null;
		
	}
	
	public static function unlink_session(){
		if (self::$instance)
		@unlink(self::$instance->get_session_path());
	}
	protected function get_session_path($dummy=false){

		$session_name=abs(self::$rec_id_client);
		
		$session_path=JI_PATH_TMP_LOCAL.'/'.$this->session_prefix.$session_name;
		
		return $session_path;
	}
 	
}

/*$sd=ji_search_session::read('33333333333');
$sd['dddd']='dddddddddddddd';
ji_search_session::write($sd);*/
?>