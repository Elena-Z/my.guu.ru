<?
class rpc extends BaseJsonRpcServer{
	public $obj=null;
	public $auth=false;
	public $class='jwrapper';
	
	public function __construct($class){
		$this->class=$class;
	
		parent::__construct();
	}	
	
	public function rpc_auth($login='',$password=''){
		
		if ($login!=$GLOBALS['CFG']['irb64_user'] || $password!=$GLOBALS['CFG']['irb64_password']) 
			throw new Exception('Не верный логин или пароль JSON-RPC! Клиент не авторизован!',1);		
		$this->auth=true;
		switch ($this->class){
			case 'jwrapper':				
			default:{
				$this->obj=new jwrapper(true);					
				if ($this->obj->is_errors())
					throw new Exception($this->obj->get_last_error_message(),$this->obj->get_last_error_code());		
			}	
		}
			return true;
	}
	
	public function req_full_count($database,$request,$seq=''){
		if (!$this->auth) 
			throw new Exception('Ранее клиент JSON-RPC не был авторизован, поэтому операция не может быть выполнена',2);		
			
		$res=$this->obj->req_full_count($database,$request,$seq);			
		
		if ($this->obj->is_errors())
			throw new Exception($this->obj->get_last_error_message(),$this->obj->get_last_error_code());		
		return $res;
	}

	public function FindRecords($db_string, $search_expression, $seq = '', $first_number = 1, $portion = 0,$expired=0) {
		if (!$this->auth) 
			throw new Exception('Ранее клиент JSON-RPC не был авторизован, поэтому операция не может быть выполнена',2);		
			
		$recs=$this->obj->FindRecords($db_string, $search_expression, $seq, $first_number, $portion,$expired);			
		
		if ($this->obj->is_errors())
			throw new Exception($this->obj->get_last_error_message(),$this->obj->get_last_error_code());		

		return $recs;
	}	
	
	public function find_jrecords($db_string,  $search_expression, $seq = '', $first_number=1, $portion=0,$format_types=array(), $expired=0) {
		if (!$this->auth) 
			throw new Exception('Ранее клиент JSON-RPC не был авторизован, поэтому операция не может быть выполнена',2);		
		$format_types=object_to_array($format_types);
		$recs=$this->obj->find_jrecords($db_string, $search_expression, $seq, $first_number, $portion,$format_types,$expired);						
		
		if ($this->obj->is_errors())
			throw new Exception($this->obj->get_last_error_message(),$this->obj->get_last_error_code());		
		return $recs;
	}
	public function __call($metod,$arg){
		//file_put_contents('C:/rpc_test6.pack',var_export($metod,true));
			
		$result=call_user_func(array(&$this->obj,$metod),$arg);
		switch ($this->class){
			case 'jwrapper':				
			default:{
				if ($this->obj->is_errors())
					throw new Exception($this->obj->get_last_error_message(),$this->obj->get_last_error_code());		
			}	
		}		
		return $result;
	}
	
	
}

?>