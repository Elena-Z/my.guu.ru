<?php
require_once('ilog.php');

class ji_ilog extends ilog{
	    
	protected static $instance;  // object instance
	
	public function w($msg='',$type=I_INFO,$error_level=0,$error_number=0){		
		
		parent::w($msg,$type);		
		
		if ($type==I_ERROR){
			errors::add($error_number,$msg,$error_level);
			if ($error_level==I_ERROR_LEVEL_CRITIICALY)
				throw new Exception($msg,$error_number);
		}			
	}	

	
	public function __construct(){
		self::$debug_path=JI_PATH_DEBUG_LOCAL;
	}
	
    /**
     * Возвращает единственный экземпляр класса
     *
     * @return ji_ilog
     */
    public static function i() {
        if ( is_null(self::$instance) ) {
            self::$instance = new ji_ilog;
        }
        return self::$instance;
    }	
	
}

/* @var ilog::i() ilog */
/* @var ilog::i ilog */
/*jlog::$debug_enable=true;
jlog::$debug_path='C:/temp';
jlog::i()->w('main','6666','Message',I_INFO);
jlog::i()->w('temp','888','MEssage',I_INFO);
jlog::i()->formated_output(jlog::i()->get_txt_as_array());
jlog::i()->sql('SQL','sssss',array('f'));
jlog::i()->packet('PACKET','dfsdfsdf');
jlog::i()->replace_debugs();
*/


?>