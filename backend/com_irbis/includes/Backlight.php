<?php
/**
 * Project:     Backlight: the PHP Backlight text
 * File:        Backlight.class.php
 *
 * @link http://backlight.mypick.ru/
 * @copyright 2010 Semen Toropov, Personal.
 * @author Semen Toropov <semen@maincraft.ru>
 * @author Semen Toropov
 * @package Backlight
 * @version 1.0.0
 */
class Backlight {

    //Word Counter
    private $counter;

    public function __construct($array) {
        $this->data = $array;
    }

    //Regular expression pattern
	private function make_query($matches) {
        return '|'. preg_quote($matches) . '|iu';
    }

    private function preparyQuery($array) {
        $arrayPattern = array_map(
                array(&$this,"make_query"),
                $array
        );

        return $this->backlightSource($arrayPattern,$this->data['source']);

    }

    //Backlight words
    private function backlightSource($pattern,$source) {
        return preg_replace_callback($pattern, array($this,'replaceWords'), $source);
        ;
    }

    private function replaceWords($result,$parrams = array()) {
        $value = $result[0];
        //Update counter
        $this->counter++;
        //Apply modificators
        $value = $this->modifer($value);
        //Apply preFilters
        $value = $this->preFilter($value);
        //Format html output
        $parrams = (isset($this->data['params']['options'])) ?
                chr(32).implode(" ",$this->data['params']['options']) : '';
        //Формируем тэг с опциями
        if(isset($this->data['params']['tag'])) {
            $value = "<".$this->data['params']['tag'].$parrams.">".$value."</".$this->data['params']['tag'].">";
        }
        return $value;
    }

    //Basic Modifiers
    private function modifer($value) {
        if(!empty($this->data['modifer'])) {
            foreach($this->data['modifer'] as $key => $params) {
                $value = $this->$key($value,$params);
            }
        }
        return $value;
    }

    //Call custom function
    private function preFilter($value) {
        //Проверим существует ли функция
        if(function_exists(@$this->data['filter'])) {
            $value = call_user_func($this->data['filter'],$value);
        }
        return $value;
    }

    //Select counter
    public function countWords() {
        return $this->counter;
    }

    //Display result
    public function display() {
        return $this->preparyQuery($this->data['query']);
    }

    /*--------------------------------/
     * Basic modifiers function:
     *-------------------------------*/
    private function mb_strtoupper($string,$params = '') {
        return mb_strtoupper($string,$this->data['encoding']);
    }
    private function mb_strtolower($string,$params = '') {
        return mb_strtolower($string,$this->data['encoding']);
    }
    private function mb_substr($string,$params) {
        return mb_substr($string,$params['start'],$params['length'],$this->data['encoding']);
    }

}
?>
