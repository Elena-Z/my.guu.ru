<?php
require_once('error.php');
class errors{
	public static $array=array();
	
	public static function is_errors(){
		return (self::$array) ? true: false;
	}
	
	public static function is_critical_errors(){
		foreach (self::$array as $err){
			if ($err->error_level>0) return true;
		}		
		return  false;
	}	
	
	
	public static function add($error_number='',$error_message='',$error_level=0){
		self::$array[]=new error($error_number,$error_message,$error_level);
	}
	
	public static function add_objects_array($errors_object1=array(),$errors_object2=array()){
		if (is_array($errors_object1) && is_array($errors_object2))	
			self::$array=self::$array+$errors_object1+$errors_object2;
			
		return self::get_objects_array();
	}
	
	public static function get_objects_array(){
		return self::$array;
	}
	
	public static function get_as_messages_array(){
		$return=array();
		foreach (self::$array as $err){
			$return[]=$err->error_message;
		}		
		return $return;
	}

}


/*errors::add('ddddddddddd',444,1);
print_r(errors::get_array());
*/


/*




*/

?>