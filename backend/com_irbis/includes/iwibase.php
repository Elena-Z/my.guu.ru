<?php 

require_once("ibase.php"); 

class iwibase extends ibase{
	protected $req_count_marker='Общее количество найденных документов';


	
	
	public function __construct($host, $port_number=80, $timeout=30,$password=''){
		
		$this->host=$host;
		$this->port_number=$port_number;
		$this->timeout=$timeout;
		$this->password=$password;
		
		return true;
	}
	
	
	
	public function RecUpdate($db, $bLock, $bIfUpdate, $rec){		
		return $this->write($db,$bIfUpdate,$rec,3);
	}
	
	public function RecWrite($db, $bLock, $bIfUpdate, $rec){				
		return $this->write($db,$bIfUpdate,$rec,0);
	}

	public function get_password(){
		return $this->password; 
	}
	
	public function req_full_count($database,$request,$seq=''){
		
				
		$req['C21COM']='G';
 		$req['I21DBN']=$database;
		$req['S21All']=$request;
		$req['EXP21FMT']='TEXT';
		// устанавливается по умолчанию
		$req['EXP21CODE']='UTF8';		
		//Только для J-ИРБИС
		$req['so']='1';
		$req['option']='com_irbis';
		$req['Itemid']='300';
		$req['no_html']='1';		
		$req['UTF8']='1';
		$req['S21SCAN']=(substr($seq,0,1)==='@') ? substr($seq,1) : '' ;
		
		$result=$this->send_req($req);
		if ($result==-9995){
			$temp=$this->search_output($database,$request,$seq,1,1,'@briefweb');
			if ($temp<0) 
				return $temp;			
			if (($pos=strpos($temp,$this->req_count_marker))){
				$temp=substr($temp,$pos+strlen($this->req_count_marker),20);
			if (preg_match('{<b>(\d+)</b>}i',$temp,$res)) 
				 return (int)$res[1];				
			}
		}elseif ($result<0) 
			return $result;
		
		if (preg_match('{RESULT=(\d*)}',$result,$res)) 
			 return (int)$res[1];
	
		return  0;
	}
		 
	public function search_output($database,$request,$seq='', $first_number='1', $portion='0',$format=''){
				
		$req['C21COM']='S';
 		$req['I21DBN']=$database;
		$req['S21All']=$request;
		//Только для J-ИРБИС
		$req['so']='1';
		$req['option']='com_irbis';
		$req['Itemid']='300';
		$req['no_html']='1';		
		$req['UTF8']='1';
		$req['S21FMT']=(substr($format,0,1)==='@') ? substr($format,1) : '' ;
		$req['S21SCAN']=(substr($seq,0,1)==='@') ? substr($seq,1) : '' ;
		$req['S21STN']=$first_number;
		$req['S21CNR']=($portion) ? $portion : 99999;
		
		$result=$this->send_req($req);
		return $result;
	}
	
	
	
	public function write($db,$dic_update,$rec,$write_mode){
		if (count($rec->Content)<1) return -1;		
		
		$req=array();
		$i=0;

		foreach($rec->Content as $key=>$value){
				

			for($j=0;$j<count($value);$j++){
				$req['1_R21NUM'.(++$i)]=$key;
				$req['1_R21VOL'.$i.'_1']=$value[$j];
			}
			
		}

		$req['C21COM']='R';
		$req['I21DBN']=$db;		
		$req['1_R21UPD']=$write_mode;
		$req['1_R21IFP']=($dic_update) ? 1 : 0;
		$req['1_R21MFN']=($rec->mfn>0) ? $rec->mfn : 0;
			
		$req['S21FRAME']=(isset($this->dummy_frame_name)) ? $this->dummy_frame_name : '';
				
		return $this->send_req($req);
	}
	
	protected function send_req($req){

		$req['Z21ID']=$this->password;
		$req['SEARCH_STRING']=$this->password;
		$integrator='';		
		$full_request='';
		foreach ($req as $par=>$value){
			$full_request.=$integrator.$par.'='.u::to_uri($value);
			$integrator='&';	
		}
		
		$full_host="http://{$this->host}";
		try{
			$ch = curl_init($full_host);  
			//curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; MRA 4.4 (build 01334); DVD Owner; .NET CLR 1.1.4322; InfoPath.1)');
			//curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
			//curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
			curl_setopt($ch, CURLOPT_FAILONERROR, 1);  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
			curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout); 
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_PORT,$this->port_number);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $full_request); 
			//file_put_contents('nnnnn',$full_request);
			$result = curl_exec($ch); // run the whole process  
					
			
			if (curl_errno($ch)) 
				throw new iexception(curl_error($ch),curl_errno($ch),0);	                
			
			$info=curl_getinfo($ch);   
			if ($info['http_code']!=200)  
				throw new iexception('Ошибка HTTP: ',$info['http_code'],$info['http_code']);	          			
			if (strpos(substr($result,0,300),'Error request!')!==false)  
				throw new iexception("Ошибка WEB ИРБИС: \" ".trim(str_replace(array("\n","\r"),'',strip_tags($result)))." \"  Возможно, неверный пароль или база данных",9995,0);	                 
			
			curl_close($ch);   
			
			return $result;	
			
    	}catch(iexception $e){        				
    		$this->errors[]=$e;        	
        	return -$e->getCode();
        }
    
					
	}
	
}
/*//	public function __construct($host, $port_number=80, $timeout=30,$password=''){
$c= new iwibase('irbis.ursmu.ru/CGI/irbis32r/cgiirbis_32.exe', 80, 30);
echo $c->req_full_count('IBIS','<.>V=$<.>');

*/

?>