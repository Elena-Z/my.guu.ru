<?php
require_once('ilog.php');

class ji_log extends ilog{
    
	protected static $instance;  // object instance
	
	public function w($msg='',$type=I_INFO,$client_id='',$error_number=0,$error_level=0){		
		
		if ($type==I_ERROR)
			errors::add($error_number,$msg,$error_level);
			
		parent::w($msg,$type,$client_id);		
	}	
	
	/**
     * Форматирование и вывод протокола
     *
     * @return ji_log
     */

	//Форматирование протокола
	public function formated_output($log_array,$autonomous=true){
		if ($autonomous){
		//@header('Content-Type: html/text; charset=utf-8');	
		?>		
		<html>
			<head>
					<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
			</head>				
		<body>
		<table width="100%" border="1">		
		<?php
		}
		
		if (!is_array($log_array))
			echo 'Данные протокола отсутствуют!';
		
		$start_microtime=$log_array[0]['microtime'];		
		foreach($log_array as $c){
			
			switch ($c['type']){
				case I_ERROR: $message='<span style="background-color:red">'.$c['msg'].'</span></br>'; break;
				case I_INFO: 
				default: {
					switch ($c['class']){
						case 'broad_search_unit': $message='<span style="background-color:#ffffcc">'.$c['msg'].'</span></br>'; break;
						case 'output': $message='<span style="background-color:#ffccff">'.$c['msg'].'</span></br>'; break;
						case 'single_search_unit': $message='<span style="background-color:#d2feff">'.$c['msg'].'</span></br>'; break;
						case 'show': $message='<span style="background-color:#d4feff">'.$c['msg'].'</span></br>'; break;
						case 'provider': $message='<span style="background-color:brown">'.$c['msg'].'</span></br>'; break;
						default: 
						$message=''.$c['msg'].'';	break;
					}
				}
			}
			$microtime=floatval(($c['microtime']-$start_microtime)/10000);
			
			?>
			<tr>
				<td>
					<?php echo $microtime ?>
				</td>
				<td>
					<?php echo $c['client_id']; ?>
				</td>			


				<td>
					<?php echo $c['function']; ?>
				</td>
				
				<td>
					<?php echo $c['line']; ?>
				</td>
				
				<td>
					<?php echo I_INFO==$c['type']? '' : $c['type']; ?>
				</td>
							
				<td>
					<?php echo $message; ?>
				</td>
				
			</tr>
			<?php
		}
		if ($autonomous){
		?>
		</table>
		</body>
		</html>
		<?php
		}
	}
	
	public function __construct(){		
		self::$debug_path=JI_PATH_DEBUG_LOCAL;
	}
	
    /**
     * Возвращает единственный экземпляр класса
     *
     * @return ji_log
     */
    public static function i() {
        if ( is_null(self::$instance) ) {
            self::$instance = new ji_log;
        }
        return self::$instance;
    }	
	
}

/* @var ilog::i() ilog */
/* @var ilog::i ilog */
/*jlog::$debug_enable=true;
jlog::$debug_path='C:/temp';
jlog::i()->w('main','6666','Message',I_INFO);
jlog::i()->w('temp','888','MEssage',I_INFO);
jlog::i()->formated_output(jlog::i()->get_txt_as_array());
jlog::i()->sql('SQL','sssss',array('f'));
jlog::i()->packet('PACKET','dfsdfsdf');
jlog::i()->replace_debugs();
*/


?>