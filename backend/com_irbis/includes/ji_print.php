<?php

class ji_print extends ji_st {
	public $number_enable=true;
	public $cover_enable=false;
	
	public $default_no_cover_name='no_cover.jpg';
	public $cover_width=152;
	public $cover_height=196;
	public $covers_path_local='';
	public $levels_rtf_sizes=array('0'=>'30','1'=>'25','2'=>'25','3'=>'20','4'=>'20');

	// RECORD PRESENTATION
	//public $rp=array();
	//public $recs=array();

	public function __construct(){
		parent::__construct(true);
	}
	
	
	
	
	public function html($output){ 

		global $bo_grid_debug;
		
		
		
		if (DEBUG_BO_PRINT) $output['recs']=unserialize($bo_grid_debug);
		
		if (!count($output['recs'])) return '';		

		
		ob_start();
		
		?>
		
		<html>
			<head>
			<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
			<title>Печать записей</title>
			</head>
			<body onload="window.print(); window.close();">

			<?php			
			if ($this->selected_search_flag)
				echo '<h1>ПЕЧАТЬ ВЫБРАННЫХ ЗАПИСЕЙ</h1>';
			else {
				echo "<h2>{$this->get_req_description_html_string()}</h2>";

			}
			?>

			<form>
			<table class="records_print"> 
		
		<?php 

		$i=0;
		foreach($output['recs'] as $r){
			$i++;
			
			if (is_array($r) && isset($r['title'])){
				// Уровень тега
				$h=$r['level']+1;
				$i=0;
				?>
				<tr>
					<td colspan="2">					
					<?php echo "<h$h>{$r['title']}</h$h>" ?>
					</td>
				</tr>
			<?php 
				continue;
			}			
			?>
			
			

			<tr>
				<td>
				<?php echo $i ?>.
				</td>
				<td>
				<?php 
					echo $this->bo_html($r); 
				?>
				</td>
			</tr>
			<?php
		}
		
		?>

		</table> 
		</form>
		</body>		
		</html>

		<?php
		return ob_get_clean(); 


	}

	
	
	private function bo_html($rec){
		return $this->bo($rec);
	}

	
	public function rtf($output){ 

		global $bo_grid_debug;
		
		if (DEBUG_BO_PRINT) $output['recs']=unserialize($bo_grid_debug);
		
		if (!count($output['recs'])) return '';		

		
		ob_start();
		
		?>
{\rtf1\ansi \deff0\deflang1033

{\fonttbl{\f0\fswiss\fcharset204\fprq2 Arial Cyr;}}
{\stylesheet{\widctlpar \f0\fs20\lang1049 \snext0 Normal;}
{\s1\sb240\sa60\keepn\widctlpar \b\f0\fs28\lang1049\kerning28 \sbasedon0\snext0 heading 1;}
{\s2\sb240\sa60\keepn\widctlpar \b\i\f0\lang1049 \sbasedon0\snext0 heading 2;}
{\*\cs10 \additiveDefault Paragraph Font;}

}

\paperw11907\paperh16839\margl1134\margr1134\margt1134\margb1134 

{\colortbl;\red0\green0\blue0;\red0\green0\blue255;\red0\green255\blue255;\red0\green255\blue0;\red255\green0\blue255;\red255\green0\blue0;\red255\green255\blue0;\red255\green255\blue255;\red0\green0\blue128;
\red0\green128\blue128;\red0\green128\blue0;\red128\green0\blue128;\red128\green0\blue0;\red128\green128\blue0;\red128\green128\blue128;\red192\green192\blue192;}

<?php			
if ($this->selected_search_flag)
	echo u::utf_win('\qc\fs30 ПЕЧАТЬ ВЫБРАННЫХ ЗАПИСЕЙ');
else {
	echo u::utf_win('\qc\fs30 '.$this->get_req_description_rtf_string());

}
?>
\par\ql\fs20

<?php 

			

		$i=0;
		foreach($output['recs'] as $r){
			$i++;

			if (is_array($r) && isset($r['title'])){
				// Размер шрифта в типографских пунктах
				$size=$this->levels_rtf_sizes[$r['level']];
				$i=0;
				?>
\trowd\trqc\trgaph108\trleft-263\trhdr\trkeep 
\clbrdrt\clbrdrl\clbrdrb\clbrdrr\cellx9500
\pard \ql\widctlpar\intbl { \par \par \qc \b \fs<?php echo $size.' '.u::utf_win($r['title'])?>
\ql \b0\cell }\pard \widctlpar\intbl {    \row }\pard \qc\widctlpar <?php 
				continue;
			}			
			
?>
\trowd\trqc\trgaph108\trleft-263\trhdr\trkeep 
\clbrdrt\clbrdrl\clbrdrb\clbrdrr\cellx475
\clbrdrt\clbrdrl\clbrdrb\clbrdrr\cellx9500
\pard \ql\widctlpar\intbl {
<?php echo $i ?>.
\cell }
\pard \ql\widctlpar\intbl {
<?php 
echo u::utf_win($this->bo_rtf($r)); 
?>
\cell }
\pard \widctlpar\intbl {    \row }\pard \qc\widctlpar 
<?php
}		
?>
}
		<?php
		return ob_get_clean(); 
	}
	
	private function bo_rtf($rec){
		 return $this->html_to_rtf($this->bo($rec));
	}

	private function bo($rec){
		$result='';
		foreach ($this->rp as $format_type=>$rules){
			if ($rules['type']==0)
				$result.=$rec->GetFormatValue($format_type);
		}
	return $result;
	}
	
	private function html_to_rtf($html){
		$search=array('<b>', '</b>','<u>','</u>','<br>', '<td>', '<table>');
		$replace=array('\b ','\b0 ','\u ','\u0 ','\par ','     ','\par');
		$rtf=str_replace($search,$replace,$html);
		$rtf=strip_tags($rtf);
		return $rtf;
	}
	
	
	private function get_req_description_rtf_string(){
		$integrator='';
		$res='';
		foreach($this->get_req_description() as $e){
			$res.= $integrator.'\b '.$e['name'].'\b0:  '.$e['value'];
			$integrator=', ';
		}
		return $res;
	}

	private function get_req_description_html_string(){
		$integrator='';
		$res='';
		foreach($this->get_req_description() as $e){
			$res.= $integrator.'<b> '.$e['name'].'</b>:  '.$e['value'];
			$integrator=', ';
		}
		return $res;
	}

	private function get_req_description(){
		$res=array();
		foreach($this->fp as $key=>$value){
			if (isset($this->r[$key])){
				if (!$this->r[$key]) continue; 
				
				$title_data=(is_array($this->r[$key])) ? implode(', ',$this->r[$key]) : $this->r[$key];					
				$res[]=array('name'=>$value['title'],'value'=>$title_data);
			}
		}
		return $res;
	}
	
}


?>