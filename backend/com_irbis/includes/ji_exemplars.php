<?php

class ji_exemplars extends ji_st {
	
	public function __construct(){
		parent::__construct();
	}
	
	public function get_mhr_table(){
		global $__db;
		$sql="SELECT * FROM #__mhr 
			ORDER BY mhr_shot_name";
		idb::i()->setQuery($sql);				
		$mhr_table=idb::i()->loadAssocList('mhr_id');	
		return $mhr_table;
	}
		
// Получение массива с указанием количества экземпляров.
	public function get_ex_count($rec){
		$mhr_ex=array();
		for($i=1;$i<=count($rec->GetFieldContent(910));$i++){
			$mhr_name=$rec->GetSubField(910,$i,'D');
			$status=$rec->GetSubField(910,$i,'A');
			$mhr_name_crc32=crc32($mhr_name);
			// Если текущее место хранение не отражено в базе, значит оно не должно отображаться. 
			if (empty($mhr_ex[$mhr_name_crc32]))
				$mhr_ex[$mhr_name_crc32]=array('total'=>0,'free'=>0,'mhr_shot_name'=>$mhr_name);
				
			
			if (in_array($status,array('C','U'))){ 
					$mhr_ex[$mhr_name_crc32]['total']+=$rec->GetSubField(910,$i,'1');
					$mhr_ex[$mhr_name_crc32]['free']+=$rec->GetSubField(910,$i,'1')-$rec->GetSubField(910,$i,'2');
			}else{
				if (in_array($status,array('1','0')))
					$mhr_ex[$mhr_name_crc32]['total']++;
				if (in_array($status,array('0')))
					$mhr_ex[$mhr_name_crc32]['free']++;
			}
			
		}
		return $mhr_ex;
	}
	
// Возвращает полный массив информации о
	public function get_full_mhr_data($mhr_ex,$mhr_table){
		$mhr_merged=array();
		foreach($mhr_ex as $ex_key_crc32=>$ex_value){
			if ((@!$ex_value['free'] && $this->show_only_avalable_exemp) || @!$ex_value['total']) 
				continue;
			foreach($mhr_table as $inf_value){	
				
				if ($ex_value['mhr_shot_name']===$inf_value['mhr_shot_name']){
					$mhr_element=array();
					//$full_name=$inf_value['kaf_full_name'] ? $inf_value['kaf_full_name'] : $inf_value['mhr_full_name'];
					$mhr_element=array_merge($inf_value,$ex_value);					
					
					// Поле order_enable присутствует в таблице MySQL. Устанавливается в 0 в том случае, если ограничения касаются конкретного пользователя									
					//Если в данном месте хранения разрешен заказ. Если пользователь авторизован и  место хранения присутствует в разрешенных или если оно отсутствует в запрещенных 
/*					if (!($this->user && (($this->user->GetField(56,1) && !$this->user->SearchFieldOcc(56,$ex_value['mhr_shot_name'],'',false)) || !$this->user->SearchFieldOcc(57,$ex_value['mhr_shot_name'],'',false)))){
						$mhr_element['order_enable']=0;						
					}	
*/


					if ($this->user){
						if ($this->user->GetField(56,1) && !$this->user->SearchFieldOcc(56,$ex_value['mhr_shot_name'],'',false)){
							$mhr_element['order_enable']=0;
							$mhr_element['strict']="Нет права на обслуживание в отделе. Отдел не внесён в список доступных.";
						}elseif ($this->user->SearchFieldOcc(57,$ex_value['mhr_shot_name'],'',false)){
							$mhr_element['strict']="Обслуживание в отделе запрещено. Возможно, есть задолженности или подписан обходной лист.";
							$mhr_element['order_enable']=0;
						}
					}	
					
					$mhr_merged[]=$mhr_element;					
				}
			}
				
		}
		return $mhr_merged;		
	}
	
	//Заказ, бронирование, процедура заказа
	public function order($rec_id,$mhr,$kaf){
		global $CFG;
		$answer=array();		
		$answer['success']=true;		
		
		try{
			/* @var $rec record */	
			/* @var self::user record */	
			/* @var $c iserver64 */	
			//-------------------------------INIT-----------------------
			ji_rec_common::$st=$this;
/*			$c=new jwrapper(true,0,array('arm'=>'C'));
			//BL_ID нам небходимо задавать после получения кэшированной записи. А это мы делаем позже. Да и вообще нет смысла утежелять код.
			if ($c->is_errors()) 	
				throw new Exception("Ошибка соединения с ИРБИС-сервером".$c->get_last_error_message(),$c->get_last_error_code());					
*/			
			//Используем своё соединение для записи и для переформатирования. Неэффективно, но зато логично. 	
			//			
			//\-------------------------------INIT-----------------------
			
			if (!$this->user)
				throw new Exception('Авторизуйтесь повторно. Ваша сессия устарела.');
				
			if (($debt_bo=$this->is_debtor()) && $CFG['order_debtor_deny'])
					throw new Exception("В связи с наличием не сданной в срок литературы ($debt_bo) заказ новой запрещен");

			ji_rec_common::init_jwrapper('C');	
			$rec=ji_rec_common::get_orig_rec_by_rec_id($rec_id);
			// Используем уже созданное соединение, с BL_ID использованной записи. Некрасиво, но эффективно. 
			
			$c=ji_rec_common::$c;
			
			$user_password=$this->user->GetField($this->user_password_tag,1);
			
			if ($c->req_full_count('RQST',"(<.>RI=$user_password<.>)+(<.>RB=$user_password<.>)")>$CFG['order_max_count'])
				throw new Exception("Было сделано максимально возможное количество заказов -- {$CFG['order_max_count']}. Заказ не возможен. ");		
			//echo "(<.>RR=$user_password/{$rec->GetField(903,1)}<.>)";
			if ($c->req_full_count('RQST',"(<.>RR=$user_password/{$rec->GetField(903,1)}<.>)")>0)
				throw new Exception("Выбранное издание уже было заказано Вами. Заказ не возможен. ");		
				
			//print_r($rec);	
			// Повторная проверка на наличие свободных экземлпяров -- сейчас не выполняем, так как работаем с оригинальной записью
/*			if (!$this->find_free_ex_occ($rec,$mhr))
				throw new Exception('К сожалению, согласно последним данным свободные экземпляры в отделе отсутствуют. Заказ не возможен.');		*/
			
					
							 
			/* Пока статус бронеполки не используем -- не понятно, как действовать в случае с многоэкземплярами
			if ($rec->GetSubField(910,$occ,'A')=='0')
				$rec->SetSubField(910,$occ,'A','9');*/			
			//--------------Создание записи в базе RQST------------------------------------------
			
			$rec_rqst=new jrecord();
			//Краткое описание книги
			$bns=$this->get_bns_by_bl_id($rec->GetBl_id());
			$brief_book=$c->RecReadAndFormat($bns, $rec->GetMfn(), false, '@brief');
			//Краткое описание читателя
			$brief_reader=$c->RecVirtualFormat('RDR', '@brief',$this->user->GetAsIrbRec());
			
			$rec_rqst->AddField(30,$this->user->GetField($this->user_password_tag,1));
			//Краткое описание читателя
			$rec_rqst->AddField(31,$brief_reader);
			//метка поля КРАТКОЕ БИБ.ОПИСАНИЕ в БД RQST
			$rec_rqst->AddField(201,$brief_book);
			//метка поля ШИФР ДОКУМЕНТА в БД RQST
			$rec_rqst->AddField(903,$rec->GetField(903,1));
			//{метка поля ДАТА/ВРЕМЯ СОЗДАНИЯ ЗАКАЗА в БД RQST}
			$rec_rqst->AddField(40,date('d-m-Y H:i:s'));
			//метка поля ИМЯ БД ЭК в БД RQST}
			$rec_rqst->AddField(1,$bns);
			//{метка поля МЕСТО ВЫДАЧИ в БД RQST}
			$rec_rqst->AddField(102,($kaf ? $kaf : $mhr));
			
			ji_rec_common::rec_write('RQST',true,$rec_rqst);
			
			//\--------------Создание записи в базе RQST------------------------------------------

		}catch (Exception $e){
			$answer['error']='Возникла ошибка: '.$e->getMessage().($e->getCode()? '('.$e->getCode().')' : '');
			$answer['success']=false;
		}	
		
		return $answer;
	}

	private function find_free_ex_occ($rec,$mhr){
			//echo $mhr.'-'.$rec->GetSubField(910,1,'D');
			for($i=1;$i<=$rec->GetFieldOccCount(910);$i++){ 
				if ($rec->GetSubField(910,$i,'D')===$mhr && ($rec->GetSubField(910,$i,'A')==0 || $rec->GetSubField(910,$i,'1')-$rec->GetSubField(910,$i,'2')>0)){
					return $i;
				}
			}
		return 0;
	}
	

	private function is_debtor(){		
			for($i=1;$i<=$this->user->GetFieldOccCount(40);$i++){ 
				if ($this->user->GetSubField(40,$i,'F')==='******' && $this->user->GetSubField(40,$i,'E')<date('Ymd')){
					return $this->user->GetSubField(40,$i,'C');
				}
			}
			
		return false;
	}	
	
	public function show_ex($rec_id,$mhr_data){	
		?>
		<table class="show_ex">
		<?php
		// 1.Название отдела или кафедры выдачи
		// 2.Количество экземпляров
		// 3. Количество свободных экземпляров 
		if (!$mhr_data){
			?>
			<tr>	
				<td>
				<?php
				 echo '<span class="no_exems">Экземпляры в доступных отделах выдачи отсутствуют</span>';			  
				?>
				</td>
			</tr>
			<?php		
			return;
		}

		
		
			?>
			<tr>	
				
				<th class="ex_full_name_cell">
				Место выдачи
				</th>
			<?php			
			
			if ($this->show_total_ex){
				?>
					<th class="ex_number_cell">
					Экз.
					</th>

				<?php
			}
			
			if ($this->show_free_ex){
				?>
					<th class="ex_number_cell">
					Свободно
					</th>

				<?php
			}
	
	
			if ($this->show_order  && $this->user){
				?>	

					<th class="order_cell">
					Заказ
					</th>

				<?php
			}		
		?> 
		</tr>
		<?php 

		
		
		
		foreach($mhr_data as $mhr){
	
			?>
			<tr>	
				<td class="ex_full_name_cell">
				<?php
				 echo $mhr['kaf_full_name']? $mhr['kaf_full_name'] : $mhr['mhr_full_name'];			  
				if (!empty($mhr['comment']) || !empty($mhr['strict'])){
					 echo "<span  class=\"mhr_comment\">";
					 echo !empty($mhr['strict']) ? '<br><span class="ui-state-error ui-corner-all">'.$mhr['strict'].'</span>' : '';
					 echo !empty($mhr['comment']) ? '<br>'.$mhr['comment'] : '';
					 
					 echo '</span>';
				 }
				 //style=\"display:none\"
				?>
				</td>
			<?php
			
			if ($this->show_total_ex){
				?>

					<td class="ex_number_cell">
					<?php
					 echo $mhr['total'];
					?>
					</td>

				<?php
			}
			
			if ($this->show_free_ex){
				?>

					<td class="ex_number_cell">
					<?php
					 echo $mhr['free'];
					?>
					</td>

				<?php
			}
	
	
			if ($this->show_order && $this->user){
				?>
					<td class="order_cell">
					
					<?php
					 if ($mhr['order_enable']){
					?>
						  <input type="button" value="Заказать" class="order_button" 
						<?php
							echo "title=\"rec_id=$rec_id&mhr_shot_name={$mhr['mhr_shot_name']}&kaf_shot_name={$mhr['kaf_shot_name']}\"";
						?>					
						>
						<?php
					 }else {
					 	//echo 'Не возможен';
					 }
					 ?>
					</td>

				<?php
			}		
		?> 
		</tr>
		<?php 
		}
		
		
		?>
		</table>
		<?php
	}
	
}
