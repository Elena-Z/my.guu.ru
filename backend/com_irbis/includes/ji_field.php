<?php

class ji_field {
   // Не менять PUBLISH ни в коем случае! Иначе неправильно будет рабтать __get()!
	public $fp=array();
	
	//FIELD PROPERTIES
	public $name;
	public $req='';	
	/*
	public $truncation=true;
	public $normalisation=false;
	public $words_extraction=false;
	public $number_extraction=false;*/
	
	
	public $useful_term_preg='{[^ ,\.\;\(\)\:\!\"\'\?]+}';
	public $delim_preg='{[\xA0\xC22 ,\.\;\(\)\:\!\"\'\?]+}';
	// В связи с проблемами поддержки UTF пользуемся исключением....
	public $words_mask='{([^1234567890 ,\.\;\(\)\:\!\"\'\?]+)}';
	public $numbers_mask='{([\d]+)}';
	public $min_term_length=1;
	public $min_term_extracted_length=2;
	public $ru_enable=true;
	public $en_enable=true;
	
	public function __construct($fp){
		$this->fp=$fp;
	}
	
	public function get_field_req($name){
		$this->req='';
		$this->name=$name;
		$this->value=is_string($this->value) ? trim($this->value) : $this->value;
		$this->cvalif=(isset($this->fp[$this->name.'_cvalif']['value']) and $this->fp[$this->name.'_cvalif']['value']) ? '/('.$this->fp[$this->name.'_cvalif']['value'].')' : '';
		$this->logic=(isset($this->fp[$this->name.'_logic']['value']) and $this->fp[$this->name.'_logic']['value']) ? $this->fp[$this->name.'_logic']['value'] : $this->logic;
		$this->word_extraction=(isset($this->fp[$this->name.'_word_extraction']['value']) and $this->fp[$this->name.'_word_extraction']['value']) ? $this->fp[$this->name.'_word_extraction']['value'] : $this->word_extraction;

		if ($this->function){
			call_user_func(array(&$this,$this->function));
		}else {
			$this->default_field();
		}
		return $this->req;	
	}
	
	protected function default_field(){
		$terms_array=$this->get_terms_array();
		$this->req=$this->build_req($terms_array);
	}

	protected function get_terms_array(){
		
		$terms_array=array();

		// Извлечение слов из строки
		if ($this->break_on_parts){
			$terms_array=preg_split($this->delim_preg,$this->value);
		}else 
			$terms_array[]=$this->value;
		
			//Выделение слов или цифр из термина
		foreach($terms_array as &$term){			
			
			// Если нас интересует выделение отдельных слов, то обработка обязательна. И спецсимволов быть не должно.
			if ($this->break_on_parts){
				if (mb_strlen($term,'UTF-8')<$this->min_term_extracted_length) {
					$term='';
					continue;
				}							
			}
	
			if (mb_strlen($term,'UTF-8')<$this->min_term_length) {
				$term='';
				continue;
			}
			
			if ($this->word_extraction){				
				if (preg_match($this->words_mask,$term,$p)) 
					$term=$p[1];
				else $term='';
			}
			
			if ($this->number_extraction){				
				if (preg_match($this->numbers_mask,$term,$p)) 
					$term=$p[1];
				else $term='';
			}
						
			
			if ($this->morfology) {
				$m=new morfology($term,$this->ru_enable,$this->en_enable);
				if ($this->morfology==='normalisation')
					$term=$m->normalisation();				
				else 
					$term=$m->forms();				
				unset($m);
			}
			
		}
		sort($terms_array);
		return $terms_array;
	}
	
	protected function build_req($requests_array,$logic_special=''){
			//$this->req='';			
			// Если логика объединения не указана в параметрах функции, то пробуем брать её вначале из параметра			
			if (!$requests_array) return;	
			$logic=($logic_special) ? $logic_special : $this->logic;
			$req='';	
		foreach ($requests_array as &$term){
			if (!$term) continue;
			if (is_array($term)){
				$req.=($req ? $logic : '')."(".$this->build_req($term,'+').")";
			} else {
				$req.=($req ? $logic : '').$this->build_atomic_request($term);
			}
		}
		
		return $req;
	}
	
	protected function build_atomic_request($term,$truncation=false){
			$truncation=($truncation) ? $truncation : $this->truncation;
			$req="<.>{$this->prefix}".mb_strtoupper($term,'UTF-8').($truncation ? '$' : '')."<.>$this->cvalif";
		return $req;
	}


	
	
	
	
	
	protected function ed(){		
		$this->req="<.>{$this->prefix}$<.>";	
	}	
	
	protected function keywords(){
		$req='';
		foreach($this->get_terms_array() as $val){
			if (!$val) continue;
			if (is_array($val)){
				if (count($val)==1)
					$req.=($req ? '*' : '')."(".$this->build_atomic_request($val[0],true).")";
			 	else  
			  		$req.=($req ? '*' : '')."(".$this->build_req($val,'+').")";			
			}
		}
				
			
		$this->req=$req;	
	}
				
	protected function year1(){
		$year1=$this->get_year($this->value);
		
		if (@$this->fp['year2']['value']) {
			$year2=$this->get_year($this->fp['year2']['value']);
			$this->req="<.>{$this->prefix}$year1<.>[...]<.>{$this->prefix}$year2<.>";	
		}else 
			$this->req="<.>{$this->prefix}$year1<.>";	
			
		
	}

	protected function disc_multiselect(){		
		if (is_array($this->value)) {
			foreach ($this->value as $disc_name){
				$this->req.=($this->req ? '+' : '')."<.>{$this->prefix}$disc_name<.>";				
			}			
		}
	}		
	
	protected function grnti_multiselect(){		
		if (is_array($this->value)) {
			foreach ($this->value as $grnti){
				$this->req.=($this->req ? $this->logic : '')."<.>{$this->prefix}$grnti<.>";				
			}			
		}
	}			
	
	protected function author(){		
		$author_array=array();	
		$author_array['base']=$author_array['orig']=trim($this->value);

		

			if (preg_match('{([^,\. ]+)\,? ?([^,\. ]*)\.? ?([^,\. ]*)\.?}',$author_array['base'],$ap)){
				$author_array['base']=($ap[2]) ? $ap[1].', '.mb_substr($ap[2],0,1,'UTF-8').'.' : $ap[1];
				$author_array['base'].=($ap[3]) ? ' '.mb_substr($ap[3],0,1,'UTF-8').'.' : '' ;

				
				// Если размер второго или третьего слова больше 1 или если первый инициал включает 1 символ, а второй отсутствует
				if (mb_strlen($ap[2],'UTF-8')>1 || mb_strlen($ap[3],'UTF-8')>1 || (mb_strlen($ap[3],'UTF-8')==0 && mb_strlen($ap[2],'UTF-8')==1)   ){
					$author_array['full']=($ap[2]) ? $ap[1].', '.$ap[2] : '';
					$author_array['full'].=($ap[3]) ? ' '.$ap[3] : '' ;
				}
				
				
					$author_array=array_unique($author_array);
			}
			$this->req=$this->build_req($author_array,'+');
	}
	
	protected function isbn(){
		
			
			$isbn_array=array();
			//Заменяем некорректные X на корректный
			$isbn=$isbn_array['orig']=trim($this->value);

			
			// Если в качестве ISBN штрихкод, то
			if (is_numeric($isbn) && strlen($isbn)==13){
				$isbn_full=$isbn;
				$isbn=substr($isbn,3, 9);
			}else {
				$isbn_full=$isbn;
			}
			
			$isbn=str_replace(array('Х','х','x','-',' '),array('X','X','X','',''),$isbn);
			$isbn_array['base']=$isbn;
			$isbn_array['alt1']=substr($isbn_full,0, 3).u::sb('-',substr($isbn_full,3, 1)).u::sb('-',substr($isbn_full,4, 4)).u::sb('-',substr($isbn_full,8, 4)).u::sb('-',substr($isbn_full,12, 1));
			$isbn_array['alt2']=substr($isbn,0, 1).u::sb('-',substr($isbn,1, 3)).u::sb('-',substr($isbn,4, 5)).u::sb('-',substr($isbn,9, 1));
			$isbn_array['alt3']=substr($isbn,0, 1).u::sb('-',substr($isbn,1, 5)).u::sb('-',substr($isbn,6, 3)).u::sb('-',substr($isbn,9, 1));
			$isbn_array['alt4']=substr($isbn,0, 1).u::sb('-',substr($isbn,1, 4)).u::sb('-',substr($isbn,5, 4)).u::sb('-',substr($isbn,9, 1));
			$isbn_array['alt5']=substr($isbn,0, 3).u::sb('-',substr($isbn,3, 1)).u::sb('-',substr($isbn,4, 3)).u::sb('-',substr($isbn,7, 5)).u::sb('-',substr($isbn,12, 1));
			$isbn_array['alt6']=substr($isbn,0, 9).u::sb('-',substr($isbn,9, 1));
			$isbn_array['alt7']=substr($isbn,0, 1).u::sb('-',substr($isbn,1, 5)).u::sb('-',substr($isbn,6, 2)).u::sb('-',substr($isbn,6, 1));
			$isbn_array['alt8']=substr($isbn,0, 1).u::sb('-',substr($isbn,1, 8)).u::sb('-',substr($isbn,9, 1)).u::sb('-',substr($isbn,6, 1));
			$isbn_array['alt9']=substr($isbn,0, 3).u::sb('-',substr($isbn,3, 1)).u::sb('-',substr($isbn,4, 5)).u::sb('-',substr($isbn,9, 3)).u::sb('-',substr($isbn,12, 1));			
			$isbn_array['alt10']=substr($isbn,0, 3).u::sb('-',substr($isbn,3, 3)).u::sb('-',substr($isbn,6, 3)).u::sb('-',substr($isbn,9, 3)).u::sb('-',substr($isbn,12, 1));			
			$this->req=$this->build_req($isbn_array,'+')."+<.>HI=$isbn$<.>";
	}
	
	
	
	protected function __get($key) {
		
		if (isset($this->fp[$this->name][$key])) 
	    	return $this->fp[$this->name][$key];
	    	else 
	    	echo('Не могу найти параметр: '.$key);  
	    	return '';  
	}
	
	protected function __set($key, $value) {
	    	$this->fp[$this->name][$key]=$value;    	
	}

	private function get_year($year){
			$year=trim($year);			
			if (strlen($year)==2){									
				$year=((intval($year)>(date('y')+1))? '19' : '20').$year;
			}
			return $year;
	}
	
}


?>