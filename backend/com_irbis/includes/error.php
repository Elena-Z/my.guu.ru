<?php
class error{
	public $error_number=0;
	public $error_message='';
	public $error_level=0;

	public function  __construct($error_number='',$error_message='',$error_level=0){		
		$this->error_message=$error_message;
		$this->error_number=$error_number;
		$this->error_level=$error_level;
	}
}
?>