<?php

$GLOBALS['default_settings']=$default_settings=array(
	//'req'=>'<.>A=$<.>',
	// Параметры запроса
/*	'bl_id_array'=>array(1,2,3),
	'bl_id_string'=>'1, 2, 3',
	'bl_id_array_selected'=>array(1,2,3),
	'bl_id_string_selected'=>'1, 2, 3',
*/

	// Вот это всё должно формироваться автоматически исходя из массива баз
	'bl_id_array'=>array(1),
	'bl_id_string'=>'1',
	'bl_id_array_selected'=>array(1),
	'bl_id_string_selected'=>'1',

	'selected_search_flag'=>false,
	'rec_id_array_selected'=>array(),
	'rec_id_string_selected'=>array(),

	//'self_net_path'=>'http://localhost/jirbis2/components/com_irbis/ajax_provider.php',
	//'jirbis_local_path'=>'http://localhost/jirbis2',
	//'covers_path_local'=>'http://localhost/jirbis2/images/covers',
	
	// Простое определение переменных
	'results_pages_changes'=>array(),
	// \Простое определение переменных
	
	
	'first_number'=>1,
	'first_number_normalized'=>1,
	'portion'=>10,
	'portion_output'=>10,
	'portion_output_normalized'=>10,
	'portion_dic'=>10,
	'reformat_enable'=>true,
	'permanent_output'=>true,
	'debug_reporting'=>0,
	'sort_direction'=>'UP',
	// Текущий, действующий профиль расформатирования
	
	
	
	'profile_name'=>'full',
	'profile'=>array(),	
	
	'rp'=>array(),
	//=array( RECORD PRESENTATION все параметры вывода. Формируется с учётом пользовательских настроек в Settings)	

	'sort'=>'',
	'sort_name'=>'',	

	'permanent_search'=>true,	
	//array(bl_ID,REC_ID)
	// 
		



	//MAIN form
	'header'=>'Доступ к электронным каталогам',						
	'search_mode_description'=>'Расширенный режим поиска', //(название текущего поискового режима на естественном языке)
	'user_settings_enable'=>true,
	'progress_bar_enable'=>true,
	//(массив сортировок)
	//( возможных форматов отображения В качестве ключа -- не название файла формата, а название типа FULL BRIEF)
	'formats_chose'=>true,
	'sort_chose'=>true,
	 //(расшифровка запроса на естественном языке)
	'req_description_enable'=>true,
	'errors_reporting_enable'=>true,
	// Определяет наличие фрейма с информацией о ходе выполнения запроса.
	'debug_reporting'=>false,
	'close_afrer_search'=>false,
	'print_sort_enabe'=>true,
	'print_format_enable'=>true,
	'print_titles_enable'=>true,
	'print_send_email_enable'=>true,	
	// Поисковая форма

	'form_path'=>'',
	//(логика объединения элементов формы)
	'logic_form'=>'*',
	//(аналог $_REQUEST)
	'request_array'=>array(),
	//=array( dic_values,dic_only,field_type,(свойства полей. Массив, настраивается и создаётся пользователем) 
	//(кодовое название поискового режима)		
	'search_mode'=>'extended', 
	// Состояние формы: основное или поиск в найденном
	'form_mode'=>'main',

	// ji_bo_grid Результат вывода 
	'number_enable'=>true,
	'download_enable'=>false,
	'checkbox_enable'=>true,
	'cover_enable'=>true,
	'zebra_enable'=>true,
	'lighting_enable'=>true,
	'pager_enable'=>true,
	'print_enable'=>true,
	'this_url'=>'',
	
	
	
	
);



?>