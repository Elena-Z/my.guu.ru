jQuery(function ($) {/*
 $("td.explication").cluetip({
 splitTitle: "|",
 showTitle: true,
 cluetipClass: 'rounded',
 dropShadow: false,
 titleAttribute:'tip',
 hoverClass:'explication_selected',
 positionBy:'bottomTop'

 });*/

    if(navigator.userAgent.toLowerCase().indexOf('firefox') < 0) $("fieldset").corner("round 6px");


    $("#tabs").tabs({
        disabled: [1]
        //cookie: { expires: 100, name: 'startTab' }
    });

    $('.button').button();


/*    var dialog = {
        loading: function () {
            $("#dialog").dialog({
                width: 150,
                height: 150,
                title: 'Загрузка....'
            });

            $("#dialog").html("<img src='" + st.images_net_path + "/loading_green.gif' border=0><br><br>Загрузка данных...");
        },
        show: function (msg) {
            $("#dialog").dialog({
                width: 300,
                height: 250,
                title: 'Системная информация',
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });

            $("#dialog").html(msg);
        },
        destroy: function () {
            $("#dialog").dialog("destroy").empty();
        }
    }

//dialog.loading();
    var errors = {

        hi_output: function (errors) {
            if (errors) {
                for (i = 0; i < errors.length; i++)
                    this.log("Ошибка: " + errors[i].error_message, errors[i].error_number);
            }
        },

        lo_output: function (xhr) {
            if (!xhr) {
                errors.log("Не опознанные низкоуровневые проблемы соединения с сервером. Возможно, сервер не доступен.", 0);
            } else {
                if (!xhr.statusText)
                    errors.log("Отсутствует значение xhr.data. Возможно, проблемы с доступностью сервера.", xhr.status);
                if (xhr.statusText == 'OK')
                    errors.log("Ошибка интерпретации JSON. Возможно, непредвиденные ошибки при выполнении запроса.", xhr.status);
                else
                    errors.log("Ошибка AJAX при выполнении запроса: " + xhr.statusText, xhr.status);
            }
        },

        log: function (error_message, error_number) {
            $("#errors_cell").html($("#errors_cell").html() + "<p><span class=\"ui-state-error ui-corner-all\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span> " + error_message + "(" + error_number + ")</span></p>");
        },

        clean: function () {
            $("#errors_cell").html('');
        }
    };
*/

    $("#ji_install1").validate({
        rules: {
            user_id: {
                required: true
            },        	
            full_name: {
                required: true,
                minlength: 5
            },
            shot_name: {
                required: true,
                minlength: 2
            },

            email: {
                required: true,
                email: true
            },

            irb64_host: {
                required: true
                //,
                //url:true

            },
            irb64_port: {
                required: true,
                rangelength: [2, 5]
            },

            irb64_format_base: {
                required: true

            },
            irb64_user: {
                required: true
            },

            irb64_password: {
                required: true
            }

        },


        messages: {
            user_id: {
                required: "Укажите цифру-идентификатор библиотеки из письма поставки"
            },        	
            full_name: {
                required: "Укажите полное название библиотеки",
                minlength: "Не менее 5 символов"
            },
            shot_name: {
                required: "Укажите краткое название библиотеки",
                minlength: "Не менее 2 символов"
            },

            email: {
                required: "Укажите Email",
                email: "Адрес электронной почты введён некорректно"
            },

            irb64_host: {
                required: "Укажите адрес TCP/IP сервера ИРБИС"
                //,
                //url:"Адрес библиотеки в интернет введён некорректно"
            },

            irb64_port: {
                ralgelength: "Номер порта не может быть меньше чем из 2 цифр и не может быть больше 5"
            },
            irb64_format_base: {
                required: "Требуется указать хотя бы одну базу каталога"

            },
            irb64_user: {
                required: "Требуется указать логин для доступа к АРМу Каталогизатор"
            },

            irb64_password: {
                required: "Требуется указать пароль для доступа к АРМу Каталогизатор"
            }

        },

        submitHandler: function (form) {

            $(form).ajaxSubmit({
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    errors.clean();
                    my_dialog.loading();
                },
                success: function (data) {
                    my_dialog.destroy();
                    if (!data) {
                        errors.lo_output();
                        return;
                    }


                    if (data.errors.length)
                        errors.hi_output(data.errors);

                    if (data.success) {
                        if (!data.pft_installed)
                            my_dialog.show("<span class=\"important_msg\"> Внимание! Поскольку инсталлятору не удалось найти папку Deposit ИРБИС TCP/IP сервера, требуется вручную распаковать в неё архив с форматами! </span> <br><br><div id=\"back_to_search\"><a href=\"tmp/pft.zip\" style=\"color:blue !important\">Выгрузить архив</a></div>");

                        /*								if 	(data.errors.length)
                         dialog.show("<span class=\"important_msg\"> Обратите внимание на следующие НЕ&nbsp;критические ошибки: </span><br>"+$("#errors_cell").html());
                         */

                        $("#tabs").tabs("option", "disabled", [0]);
                        $("#tabs").tabs({ active: 1 });
                        /*
                         */
                    }

                },
                error: function (xhr) {
                    errors.lo_output(xhr);
                }

            });

        },

        errorPlacement: function (error, element) {
            error.appendTo(element.parent());
        }

    });

    //*************************ФОРМА 2******************************************************************
    $("#skip_registration").click(function () {
        my_dialog.loading();

        $.ajax({
            url: $("#ji_install2").attr('action'),
            data: $("#ji_install1").formSerialize() + '&ip_local=' + st.ip_local + '&jirbis_dir=' + st.jirbis_dir + '&skip_registration=1',
            timeout: 5,
            cache: false,
            async: false
        });

        location.reload();
    });

    $("#ji_install2").validate({
        rules: {

            url: {
                required: true,
                url: true

            },
            port: {
                required: true,
                rangelength: [2, 5]
            }

        },


        messages: {

            url: {
                required: "Укажите адрес TCP/IP сервера ИРБИС",
                url: "Полный внешний адрес J-ИРБИС"
            },
            port: {
                required: "Укажите Email",
                email: "Адрес электронной почты введён некорректно"
            }
        },

        submitHandler: function (form) {

            $.ajax({
                url: $("#ji_install2").attr('action'),
                data: $("#ji_install2").formSerialize() + '&' + $("#ji_install1").formSerialize() + '&ip_local=' + st.ip_local + '&jirbis_dir=' + st.jirbis_dir,
                type: 'POST',
                cache: false,
                dataType: 'json',
                beforeSend: function () {
                    errors.clean();
                    my_dialog.loading();
                },
                success: function (data) {
                    my_dialog.destroy();

                    if (!data) {
                        errors.lo_output();
                        return;
                    }


                    if (data.errors.length)
                        errors.hi_output(data.errors);

                    if (data.success) {
                        my_dialog.show('Поздравляем, регистрация библиотеки успешно выполнена! <br><br><div id=\"back_to_search\"><a href=\"index.php\" style=\"color:blue !important\">Выход из режима установки</a></div>');
                    }

                },
                error: function (xhr) {
                    errors.lo_output(xhr);
                }

            });

        },

        errorPlacement: function (error, element) {
            error.appendTo(element.parent());
        }

    });


});