/*
 var st={
 "self_net_path":"components/com_irbis/ajax_provider.php",
 "form_mode":"main",
 //Добавить в Settings
 "min_req_length":1,
 "keyup_delay":1,
 "max_broad_live_time":90000,
 "max_result_for_printing":2000,
 "debug":1,
 "permanent_search":1
 "max_sorted_records":2000
 };

 var def={
 "permanent_output":1,
 "recs_total":0,
 "portion_output":10,
 "portion":10,
 "first_number":1,
 "reqs_outputed":0,
 "recs_outputed":0,
 "last_output_time":0,
 "req":"",
 "finish_flag":"begin",
 "req_params":"",
 "req_id_client":"",
 "selected_search_flag":0,

 "task_broad":"search_broadcast",
 "task_renew":"show_results",
 "callback_finish_function":"search.renew_results",
 "callback_renew_function_name":"base.standart_output",
 "callback_renew_finish_function_name":"base.standart_finish"
 };
 */

var bkp = {
};

autocomplete_info = '';


jQuery.noConflict();

jQuery(function ($) {

    // Подставляем правильный относительный размер шрифта для выпадающих списков автокомплита
    (function(){
        var head = $("head"),
            body = $("body"),
            computedStyle = body.get(0).currentStyle || window.getComputedStyle(body.get(0), null),
            fontSize = parseInt(computedStyle.fontSize),
            defaultFontSize = 13;

        if(computedStyle.fontSize.indexOf("pt") > -1) fontSize = fontSize * 96 / 72;

        if(fontSize > 14){

            body.prepend($("<style>ul.ui-autocomplete{font-size: "+ Math.round((defaultFontSize/fontSize)*100)/100 +"em;}</style>"));
        }
    })();

    // Создаем объект для сохранения полей формы
    var form_transfer = new FormTransfer();



    $("#progress").progressbar();
    $("#progress_print").progressbar();

    // Отладочная строка:
    // http://localhost/jirbis2/components/com_irbis/ajax_provider.php?task=show_search_form&form_mode=main

    $('#form_div').load(st.self_net_path + '?task=show_search_form&form_mode=' + st.form_mode + '&search_mode=' + st.search_mode + '&debug_reporting=' + st.debug,
        function () {

            base.init();
        }
    );
    //$('#form_div').html('dddddddddddd');
    //user_interface.update_search_form();

    /*	$("ul.menu").treeview({
     control: "#treecontrol",
     persist: "cookie",
     cookieId: "treeview-black",
     collapsed: false
     });*/


    //================РАБОТА С МОДУЛЕМ БАЗ====================================
    $('.base_checkbox').click(function(e){
        e.stopPropagation();

        bases_module_submit();
    }).parents("li:first").click(function (e) {
            e.preventDefault();

            $(".base_checkbox", this).click();

        });

    $('#select_all').click(function () {
        $(".base_checkbox").prop('checked', true);
        bases_module_submit();
    });


    function bases_module_submit() {
        $('#bases_module').ajaxSubmit({
            cache: false,
            success: function (answer) {
                if (answer)
                    alert(answer);
                else
                    user_interface.update_search_form();
                if (search.results_present())
                    base.new_search(true);
            }
        });
    }

    //================/РАБОТА С МОДУЛЕМ БАЗ====================================


    //===============РАБОТА ФОРМОЙ НАСТРОЕК ПОЛЬЗОВАТЕЛЯ=========================================

    $('#set_selected_user_profile').change(function () {
        user_interface.profile_submit();
    });

    //===============/РАБОТА ФОРМОЙ НАСТРОЕК ПОЛЬЗОВАТЕЛЯ=========================================

    //===============ПОКАЗ ВЫБРАННЫХ ЗАПИСЕЙ======================================================

    $('#selected_total').load(st.self_net_path + '?task=get_selected_count');

    $('#show_selected_records, #basket_icon').click(function () {
        base.show_selected();
    });

    $('#clean_selected').click(function () {
        $('#selected_total').load(st.self_net_path + '?task=clean_selected');
        $(".code_checkbox:checked").prop('checked', false);
    });


    //===============/ПОКАЗ ВЫБРАННЫХ ЗАПИСЕЙ======================================================


    //===============ФОРМА ПЕЧАТИ======================================================
    $('#print_button').button();
    $('#print_button').click(function () {
        print.new_print();
    });

    $("input[name=print_output]").change(function () {
        if ($(this).val() == 'email') {
            $('#email_div').css('display', '');
        }
        else {
            $('#email_div').css('display', 'none');
        }
    });


    //===============/ФОРМА ПЕЧАТИ======================================================


    //=================================================================================================
    //=================================================================================================
    //=====================================ПРОСТО ИНИЦИАЛИЗАЦИЯ ФОРМЫ==================================
    //=================================================================================================
    //=================================================================================================


    //=================================================================================================
    //=================================================================================================
    //=====================================BASE======================================================
    //=================================================================================================
    //=================================================================================================

    //================БАЗОВЫЕ ОБРАБОТЧИКИ====================================
    var base = {
        // Инициализирует обрабочики после подгрузки поисковой формы
        init: function () {

            search_form.init_start();


            //dragAndDrop: false


            /*				if ($("#black")){
             $("#black").treeview({


             ajax: {
             data: {
             "task":"grnti",
             "rub": function() {
             return "";
             }
             },
             type: "get",
             cache: false,
             async: true,
             complete: function(){
             $("#form_accordion").accordion("destroy");
             $("#form_accordion").accordion();
             },

             error:   function (xhr){
             error_log("Ошибка AJAX при определении количества найденных записей: "+xhr.statusText,xhr.status);
             }



             },
             url: st.self_net_path
             });
             }
             */
            ///Инициализация ГРНТИ рубрикатора, если требуется
            //$('select').combobox();



        },

        // Обработчик нажатия клавиш
        permanent_search: function () {
            var modified_flag = 0;
            $(this).everyTime(st.keyup_delay + "s", "key_timer", function () {
                st.req_form = base.get_form_params();
                if (req_old == st.req_form && modified_flag) {

                    if ((st.req_form.length - default_req_length) >= st.min_req_length) {

                        if (!$(".base_checkbox:checked").length && $(".base_checkbox").length) {
                            alert('Необходимо выбрать хотя бы одну базу для поиска! ');
                        } else {
                            base.new_search();
                            req_old = st.req_form;
                            //alert(req_new);
                        }
                    } else {

                        base.clean_output();
                        $("#strict").html("Для поиска введите как минимум " + st.min_req_length + " символ(ов)");

                    }
                    modified_flag = 0;
                }

                if (req_old != st.req_form)
                    modified_flag = 1;
                req_old = st.req_form;
            });

        },

        // Очистка результата вывода
        clean_output: function () {
            base.clean_output_page();
            user_interface.progress_bar(0, '');
            $("#paginator").html('');
            $("#show_results").html("");
            $("#errors_cell").html("");
            $("select.sort_select option[value='']").prop("selected", true);

        },

        // Очистка результата вывода
        clean_output_page: function (full) {
            user_interface.foging_results(1);
            $("#req_description_cell").html('');
            $("#debug_messages").html('');
            $("#strict").html('');
            user_interface.progress_bar(0, '');
        },

        new_search: function () {

            //--------СВОРАЧИВАНИЕ ФОРМЫ-----------
            if (st.reduce_afrer_search) {
                $("#form_accordion").accordion({
                    active: false,
                    collapsible: true,
                    heightStyle: "content",
                    autoHeight: false
                });
            }
            //--------/СВОРАЧИВАНИЕ ФОРМЫ-----------
            user_interface.search_indicator(1);
            base.clean_output();

            search.set_to_default();
            search.new_req_id_client();
            search.req_params = base.get_form_params();

            search.start_broad();

        },


        new_page: function () {
			self.scrollTo(0, 0);
            user_interface.foging_results(1);
            user_interface.search_indicator(1);


            search.set_to_default_portion();
            base.clean_output_page();

            search.start_broad();
            search.save_js_session();

        },
        reload_search: function () {
            if (st.req_form) {
                user_interface.search_indicator(1);
                base.clean_output();
                search.set_to_default();
                search.new_req_id_client();

                search.req_params = st.req_form;

                search.start_broad();
            }
        },

        show_selected: function () {
            user_interface.search_indicator(1);
            base.clean_output();
            search.set_to_default();
            search.new_req_id_client();
            //search.req_params='selected_search_flag=true';

            search.task_broad = 'search_broadcast_selected';
            //search.callback_finish_function="search.renew_results";
            search.permanent_output = 0;
            search.selected_search_flag = 1;
            search.start_broad();

            search.save_js_session();

        },


        hand_start: function () {
            if (!st.permanent_search)
                base.new_search();
        },

        standart_output: function (data) {
            
            user_interface.foging_results(0);
            //alert(total_portions);
            user_interface.progress_bar(data.percent, '');
            //  || !data.recs_total
            user_interface.show_results_info();


            if (data.recs) {

                $("#show_results").html(data.recs);
                init_results();

            }


            //Обработка проблемной ситуации, когда фактически полученное количество записей меньше ожидаемого. С учётом новых данных перерисовывается paginator
            if (data.first_number_normalized >= search.recs_total && !search.recs_outputed && search.finish_flag == 'last' && search.first_number != 1) {
                if (search.recs_total > search.portion_output) {
                    //search.first_number=1;
                    user_interface.pagination();
                }
                //base.new_page();

            }

            // ДУБРИРУЕТСЯ ВЫШЕ!!!!!!!!!!!!
            if (search.first_number == 1 && search.finish_flag == 'last' && search.recs_total)
                user_interface.pagination();

            //if (search.finish_flag=='last' && search.recs_total<search.first_number_normalized)


            //  if (search.finish_flag=='last')
            //   	$("#sort_select").val('');
        },

        standart_finish: function (data) {

            if (search.recs_total > st.max_sorted_records) {
                $("select.sort_select").prop("disabled", true);
            } else {
                $("select.sort_select").prop("disabled", false);
            }
            user_interface.search_indicator(0);
            load_debug_log();
        },


        get_form_params: function () {
            params = '';
            if ($('#grnti')) {
                $('.jstree-checked').each(function () {

                    params += (params ? '&' : '') + 'grnti_multiselect[]=' + $(this).attr('id');
                });
            }

            params += (params ? '&' : '') + $("#ji_form").formSerialize();

            return params;
        }





    };
    //================/БАЗОВЫЕ ОБРАБОТЧИКИ====================================


    //=================================================================================================
    //=================================================================================================
    //=====================================PRINT======================================================
    //=================================================================================================
    //=================================================================================================


    var print = {
        // Запускается при нажатии кнопки печати
        new_print: function () {
            //var print_output=;

            if (!search.results_present() && $('#print_conditions').val() == 'all') {
                alert("Для распечатки всех найденных записей требуется выполнить поиск!");
                return;
            }

            //nn=$('#show_results').val();
            //pp=parseInt(search.recs_total);

			// TODO Определять количество выделенных записей с помощью запроса -- вынужденная мера, так как модуль корзины может отсутствовать
            if ($('#print_conditions').val() == 'checked') {
                var selected_count = $.ajax({
                    url: st.self_net_path,
                    data: 'task=get_selected_count',
                    cache: false,
                    async: false,
                    error: function (xhr) {
                        errors.lo_output(xhr,'определения количества найденных записей');
                    	//error_log("Ошибка AJAX при определении количества найденных записей: " + xhr.statusText, xhr.status);
                         
                    }
                }).responseText;

                //alert(selected_count);
                if (parseInt(selected_count) < 1) {
                    alert("Для распечатки отмеченных записей требуется выделить записи!");
                    return;
                }
            }

            if ($("input[name=print_output]").val() == 'email') {

                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

                if (!pattern.test($('#email').val())) {
                    alert("Укажите корректный E-mail");
                    return;
                }
            }


            search.task_renew = 'print_results';
            search.callback_renew_function_name = "print.output_indicators";

            if ($("#print_conditions").val() == 'all') {


                //$("#progress_bar_print_table").css('visibility','show');
                user_interface.progress_bar(0, '_print');
                //base.clean_output();
                //search.set_to_default();
                //search.new_req_id_client();
                search.new_req_id_client();
                //base.clean_output_page();
                search.set_to_default_portion();
                search.permanent_output = 1;

                //search.callback_finish_function_name="search.renew_results";				
                search.task_renew = 'print_results';
                search.callback_renew_function_name = "print.output_indicators";

                search.req_params = '&portion=' + st.max_result_for_printing + '&portion_output=' + st.max_result_for_printing + '&' + st.req_form + '&' + $("#print_form").formSerialize();

                search.start_broad();

            } else {


                //base.clean_output();
                search.set_to_default();
                search.new_req_id_client();
                search.permanent_output = 0;
                search.selected_search_flag = 1;

                search.task_renew = 'print_results';
                search.callback_renew_function_name = "print.output_indicators";

                search.req_params = $("#print_form").formSerialize();

                search.task_broad = 'search_broadcast_selected';

                search.start_broad();
            }

        },
        output_indicators: function (data) {
            user_interface.progress_bar(data.percent, '_print');

            if (data.error) {
                my_dialog.show(data.error.error_message + '(' + data.error.error_number + ')');
                return;
            }

            if (data.file_name)
                print.file_output(data.file_name);
        },
        file_output: function (file_name) {
        	
        	//TODO: вот это обязательно заменить на путь из сессии
            var file_net_path = 'tmp/' + file_name;
            switch ($("input[name=print_output]:checked").val()) {
                case 'printer':
                    x = 640;
                    y = 480;
                    cx = screen.width / 2 - (x / 2);
                    cy = screen.height / 2 - (y / 2);
                    var load_window = window.open(file_net_path, 'Records_Printing', 'status=no,location=no,directories=no,toolbar=no,scrollbars=yes,resizable=yes,width=' + x + ',height=' + y + ',top=' + cy + ',left=' + cx);
                    if (typeof(load_window) == 'object')
                        load_window.focus();

                    break;
                case 'word':
                    window.location = file_net_path;
                    break;
                case 'email':
                    my_dialog.show('<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"> <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 				Отправка файла со списком по адресу ' + $('#email').val() + ' выполнена				</div>');
                    break;


            }
            //
            search.restore_js_session();
            search.finish_flag = 'finish';
            user_interface.progress_bar(100, '_print');
            
            // После смены профиля автоматом выполняется смена контекста
           // В Chrome повторный поиск приводит к ошибкам , поэтому мы его не выполняем.

            
           	if (/chrome/.test(navigator.userAgent.toLowerCase())){
           		user_interface.profile_submit(1);             
           		base.clean_output();
           	}else {
           		user_interface.profile_submit();             
           	}          
        
           	
           
            //$("#progress_bar_print_table").css('visibility','hidden');
        }


    };

    //=================================================================================================
    //=================================================================================================
    //=====================================/PRINT======================================================
    //=================================================================================================
    //=================================================================================================

    //=================================================================================================
    //=================================================================================================
    //=====================================SEARCH======================================================
    //=================================================================================================
    //=================================================================================================
    //=============================НИЗКОУРОВНЕВЫЕ ФУНКЦИИ ПОИСКА И ОТОБРАЖЕНИЯ==========================
    var search = {
        start_broad: function () {

            // Прерывание запроса стабильно вызывает ошибку в IE
            if (typeof(broad) == 'object')
                broad.abort();

            if (typeof(renew) == 'object'){
                renew.abort();
                search.finish_flag ='begin';
            } 


            debug_session();

            //selected=(search.selected_search_flag)?  'true' :'';


            if (search.permanent_output) {
                search.renew_results();
                //eval(search.callback_renew_function_name+'();');
            }

            broad = $.ajax({
                url: st.self_net_path,
                data: search.req_params + '&task=' + search.task_broad + '&first_number=' + search.first_number + '&req_id_client=' + search.req_id_client + '&selected_search_flag=' + search.selected_search_flag,
                cache: false,
                type: 'POST',
                dataType: 'json',
                timeout: st.max_broad_live_time,
                beforeSend: function () {
                },
                success: function (data) {
                    // Если завершился актуальный, а не старый запрос.
                    //debug('data'+data+'data.req_id_client'+data.req_id_client+'  st.req_id_client'+st.req_id_client);
                    // Страховка от получения данных старым запросом, который не был прерван

                    if (data) {
                        //error_log('Пустой ответ на BROAD запрос. Возможно, не доступна сеть',1);

                        // В случае, если JSON ответ не получен, но данные есть.

                        /*							if (typeof(data)!='object') {
                         error_log('Ошибка интерпретации данных JSON при выполнении BROAD запроса. ',1);
                         search.finish_flag='last';
                         }	*/

                        if (data.req_id_client == search.req_id_client) {
                            //error_log("data.req_id_client="+data.req_id_client+"search.req_id_client="+search.req_id_client,1);

                            search.finish_flag = 'last';
                            // Устанавливаем флаг: последний опрос и запускаем в последний раз обновление результатов
                            //search.renew_results();
                            if (!search.permanent_output) {
                                eval(search.callback_finish_function + '(data);');
                            }


                        }

/*                        if (data.errors) {
                            for (i = 0; i < data.errors.length; i++)
                                error_log("Ошибка: " + data.errors[i].error_message, data.errors[i].error_number);
                        }
*/                     if (data.errors) 
                    		errors.hi_output(data.errors,'при выполнении основного поискового запроса');
                    }

                },
                error: function (xhr) {
                    search.finish_flag = 'last';
                    errors.lo_output(xhr,'broad_search');
/*                    if (xhr.statusText == 'abort') return;
                    else if (!xhr.statusText)
                        error_log("Отсутствует значение xhr.statusText при выполнении обработки ошибки broad_search", xhr.status);
                    else if (xhr.statusText == 'OK')
                        error_log("Ошибка интерпретации JSON при выполнении broad_search", xhr.status);
                    else
                        error_log("Ошибка AJAX при выполнении broad_search: " + xhr.statusText, xhr.status);
*/                }

            });


        },


        renew_results: function () {
            /*	debug("На входе в фунцию RENEW_RESULTS finish_flag: "+st.finish_flag);*/

            if (st.finish_flag == 'finish')
                return;
			//alert(st.max_broad_live_time);
                
            renew = $.ajax({
                url: st.self_net_path,
                data: 'task=' + search.task_renew + '&req_id_client=' + search.req_id_client + '&first_number=' + search.first_number + '&recs_outputed=' + search.recs_outputed + '&reqs_outputed=' + search.reqs_outputed + '&last_output_time=' + search.last_output_time + '&finish_flag=' + search.finish_flag,
                cache: false,
                dataType: 'json',
                async: true,
                timeout: st.max_broad_live_time,
                beforeSend: function () {
                },
                success: function (data) {
                    if (data && data.req_id_client == search.req_id_client) {
                        // В том случае, если запрос возвращает записи или результат нулевой по новому запросу.
                        // Если запрос новый, то отсчёт должен выполняться с нуля
                        //if (data.recs || !data.recs_total)


                        // Параметры, важные для вывода
                        search.recs_outputed = data.recs_outputed;
                        //alert(search.recs_outputed);
                        search.reqs_outputed = data.reqs_outputed;
                        //debug(search.recs_outputed);
                        search.last_output_time = data.last_output_time;
                        // \Параметры, важные для вывода

                        search.req = data.req;
                        search.recs_total = data.recs_total;
                        search.reqs = data.reqs;
                        //search.first_number=data.first_number;
                        //search.percent=data.percent;
                        //search.portion=data.portion;
                        //search.portion_output=data.portion_output;


                        //var last_portion = search.recs_total%search.portion_output;


                        eval(search.callback_renew_function_name + '(data);');


                        // Если все запросы выполнены
                        /*				if (search.reqs_outputed==search.reqs || !search.reqs)
                         search.finish_flag='finish';
                         */

                        // Рекурсивно запускаем функцию обновления(чтобы избежать дублирования процессов -- только один раз)

                        /*Если в момент завершения BROAD RENEW уже был запущен, то необходимо запустить ещё один,
                         последний запрос для "выгребания остатков" */

                        if (search.finish_flag != 'finish' && search.permanent_output) {

                            //if (search.reqs_outputed<search.reqs)
                            renew = search.renew_results();
                        } else {
                            eval(search.callback_renew_finish_function_name + '();');
                            return;
                        }

                        //alert(search.permanent_output);

                        //if (search.finish_flag=='last')
                        //search.finish_flag='finish';

                    } else {
                        if (search.finish_flag != 'finish' && search.permanent_output)
                            renew = search.renew_results();
                    }

                },
                error: function (xhr, ajaxOptions, thrownError)  {
                	search.finish_flag = 'finish';
                	errors.lo_output(xhr,'renew_results');
                	//console.log(xhr);
                	//console.log(thrownError);
                   /* if (xhr.statusText == 'abort') return;
                    else if (!xhr)
                        error_log("Отсутствует значение xhr.statusText при выполнении обработки ошибки renew_results", xhr.status);
                    else if (xhr.statusText == 'OK')
                        error_log("Ошибка интерпретации JSON при выполнении renew_results", xhr.status);
                    else
                        error_log("Ошибка AJAX при выполнении renew_results: " + xhr.statusText, xhr.status);*/

                },
                complete: function (data) {
                    if (search.finish_flag == 'last')
                        search.finish_flag = 'finish';
                    //debug('RENEW: data'+data+'data.req_id_client'+data.req_id_client+'  search.req_id_client'+search.req_id_client);
                }

            });
        },


        // Очистка результата вывода
        set_to_default: function () {
            $.extend(search, def);

            /*
             search.recs_total=def.recs_total,
             search.portion_output=def.portion_output,
             search.portion=def.portion,
             search.first_number=def.first_number,
             search.reqs_outputed=def.reqs_outputed,
             search.recs_outputed=def.recs_outputed,
             search.last_output_time=def.last_output_time,
             search.req=def.req,
             search.finish_flag=def.finish_flag,
             search.permanent_search=def.permanent_search,

             search.task_broad=def.task_broad,
             search.task_renew=def.task_renew,
             search.callback_finish_function=def.callback_finish_function,
             search.callback_renew_function_name=def.callback_renew_function_name

             */


        },
        // Очистка результата вывода
        set_to_default_portion: function () {

            search.recs_total = def.recs_total,
                search.reqs_outputed = def.reqs_outputed,
                search.recs_outputed = def.recs_outputed,
                search.last_output_time = def.last_output_time,
                search.finish_flag = def.finish_flag;
        },

        new_req_id_client: function () {
            myDate = new Date();
            search.req_id_client = crc32(search.req_params + myDate.getTime());

        },

        save_js_session: function () {

            $.extend(bkp, search);

        },

        restore_js_session: function () {

            //if (bkp){
            $.extend(search, def);
            //$.extend(bkp,{});
            //}

        },
        results_present: function () {
            if (search.recs_total >= 1 && !search.selected_search_flag && search.req)
                return  1;
            else
                return 0;
        }


    }

    search.set_to_default();
    //=============================\НИЗКОУРОВНЕВЫЕ ФУНКЦИИ ПОИСКА И ОТОБРАЖЕНИЯ==========================


    function debug(str) {
        if (st.debug) {
            $("#debug_messages").html($("#debug_messages").html() + "<br>" + str);
        }
    }


    function load_debug_log() {
        if (st.debug) {
            $('#debug_log')[0].src = st.self_net_path + '?task=show_log';
        }
    }


    function debug_log(message) {
        if (st.debug) {
            $("#debug_messages").html($("#debug_messages").html() + "<br>" + message);
        }
    }

    function debug_session() {
        if (st.debug) {
            $.each(search, function (key, val) {
                if (typeof(val) != 'function')
                    debug_log('<b>' + key + '</b>' + ': ' + val);
            });
            $.each(st, function (key, val) {
                if (typeof(val) != 'function')
                    debug_log('<b>' + key + '</b>' + ': ' + val);
            });
        }
    };


    $("#clean_cache").click(
        function () {
            $("#debug_messages").load(st.self_net_path + '?task=clean_cache', function () {
            });
        }
    );


    $("#clean_session").click(
        function () {
            $("#debug_messages").load(st.self_net_path + '?task=clean_session', function () {
            });
        }
    );


    $("#clean_js_session").click(
        function () {
            search.set_to_default();
        }
    );


    $("#show_session").click(
        function () {
            window.open(st.self_net_path + '?task=show_session', 'SESSION');
        }
    );
    $("#show_log").click(
        function () {
            window.open(st.self_net_path + '?task=show_log', 'SESSION');
        }
    );


    //=================================================================================================
    //=================================================================================================
    //=====================================SEARCH FORM==============================================
    //=================================================================================================
    //=================================================================================================

    // Концепция --- есть init_start и init_finish. init_start  вызывает цепочку асинхронных функций, которые через callback вызывают init_finish.
    var search_form = {

        init_start: function () {
            //----------------- ВНЕШНИЙ ВИД -----------------------------
            $(".accordion").accordion({
                collapsible: true,
                active: false,
                heightStyle: "content",
                autoHeight: false

            });

            $("#form_accordion").accordion({
                collapsible: true,
                heightStyle: "content",
                autoHeight: false
            });

            //$("#form_accordion").accordion( "resize" );
            //$("#user_settings_accordion").accordion( "resize" );

            $(".main .button").button();
            //\----------------- ВНЕШНИЙ ВИД -----------------------------

            search_form.autocomplete(search_form.init_finish);
        },

        init_finish: function () {
            /*				$("select").change(function(){
             console.log('ВыПОЛНЕНО!');
             $('#disc_multiselect').trigger('reloadGrid');
             });	*/




            default_req_length = base.get_form_params().length;

            function updateDisc(){
                $('#disc_multiselect').setGridParam({
                    url: st.self_net_path + '?task=get_options&dic_function=get_disc_multiselect&prf=DISC=&' + base.get_form_params()
                }).trigger('reloadGrid');
            }


            $('.float_select').combobox("option",{select: function( event, ui ) {

                updateDisc();

                return true;
            }});

            // ставим слушатель
            if (st.permanent_search) {
                req_old = "";
                base.permanent_search();
            }

            if(form_transfer.saved) form_transfer.transfer($('#form_div'));

            var callback = function(){
                if(form_transfer.saved) form_transfer.transferCheckboxes($('#form_div'));
            }

            search_form.grnti(callback);
            search_form.disc_multiselect(callback);
            search_form.icons();

            $("#search_button").click(function () {
                base.hand_start();
            });

            $("#ji_form").submit(function () {
                base.hand_start();
                return false;
            });


        },

        autocomplete: function (callback) {
            //-----------------------Автодополнение и заполнение выпадающих списков--------------------
            combobox_presint_flag = 0;
            if (autocomplete_info) {
                var dic_finished = 0;


                $.each(autocomplete_info, function (name, params) {
                	
                
                    if (params.field_type=='autocomplete'){
                        $( "#"+name ).autocomplete({
                            source: st.self_net_path+'?task=get_terms&prefix='+params.prefix,
                            minLength: 2
                        });
                    }

                    if (params.field_type == 'combobox') {
                    	
                    	// Если не определена специальная функция для вывода данных, берём их из словаря так же, как для автодополнения 
                        if (params.dic_function=='preload'){
							$("#" + name).combobox({
								source: st.self_net_path+'?task=get_terms&prefix='+params.prefix+'&portion_dic=1000',
								preload:true
							});                    	
                        }else{                     	
	                        $("#" + name).combobox();
	                    	combobox_presint_flag = 1;
	                        // Количество загруженных комбобоксов.
	                        dic_finished++;
	
	                        // Если пользователь задал какое-то значение -- лучше его использовать, а не грузить новые
	                        if (!$("select[id=" + name + "] option").size()) {
	                            $.ajax({
	                                url: st.self_net_path,
	                                data: 'task=get_options&dic_function=' + params.dic_function + '&prefix=' + params.prefix,
	                                cache: false,
	                                dataType: 'json',
	                                beforeSend: function () {
	
	                                },
	                                success: function (data) {
	
	                                    $.each(data, function (key, value_obj) {
	                                        $("#" + name).combobox("addOption",  value_obj.description, value_obj.value, value_obj.selected);
	                                    });
	
	                                    dic_finished--;
	                                    // Если количество запущенных запросов и количество выполненных одинаково
	                                    //console.log("Успешное завершение для "+name+" dic_finished: "+dic_finished);
	                                    if (dic_finished == 0) {
	                                        callback && callback();
	                                    }
	                                },
	                                error: function (xhr) {                                   
	                                	errors.lo_output(xhr,'терминов для выпадающего списка');	                                	
	                                	//error_log("Ошибка AJAX при запросе терминов для выпадающего списка: " + xhr.statusText, +"(" + xhr.status + ")");
	                                },
	                                complete: function () {
	                                    /*										dic_finished--;
	                                     if 	(dic_finished==0)
	                                     eval(callback_function+';');*/
	                                }
	
	                            });
	                        }else {
	                            dic_finished--;
	                            // Если количество запущенных запросов и количество выполненных одинаково
	                            //console.log("Успешное завершение для "+name+" dic_finished: "+dic_finished);
	                            if (dic_finished == 0) {
	                                callback && callback();
	                            }
	                        }
	
	                    }
                    }
                });

                    
            }

            // Если комбобоксы не найдены и есть только автодополнение.
            if (!combobox_presint_flag)
                callback && callback();

        },
        //\-----------------------Автодополнение и заполнение выпадающих списков--------------------

        //-----------------------ВИДЖЕТ ВЫБОРА ДИСЦИПЛИН -- СИНХРОННЫЙ--------------------
        disc_multiselect: function (load_complete_callback) {
            $("#disc_multiselect").jqGrid({
                url: st.self_net_path + '?task=get_options&dic_function=get_disc_multiselect&prf=DISC=&' + base.get_form_params(),
                datatype: "json",
                colNames: ['Дисциплина'],
                colModel: [
                    {name: 'disc_name', index: 'disc_name', width: 300}
                ],
                rowNum: 2000,
                autowidth: true,
                sortname: 'disc_name',
                recordpos: 'left',
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                caption: "Выбор дисциплин",
                loadComplete: function () {
                    load_complete_callback && load_complete_callback();

                    $('input.cbox').each(function (i) {
                            temp = $(this).prop('name');
                            disc_name = temp.split('_')[3];
                            $(this).val(disc_name);
                            $(this).attr('name', 'disc_multiselect[]');
                        }
                    );
                },
                ajaxOptions: function () {
                    async: false
                }
            });
        },
        //\-----------------------ВИДЖЕТ ВЫБОРА ДИСЦИПЛИН--------------------


        //--------------------------ГРНТИ СИНХРОННЫЙ------------------------------------
        grnti: function (load_complete_callback) {
            //$("#form_accordion").accordion("destroy");
            //$("#form_accordion").accordion();
            checked_ids = '';
            //$("#grnti").attr('data-url',st.self_net_path+'task=grnti');
            $.jstree._themes = st.js_net_path + "/themes/";
            $('#grnti').jstree({
                json_data: {
                    ajax: {
                        url: st.self_net_path + '?task=grnti',
                        data: function (n) {
                            return {
                                operation: "get_children",
                                id: n.attr ? n.attr("id") : 0
                            };
                        },
                        async: false,
                        complete: function () {
                            load_complete_callback && load_complete_callback();
                            //form_transfer.transferJstree($('#form_div'),$('#grnti'));
                        }

                    }
                },


                plugins: [ "themes", "json_data" , "checkbox", "ui" ],

                themes: {

                    theme: "classic",
                    dots: true,
                    icons: true
                }
            }).bind("open_node.jstree close_node.jstree", function (event, data) {
                    //alert("");
                    /*								$("#grnti").jstree("get_checked",null,true).each(function () {

                     alert("id чекед элементов - "+data.rslt.obj.attr("id"));
                     });

                     $('.jstree-checked,.jstree-undetermined').each(function(){

                     checked_ids+=','+$(this).find('a').attr('id');
                     });
                     alert(checked_ids);
                     */
                    /*						checked_ids='';
                     $("#grnti").find(".jstree-checked").each(function(i,element){
                     checked_ids+=','+$(element).attr("id");
                     }); */
                    // alert(checked_ids);


                });
        },
        //--------------------------ГРНТИ------------------------------------

        //-----------НОВЫЕ ПОСТУПЛЕНИЯ "СИСТЕМЫ ДОСТУПА"-----------------------------
        icons: function () {

            // Обработка ссылки с ГРНТИ -- форма автоматически сворачивается
            $(".subj_div").click(function () {
                $("#form_accordion").accordion({
                    active: false,
                    collapsible: true,
                    heightStyle: "content",
                    autoHeight: false
                });

                base.clean_output();
                search.set_to_default();
                search.new_req_id_client();
                st.req_form = 'grnti=' + $(this).attr('title') + '&year1=2010&year2=2011';
                search.req_params = st.req_form;
                search.start_broad();
            });


            $("TD.subject_cell").mouseover(function () {
                $(this).css('background-color', '#B56D6D')
            });

            $("TD.subject_cell").mouseout(function () {
                $(this).css('background-color', '#FFFFFF')
            });
        }
        //-----------НОВЫЕ ПОСТУПЛЕНИЯ "СИСТЕМЫ ДОСТУПА"-----------------------------


    }


    //=================================================================================================
    //=================================================================================================
    //=====================================SEARCH FORM==============================================
    //=================================================================================================
    //=================================================================================================

    //=================================================================================================
    //=================================================================================================
    //=====================================USER INTERFACE==============================================
    //=================================================================================================
    //=================================================================================================

    var user_interface = {
        _fog_div: $("<div style='position:absolute; top: 0; left: 0; right: 0; bottom: 3px; background: #fff; opacity: 0.7; filter:alpha(opacity=70);'></div>").appendTo("#fog_box"),
        
		profile_submit:function(quite) {

	         var answer= $.ajax({
			                        url: st.self_net_path,
			                        data: 'task=set_selected_user_profile&' + $("#set_selected_user_profile").formSerialize(),
									dataType: 'text',
									async: false,
			                        cache: false,
						            error: function (xhr) {
					            		errors.lo_output(xhr,'обновления профиля пользователя');
					            
					                //error_log("Ошибка AJAX при выполнении обновления настроек пользователя: " + xhr.statusText, +"(" + xhr.status + ")");
					            	}
						
	         }).responseText;        
	        
	        if (answer)
	            alert(answer);
	        else {
	            if (!quite)
	                base.reload_search();
	        }
	    },
        
        // Обновление поисковой формы
        update_search_form: function (text) {
            var form = $.ajax({
                url: st.self_net_path,
                data: 'task=show_search_form&form_mode=' + st.form_mode + '&search_mode=' + st.search_mode + '&debug_reporting=' + st.debug,
                cache: false,
                async: false,
                error: function (xhr) {
                    //error_log("Ошибка AJAX при загрузке новой поисковой формы: " + xhr.statusText, xhr.status);
                    errors.lo_output(xhr,'загрузки новой поисковой формы');
                }
            }).responseText;
            if ($('#form_div').html() != form) {
                form_transfer.save($('#form_div'));
                $('#form_div').html(form);

                // Исходное значение запроса -- пустой запрос. Записываем пустой запрос формы, чтобы избежать её самопроизвольного выполнения.
                search.set_to_default();
                base.clean_output();
                default_req_length = base.get_form_params().length;
                //search_form.init_start();
                search_form.autocomplete(search_form.init_finish);
            }
        },

        show_results_info: function () {
            if (search.recs_total > 0)
                var answer = 'Найдено документов:' + search.recs_total + '; Показаны документы с ' + search.first_number + ' по ' + (parseInt(search.first_number) + parseInt(search.recs_outputed) - 1);
            else
                var answer = 'Документы не найдены. ' + (st.search_fali_link ? st.search_fali_link : '');

            if (search.req)
                $("#req_description_cell").html(answer + '. <br>Запрос:&nbsp;<input class="request_field" type="text" value="' + search.req + '">');
        },


        progress_bar: function (percent, suffix) {
            if (percent == 0) {
                $("#progress" + suffix).html('');
                $("#progress" + suffix).progressbar("destroy");
                $("#progress" + suffix).progressbar();
            }
            else {
                $("#progress" + suffix).progressbar("value", percent);
            }
            //if (percent==0) alert('Null');
            $("#amount" + suffix).text(percent + "%");
        },


        pagination: function () {

            function pageselectCallback(page_index, jq) {
                // Если это всё равно первый номер и при этом установлен нулевой индекс.
                if (!page_index && search.first_number == 1) return false;
                search.first_number = page_index * search.portion_output + 1;
                base.new_page();

                /*page=new Array(search.search.first_number);
                 $("#pagination").trigger('setPage', page);*/
                return false;
            }

            var total_portions = Math.ceil(search.recs_total / search.portion_output);
            // Создаём только для первого номера и последнего запроса на выборку. Далее пэйджер работает автоматом.
            if (1 == 1) {
                $("#paginator").pagination(total_portions, {
                    callback: pageselectCallback,
                    prev_text: '<< ',
                    next_text: ' >>',
                    items_per_page: 1
                });
            }
        },
        search_indicator: function (enable) {
            if (enable)
                $('#loading').html("<img src='" + st.images_net_path + "/loading.gif' border=0>");
            else
                $('#loading').html("");
        },
        foging_results: function (enable) {
            if (enable)
                user_interface._fog_div.show();
            else
                user_interface._fog_div.hide();
        }

    };
    //=================================================================================================
    //=================================================================================================
    //=====================================/USER INTERFACE==============================================
    //=================================================================================================
    //=================================================================================================

    // Инициализация функция, ориентированных на обработку результатов. 
    function init_results() {
        $(".bo_tabs").tabs({
            
            load: function () {
                init_bo_tabs();
            },            
            ajaxOptions : {
            	cache: false,
				error: function( xhr ) {
			       errors.lo_output(xhr,'загрузки содержимого закладки');	
				
			    }            
            }
        });

        //-----------РАЗВОРАЧИВАНИЕ ЭЛЕМЕНТОВ БО----------------
        /*			$("button").button()
         .toggle(
         function(event) {
         //add_elements=;
         //alert('77777');
         // Вынужденная защита от троекратного срабатывания нажатия на кнопку. Разобраться.......
         //if ($('#'+$(this).val()).length)
         //return false;
         //	alert('nnnnnnnn');
         //	alert($(this).parent().find('div.add_elements').text());

         $(this).parent().find('div.add_elements').append("<span id='i"+$(this).val()+"'></span>");
         //alert(st.self_net_path+'?'+$(this).attr("title"))

         // Проблема: как загрузить данные во вновь созданный элемент??????? в IE это не работает.
         $('#i'+$(this).val()).load(st.self_net_path+'?'+$(this).attr("title"));



         },
         function(event) {
         $('#i'+$(this).val()).remove();
         }
         );



         */

        //-----------РАЗВОРАЧИВАНИЕ ЭЛЕМЕНТОВ БО----------------
        $("button").button()
            .each(function(){
                var toggle = false;
                $(this).click(function (event) {
                    var el = $(this);

                    if(toggle){
                        $('#a' + el.attr('id')).hide();
                    }else{
                        $('#a' + el.attr('id')).html("<br><img src='" + st.images_net_path + "/loading.gif' border=0>")
                            .show()
                            .load(st.self_net_path + '?' + el.attr("title"));
                    }
                    toggle = !toggle;
                });
            });
        //\-----------РАЗВОРАЧИВАНИЕ ЭЛЕМЕНТОВ БО----------------


        //-----------ПОКАЗ ОБЛОЖЕК----------------
        $.each($(".cover_img"), function () {
            //alert($(this).attr('title'));
            if ($(this).attr('title')) {
                $(this).attr('src', $(this).attr('title'));
                //$(this).attr('title',"Нажмите на иконку для подключения или редакции рисунка");
            }
        });
        //\-----------ПОКАЗ ОБЛОЖЕК----------------


        //-----------UPLOAD ДИАЛОГ----------------
        $('.upload_button').click(function () {
            $('#dialog').load(st.self_net_path + '?task=show_upload_form&' + $(this).attr('title'));
            $("#dialog").dialog({
                width: 400,
                height: 400,
                maxHeight: 500,
                buttons: {
                    "Закончить": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    $("#dialog").html('');
                    base.new_page();
                }
            });
        });

        //\-----------UPLOAD  ДИАЛОГ----------------


        //===============РАБОТА С ВЫБОРОМ ЗАПИСЕЙ=====================================================
        //$(".code_checkbox").unbind();
        $(".code_checkbox").change(function () {
            code_checkbox_submit(this.value, this.checked);
            add = (this.checked) ? 1 : -1;
            $("#selected_total").text(parseInt($("#selected_total").text()) + add);

        });

        function code_checkbox_submit(code, status) {


            user_settings = $.ajax({
                url: st.self_net_path,
                data: 'task=set_selected_records&code=' + code + '&record_selected=' + status,
                cache: false,
                success: function (answer) {
                    if (answer)
                        alert(answer);
                },
                error: function (xhr) {
                	errors.lo_output(xhr,'обновления списка выбранных записей');	                                	
                
                    //error_log("Ошибка AJAX при выполнении обновления списка выбранных записей: " + xhr.statusText, +"(" + xhr.status + ")");
                }

            });

        }


        //--------------ВЫВОД КОММЕНТАРИЕВ--------------------------------------------------
        // Оборачиваем переменные, что бы не передавать их замыканием в другие функции
        (function(){
            if (typeof(JComments) != 'function'  ) 
        		return;
        	// Создаем объект моудля комментариев
            window.jcomments = new JComments(1, 'com_books', st.ji_path_cms_net + '/index.php?option=com_jcomments&tmpl=component');

            var
            // Элементы управления для открытия комментариев
                cButtons = $('.c_get'),
            // Блоки для вставки комментариев
                cList = $('.c_list'),
            // Текущий открытый блок с комментариями
                cCurrent = false,
            // Прелоадер
                cLoader = $('<img src="'+st.images_net_path+'/ajax-loader.gif" class="comments_form_loader">'),
            // Обертка для блока комментариев
                cBlock = $('<div id="jc"><div id="comments"><div id="comments-list" class="comments-list"></div></div><div class="form" id="form"></div></div>');

            // Перебираем все блоки с комментариями
            cButtons.each(function(){
                var
                // id текущего слота
                    id = $(this).parent('.comments').attr('id'),
                // текущий блок для вставки комментария
                    myCList = $(this).next();

                // Обработчик клика
                $(this).click(function(){

                    // Очищаем все блоки с комментариями
                    cList.empty();

                    // Если нужно развернуть
                    if(cCurrent != id){
                        // Текущий блок открытый
                        cCurrent = id;

                        // Вставляем обертку
                        myCList.append(cBlock);

                        // Показываем комментарии, форму для добавления комментария
                        //jcomments.reinit(id, 'com_books');
                        $("#comments-form input[name=object_id]").val(id);
                        $("#comments-list").empty().append(cLoader);
                        jcomments.clear('comment');
                        jcomments.showForm(id, 'com_books', 'form');
                        jcomments.showCommentPhp(id, 'com_books');
                        jcomments.setList('comments-list');
                        jcomments.scrollTo(id);

                    }else cCurrent = false;
                });

            });
        })();
        //\--------------ВЫВОД КОММЕНТАРИЕВ--------------------------------------------------

        //--------------ВЫВОД РЕЙТИНГА--------------------------------------------------------
        $('.raeting').rating({
            fx: 'full',
            image: st.images_net_path + '/stars.png',
            loader: st.images_net_path + '/ajax-loader.gif',
            url: st.self_net_path + '?task=set_book_raeting',
            callback: function (responce) {
                this.vote_success.fadeOut(2000);
                this.vote_result.append(responce.votes);
            }
        });
        //\--------------ВЫВОД РЕЙТИНГА--------------------------------------------------------

        //===============/РАБОТА С ВЫБОРОМ=====================================================


    }


    function init_bo_tabs() {
        $(".ed_table").mouseover(function () {
            $(this).addClass('ui-state-highlight');
        });

        $(".ed_table").mouseout(function () {
            $(this).removeClass('ui-state-highlight');
        });


                //===============ПРОСМОТР PDF=====================================================
        $('.ed_file_pdf_view_cell').click(function (e) {
            var ed_window = window.open(st.self_net_path+'?task=ed_pdf_view&'+$(this).attr('title'));
            if (typeof(ed_window) == 'object')
                ed_window.focus();
    		
            e.stopPropagation();
            e.preventDefault();            
        });
        //===============ПРОСМОТР PDF=====================================================

        //===============ВЫГРУЗКА ЭЛЕКТРОННЫХ ВЕРСИЙ=====================================================
        $('.ed_table').click(function () {
            var ed_window = window.open(st.self_net_path+'?task=ed_download&'+$(this).attr('title'));
            if (typeof(ed_window) == 'object')
                ed_window.focus();
            
        });
        //===============ВЫГРУЗКА ЭЛЕКТРОННЫХ ВЕРСИЙ=====================================================

        //===============ПРЕДВАРИТЕЛЬНЫЙ ЗАКАЗ================
        $('.order_button').button();
        $('.order_button').click(function () {
            $.ajax({
                type: 'POST',
            	url: st.self_net_path,
                data: 'task=order&' + $(this).attr('title'),
                cache: false,                
                dataType: 'json',
                beforeSend: function () {
                    my_dialog.loading();
                },
                success: function (data) {
                    if (data.success) {
                        my_dialog.show("<div class=\"centred_dialog_message\">Заказ принят!</div>");
                    } else {
                        my_dialog.show(data.error);
                    }

                },
                error: function (xhr) {
                	errors.lo_output(xhr,'при выполнении заказа');                
                   // error_log("Ошибка AJAX : " + xhr.statusText, +"(" + xhr.status + ")");
                }

            });
        });
        //\===============ПРЕДВАРИТЕЛЬНЫЙ ЗАКАЗ================

    }


/*    var dialog = {
        loading: function () {
            $("#dialog").dialog({
                width: 100,
                //height: 100,
                title: 'Загрузка....'
            });

            $("#dialog").html("<img src='" + st.images_net_path + "/loading_green.gif' border=0><br><br>Загрузка данных...");
        },
        show: function (msg) {
            $("#dialog").dialog({
                width: 300,
                //height: inherit,
                resizable: true,
                title: 'Результат',
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });

            $("#dialog").html(msg);
        },
        destroy: function () {
            $("#dialog").dialog("destroy");
        }
    };*/

});