jQuery(function($){
    /**
     * FormTransfer
     * Перенос данных из одной формы в другую
     *
     * @return {*}
     * @constructor
     */
    window.FormTransfer = function(){
        var
            self = this,
            // Сохраненные данные из формы
            _data = {
                input: {},
                checkbox: {},
                select: {},
                jstree: {}
            },
            // Очистка сохраненных данных
            _clearData = function(){
                for(var prop in _data){
                    if(!_data.hasOwnProperty(prop)) continue;
                    _data[prop] = {}
                }
            };

        /**
         * Есть ли сохраненные данные
         * @type {boolean}
         */
        self.saved = false;

        /**
         * Сохранение данных формы
         * @param container
         * @return {*}
         */
        self.save = function(container){

            var self = this;

            //_clearData();

            // Текстовые поля
            container.find("input[type=text], textarea").each(function(){
                var jEl = $(this),
                    name = jEl.attr("name") || jEl.attr("id"),
                    value = jEl.val();

                if(!name) return;
                if(!value){
                    delete _data.input[name];
                }

                // Сохраняем
                _data.input[name] = value;
            });

            // Чекбоксы
            container.find("input[type=checkbox], input[type=radio]").each(function(){
                var jEl = $(this),
                    id = jEl.attr("id"),
                    value = jEl.prop("checked");

                if(!id) return;
                if(!value){
                    delete _data.checkbox[id];
                    return;
                }

                // Сохраняем
                _data.checkbox[id] = value;
            });

            // Селекты
            container.find("select").each(function(){
                var jEl = $(this),
                    name = jEl.attr("name") || jEl.attr("id"),
                    option = $(":selected", this),
                    value = "",
                    text = "";

                if(option.length){
                    value = option.attr("value");
                    text = option.text();
                }else{
                    name && delete _data.select[name];
                    return;
                }

                if(!name) return;

                // Сохраняем
                _data.select[name] = {value: value, text: text};
            });

            // Дерево jstree
            container.find(".jstree li").each(function(){
                var
                    jEl = $(this),
                    id = "_" + jEl.attr("id"),
                    value = "jstree-undetermined";

                if(id == "_") return;

                if(jEl.hasClass("jstree-unchecked")) value = "jstree-unchecked"; else
                if(jEl.hasClass("jstree-checked")) value = "jstree-checked";

                // Сохраняем
                _data.jstree[id] = value;

            });

            self.saved = true;

            return self;

        };

        /**
         * Вставка данных чекбоксов в форму
         * @param container
         * @return {*}
         */
        self.transferCheckboxes = function(container){

            if(!self.saved) return false;

            container.find("input[type=checkbox], input[type=radio]").each(function(){
                var jEl = $(this),
                    id = jEl.attr("id");

                if(!id || !(id in _data.checkbox)) return;

                // Вставляем
                jEl.prop("checked",_data.checkbox[id]);
            });

            return self;
        };

        /**
         * Вставка данных дерева в форму
         * @param container
         * @param jstree_node
         * @return {*}
         */
        self.transferJstree = function(container, jstree_node){

            if(!self.saved) return false;

            container.find(".jstree li").each(function(){
                var
                    jEl = $(this),
                    id = "_" + jEl.attr("id");

                if(!(id in _data.jstree)) return;

                if(_data.jstree[id] == "jstree-undetermined" && !jEl.hasClass(".jstree-open")){
                    jstree_node.jstree("open_node", jEl);
                }

                // Вставляем
                jEl.removeClass("jstree-unchecked jstree-checked jstree-undetermined").addClass(_data.jstree[id]);

            });

            return self;
        };

        /**
         * Вставка данных в форму
         * @param container
         * @param callback
         * @return {*}
         */
        self.transfer = function(container, callback){

            if(!self.saved) return false;

            // Текстовые поля
            container.find("input[type=text], textarea").each(function(){
                var jEl = $(this),
                    name = jEl.attr("name") || jEl.attr("id");

                if(!name || !(name in _data.input)) return;

                // Вставляем
                jEl.val(_data.input[name]);
            });

            // Чекбоксы
            self.transferCheckboxes(container);

            // Селекты
            container.find("select").each(function(){
                var jEl = $(this),
                    name = jEl.attr("name") || jEl.attr("id");

                if(!name || !(name in _data.select)) return;

                $(":selected", this).prop("selected",false);

                // Вставляем
                $("option[value='" + _data.select[name].value + "']", this).prop("selected",true);
            });

            callback && callback();

            return self;
        };

        return self;

    };
});