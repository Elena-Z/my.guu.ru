<?php

require_once 'Class.Institute.php';
require_once $ROOT_PATH . "/backend/core/student_group/Class.StudentGroup.php";


$__modules[Institute::MODULE_NAME] = array(
    'GET' => array(
        Institute::MODULE_NAME . '{/(id:[0-9]+)/student_groups}' => function ($id) use ($__user, $__db) {
            if ($__user->getStaffInstance()->getInstituteId() == $id || $__user->hasRights(array('module' => 'main', 'min_level' => $__user::SUPER_ADMIN_LEVEL))) {
                $institute = new Institute($id, $__db);
                return $institute->getStudentsGroups();
            } else
                throw new PrivilegesException('Not enough rights to access this information', $__db);
        },
        Institute::MODULE_NAME . '{/(id:[0-9]+)/student_group/(group_id:[0-9]+)}' => function ($id, $group_id) use ($__user, $__db) {
            if ($__user->getStaffInstance()->getInstituteId() == $id || $__user->hasRights(array('module' => 'main', 'min_level' => $__user::SUPER_ADMIN_LEVEL))) {
                $group = new StudentGroup($group_id, $__db);
                return $group->getInfo();
            } else
                throw new PrivilegesException('Not enough rights to access this information', $__db);
        },
        Institute::MODULE_NAME . '{/(id:[0-9]+)/student_group/(group_id:[0-9]+)/students}' => function ($id, $group_id) use ($__user, $__db) {
            if ($__user->getStaffInstance()->getInstituteId() == $id || $__user->hasRights(array('module' => 'main', 'min_level' => $__user::SUPER_ADMIN_LEVEL))) {
                $group = new StudentGroup($group_id, $__db);
                return $group->getConfidentialInfoStudentsList();
            } else
                throw new PrivilegesException('Not enough rights to access this information', $__db);
        },
        Institute::MODULE_NAME . '{/my/student_groups}' => function () use ($__user, $__db, $__args) {
            if ($institute_id = $__user->getStaffInstance()->getInstituteId()) {
                $institute = new Institute($institute_id, $__db);
                return $institute->getStudentsGroups();
            } else
                throw new LogicException('user has no institute');
        },
        Institute::MODULE_NAME . '{/my/student_group/(id:[0-9]+)/students}' => function ($group_id) use ($__user, $__db, $__args) {
            $student_group = new StudentGroup($group_id, $__db);
            $institute_id = $student_group->getInstitute()->getId();
            if ($institute_id == $__user->getStaffInstance()->getInstituteId()) {
                $group = new StudentGroup($group_id, $__db);
                return $group->getConfidentialInfoStudentsList();
            } else
                throw new LogicException('user has no institute');
        },
    ),
    //TODO
    /*'POST' => array(
        'my' => function() use ($__user, $__db, $__args, $__pg_db, $__request){
            if ($__args[0] == 'student'){
                $institute = new Institute($__user->getStaffInstance()->getInstituteId(), $__db);
                return $institute->updateStudentsPassport($__request, $__pg_db);
            }elseif ($__args[0] == 'error'){
                $institute = new Institute($__user->getStaffInstance()->getInstituteId(), $__db);
                return $institute->addErrorRequest($__request);
            }
        },
        'error' => function($__user, $__db, $__args, $__pg_db, $__request){
        }
    )*/
);