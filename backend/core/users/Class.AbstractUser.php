<?php

abstract class AbstractUser{

	public abstract function getId();
	public abstract function hasRights(array $min_rights);
}