<?php
/**
 * Created by PhpStorm.
 * User: Инал
 * Date: 18.10.2014
 * Time: 23:09
 */

class SUM {

	public static $DB_SERVER;
	public static $DB_USER;
	public static $DB_PASSWORD;
	public static $DB_NAME;
	public static $DOMAIN;
	public static $CANTEENS_SERVER_IP = '192.168.12.7';


	/*
	 * init config options for deploy and versioning
	 * */
	static function init(){
		$_SERVER['ENV'] = isset($_SERVER['ENV']) ? $_SERVER['ENV'] : 'prod';
		$filename = 'config.json';
		$counter = 0;
		if (file_exists($filename) == false){
			do{
				$filename = '../' . $filename;
				$counter++;
			}while(file_exists($filename) == false && $counter < 5);
		}
		$config_json = file_get_contents($filename);
		//$obj = str_replace('module.exports = ', '', $config_json);
		$obj = json_decode($config_json);
		self::$DB_NAME = $obj->$_SERVER['ENV']->db->database;
		self::$DB_SERVER = $obj->$_SERVER['ENV']->db->host;
		self::$DB_USER = $obj->$_SERVER['ENV']->db->user;
		self::$DB_PASSWORD = $obj->$_SERVER['ENV']->db->password;
		self::$DOMAIN = $obj->$_SERVER['ENV']->domain;
	}

/*Microsoft Azure settings*/
//IN node/server.js

static private $semester_id;

	static public function getCurrentSemesterId(PDO $db){
	if (is_numeric(self::$semester_id)){
		return self::$semester_id;
	}
	$q_get_sem = 'SELECT *
			FROM semesters
			WHERE semesters.active = 1';

	$p_sem = $db->prepare($q_get_sem);
	$p_sem->execute();
	if ($p_sem === FALSE) throw new DBQueryException('', $db);
	$result = $p_sem->fetch();
	self::$semester_id = $result['id'];
	return $result['id'];
}

	static public function getCurrentSemesterInfo(PDO $db){
	$q_get_sem = 'SELECT *
			FROM semesters
			WHERE semesters.active = 1';

	$p_sem = $db->prepare($q_get_sem);
	$p_sem->execute();
	if ($p_sem === FALSE) throw new DBQueryException('', $db);
	$result = $p_sem->fetch();
	return $result;
}
}