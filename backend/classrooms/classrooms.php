<?php

require_once 'Class.Classroom.php';


$__modules['classrooms'] = array(
    'GET' => array (
        //При запрашивании id, которого нет в базе, некорректный ответ
        '{id:[0-9]+}' => function( $id ) use ($__db) {
            $classroom = new Classroom( $id, $__db );
            return $classroom->getInfo();
        },
        //Если не заданы параметры, то ответ все строки таблицы
        //Можно изменить, чтобы достуные для изменения строки вытаскивались из базы данных
        'search' => function() use ($__db, $__request) {
            return Classroom::search($__db, $__request);
        }
    ),
    'POST' => array(
        '{id:[0-9]+}' => function( $id ) use ($__db, $__request) {
            $classroom = new Classroom( $id, $__db );
            $classroom->setAll($__request);
            //return $classroom->getInfo();
            return new Result(true);
        }
    ),

);