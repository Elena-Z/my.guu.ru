<?php

	require_once 'Class.MealGroup.php';
	require_once 'Class.Canteens.php';
	require_once 'Class.Meal.php';

class Canteen{

	const MODULE_NAME = 'canteens';
	private $db;
	private $id;
	private $name;
	private $user_name;

	public function __construct($id, PDO $db){
		$q_get_canteen = 'SELECT canteens.id, canteens.canteen_name,
			canteens.canteen_user_name, canteens.status
			FROM canteens
			WHERE id = :canteen_id
			AND status = 1';

		$p_get = $db->prepare($q_get_canteen);
		$p_get->execute(array(
			':canteen_id' => $id
		));
		if ($p_get === FALSE) throw new DBQueryException('', $db);
		if ($p_get->rowCount() != 1) throw new LogicException('Найдено слишком много ');

		$res = $p_get->fetch();

		$this->id = $res['canteen_id'];
		$this->name = $res['canteen_name'];
		$this->user_name = $res['canteen_user_name'];
		$this->db = $db;
	}


	public function getId(){
		return $this->id;
	}

	public function getInfo(){
		return new Result(true, '', array(
			'canteen_id' => $this->id,
			'canteen_name' => $this->name,
			'canteen_user_name' => $this->user_name,
			'canteen_update_time' => DBDate::toReadable(Canteens::getLastUpdateTime($this->db), true)
		));
	}

	public function getMealsInGroup($group_id){
		$group = new MealGroup($group_id, $this->db);
		return $group->getFullMenu($this);
	}

	public function getFullMenu(){
		$q_get_all = 'SELECT
			canteens.canteen_name,
			canteens_meal_groups.name as meal_group_name,
			canteens_meals.name as meal_name,
			canteens_exist_meals.meal_group_id,
			canteens_exist_meals.meal_id,
			canteens_exist_meals.canteen_id,
			canteens_meals_prices.price
		FROM canteens_exist_meals
		INNER JOIN canteens ON canteens.id = canteens_exist_meals.canteen_id
		INNER JOIN canteens_meal_groups ON canteens_meal_groups.id = canteens_exist_meals.meal_group_id
		INNER JOIN canteens_meals ON canteens_meals.id = canteens_exist_meals.meal_id
		INNER JOIN canteens_meals_prices ON canteens_meals_prices.canteen_exist_id= canteens_exist_meals.id
		WHERE canteens_meals_prices.status = 1
		AND canteens.id = :canteen_id';
		$p_get_all = $this->db->prepare($q_get_all);

		$p_get_all->execute(array(
			':canteen_id' => $this->id
		));

		return new Result(true, '', array('canteen' => $this->getInfo()->getData() , 'menu' => $p_get_all->fetchAll()));
	}

}