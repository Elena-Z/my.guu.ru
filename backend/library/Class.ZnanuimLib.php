<?php


class ZnaniumLib{

	const PRIVATE_KEY = '';


	public static function getAutoSignOnData(User $user){
		$timestamp = date( 'YmdHis' );
		return new Result(true, '', array(
			'link' => 'http://znanium.com/autosignon.php',
			'domain' => '*.guu.ru',
			'id' => $user->getId(),
			'login' => $user->getLogin(),
			'name' => $user->getFirstName(),
			'patr' => $user->getMiddleName(),
			'lname' => $user->getLastName(),
			'time' => $timestamp,
			'sign' => md5( $user->getId() . '783582a231907200275ab21fa4a96085' . $timestamp )
		));
	}

}