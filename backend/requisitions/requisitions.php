<?php
require_once 'Class.AbstractRequisitionsCollection.php';
require_once 'Class.RequisitionsToInstituteCollection.php';
require_once 'Class.AbstractRequisition.php';
require_once 'Class.RequisitionToInstitute.php';

$__modules[AbstractRequisitionsCollection::MODULE_NAME] = array(
	'GET' => array(
		'institute' => function() use ($__user, $__args, $__db){
			if ($__args[0] == 'all'){
				$req_col = new RequisitionsToInstituteCollection('all', $__user, $__db);
				return $req_col->get();
			}elseif ($__args[0] == 'my'){
				$req_col = new RequisitionsToInstituteCollection('my', $__user, $__db);
				return $req_col->get();
			}else{
				$inst_req = new RequisitionToInstitute($__args[0], $__user, $__db);
				return $inst_req->getFullInfo();
			}
		}
	),
	'PUT' => array(
		'institute' => function() use ($__args, $__request, $__user, $__db){
			$req = new RequisitionToInstitute($__args[0], $__user, $__db);
			if ($__args[1] == 'status'){
				return $req->setStatus($__request['status']);
			}
		}
	),
	'POST' => array(
		NULL => function() use ($__args, $__request, $__user, $__db){
			if ($__request['type'] == 'institute'){
				$instance = 'RequisitionToInstitute';
			}
			return call_user_func_array($instance.'::create', array(
				$__request, $__user, $__db
			));

		},
	)
);