<?php


	class Suggests{

		private $db;

		public function __construct(PDO $db){
			$this->db = $db;
		}

		private function normalizeResult($results, $col_name){
			$r = array();
			foreach($results as $result){
				$r[] = $result[$col_name];
			}
			return $r;
		}

		public function subjects($q){
			$q_get_subjects = 'SELECT subject_name
				FROM subjects
				INNER JOIN timetable ON subjects.subject_id=timetable.subject_id
				WHERE subject_name LIKE :q';
			$p_get_subjects = $this->db->prepare($q_get_subjects);
			$p_get_subjects->execute(array(
				':q' => ($q . '%')
			));
			return $this->normalizeResult($p_get_subjects->fetchAll(), 'subject_name');
		}



	}