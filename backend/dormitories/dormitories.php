<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 24.02.2015
 * Time: 20:38
 *
 *  api
 * get
 * +   my.guu.ru/api/dormitories/<id>
 * my.guu.ru/api/dormitories/sum
 * +   my.guu.ru/api/dormitories/search
 * return dorms id
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/<id>
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/search?{param=value[…]}
 * available parameters at $available
 * if none parameters return all records
 * my.guu.ru/api/dormitories/residents/statuses
 * my.guu.ru/api/dormitories/residents/student?{param=value[…]}
 *  all parameters required
 *   post
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/<id>?{param=value[…]}
 *  available parameters at $available
 * my.guu.ru/api/dormitories/{blocks | rooms | residents}/add?{param=value[…]}
 *  required parameters at $required
 *  available parameters at $available
 */


require_once 'Class.Dormitory.php';
require_once 'Class.DormBlock.php';
require_once 'Class.DormRoom.php';
require_once 'Class.DormResident.php';

$__modules[Dormitory::MODULE_NAME] = array(
    'GET' => array(
        Dormitory::MODULE_NAME.'/{(id:[0-9]+)}' => function ($id) use ($__db) {
            $dorm = new Dormitory($id, $__db);
            return $dorm->getInfo();
        },
        'blocks' => function () use ($__db, $__args, $__request) {
            if (is_numeric($__args[0])) {
                $id = intval($__args[0]);
                $dorm_block = new DormBlock($id, $__db);
                return $dorm_block->getInfo();
            } elseif ($__args[0] == 'search')
                return DormBlock::search($__db, $__request);
            elseif ($__args[0] == 'statuses')
                return DormBlock::getStatuses($__db);
            else
                return new Result(false, 'Invalid arguments');
        },
        'rooms' => function () use ($__db, $__args, $__request) {
            if (is_numeric($__args[0])) {
                $id = intval($__args[0]);
                $dorm_room = new DormRoom($id, $__db);
                return $dorm_room->getInfo();
            } elseif ($__args[0] == 'search')
                return DormRoom::search($__db, $__request);
            elseif ($__args[0] == 'statuses')
                return DormRoom::getStatuses($__db);
            else
                return new Result(false, 'Invalid arguments');
        },
        'residents' => function () use ($__db, $__args, $__request) {
            if (isset($__args[0])) {
                if (is_numeric($__args[0])) {
                    $id = intval($__args[0]);
                    $dorm_resident = new DormResident($id, $__db);
                    return $dorm_resident->getInfo();
                } elseif ($__args[0] == 'statuses')
                    return DormResident::getStatuses($__db);
                elseif ($__args[0] == 'search')
                    return DormResident::search($__db, $__request);
                elseif ($__args[0] == 'student')
                    return DormResident::searchStudent($__db, $__request);
                else
                    return new Result(false, 'Invalid arguments');
            } else
                return new Result(false, 'Invalid arguments');
        },
        'search' => function () use ($__db) {
            return Dormitory::search($__db);
        },
        'sum' => function () use ($__db) {
            return Dormitory::getSumInfo($__db);
        },
//        'test' => function () use ($__request, $__args, $__db) {
//            require_once 'update_residents.php';
//            return new Result(true, '');
//        }
    ),
    'POST' => array(
        'blocks' => function () use ($__db, $__args, $__request) {
            if (is_numeric($__args[0])) {
                $id = intval($__args[0]);
                $dorm_block = new DormBlock($id, $__db);
                return $dorm_block->setInfo($__request);
            } elseif ($__args[0] == 'add')
                return DormBlock::addBlock($__db, $__request);
            else
                return new Result(false, 'Invalid arguments');
        },
        'rooms' => function () use ($__db, $__args, $__request) {
            if (is_numeric($__args[0])) {
                $id = intval($__args[0]);
                $dorm_room = new DormRoom($id, $__db);
                return $dorm_room->setInfo($__request);
            } elseif ($__args[0] == 'add')
                return DormRoom::addRoom($__db, $__request);
            else
                return new Result(false, 'Invalid arguments');
        },
        'residents' => function () use ($__db, $__args, $__request) {
            if (is_numeric($__args[0])) {
                $id = intval($__args[0]);
                $dorm_resident = new DormResident($id, $__db);
                return $dorm_resident->setInfo($__request);
            } elseif ($__args[0] == 'add')
                return DormResident::addResident($__db, $__request);
            else
                return new Result(false, 'Invalid arguments');
        }
    )
);
