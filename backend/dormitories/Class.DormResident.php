<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 24.02.2015
 * Time: 20:36
 */

class DormResident {
    const MODULE_NAME = 'dormitories';
    private $db;
    private $id;

    private $first_name;
    private $middle_name;
    private $last_name;
    private $sex;
    private $phone_number;

    private $address;
//    private $country;
//    private $region;
//    private $district;
//    private $city;
//    private $street;
//    private $building;
//    private $flat;

    private $institute_name;
    private $institute_abbr;
    private $budget_form;
    private $edu_form_name;
    private $course;
    private $year;
    private $term;
    private $year_end;

    private $application_id;
    private $application_date;
    private $application_document;
    private $student_id;
    private $room_id;
    private $status_id;
    private $status_reason;
    private $distance_from_Moscow;
    private $is_paid;
    private $note;
    private $created_at;
    private $updated_at;

    private static $available = array(
        'application_id', 'application_date', 'application_document', 'student_id', 'room_id', 'status_id',
        'status_reason', 'distance_from_Moscow', 'note', 'is_paid'
    );
    private static $required = array(
        'student_id', 'application_date',
    );

    /**
     * @param PDO $db
     * @param int $id
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public function __construct($id, PDO $db) {
        $this->id = (int)$id;
        $this->db = $db;

        $q_get = 'SELECT
                      view_users.first_name, view_users.middle_name, view_users.last_name,
                      users.sex,
                      view_users.phone_number,
                      view_users.institute_name,
                      view_users.institute_abbr,
                      view_users.budget_form,
                      view_users.edu_form_name,
                      view_users.course,
                      view_users.year,
                      edu_terms.term,
                      (view_users.year + edu_terms.term) AS year_end,

                      addresses.id AS address_id,
                      addresses.country,
                      addresses.region,
                      addresses.district,
                      addresses.city,
                      addresses.street,
                      addresses.building,
                      addresses.flat,

                      dorm_residents.application_id, dorm_residents.application_date, dorm_residents.application_document,
                      dorm_residents.student_id, dorm_residents.room_id, dorm_residents.status_id, dorm_residents.status_reason,
                      dorm_residents.distance_from_Moscow, dorm_residents.is_paid,
                      dorm_residents.note, dorm_residents.created_at, dorm_residents.updated_at
                  FROM users
                      INNER JOIN view_users ON view_users.student_id = dorm_residents.student_id
                      INNER JOIN students ON students.user_id = users.id
                      INNER JOIN dorm_residents ON dorm_residents.student_id = students.id

                      INNER JOIN semesters_info ON semesters_info.student_id = dorm_residents.student_id
                      INNER JOIN edu_groups ON edu_groups.id = semesters_info.edu_group_id
                      INNER JOIN edu_forms ON edu_forms.id = edu_groups.edu_form_id
                      INNER JOIN edu_programs ON edu_programs.id = edu_groups.edu_program_id
                      INNER JOIN edu_specialties ON edu_specialties.id = edu_programs.edu_specialty_id
                      INNER JOIN edu_qualifications ON edu_qualifications.id = edu_specialties.edu_qualification_id
                      INNER JOIN edu_terms ON edu_terms.edu_form_id = edu_forms.id AND edu_terms.edu_qualification_id = edu_qualifications.id
                      INNER JOIN addresses ON addresses.id = view_users.residence_address_id
                  WHERE dorm_residents.id = :id';
        $p_get = $db->prepare($q_get);
        $p_get->execute(array(':id' => $id));
        if ($p_get === FALSE) throw new DBQueryException('Search student error', $db, 'Такой студент не найден');
        if (!$result = $p_get->fetch()) throw new InvalidArgumentException("No resident with id {$id}");

        $this->first_name = $result['first_name'];
        $this->middle_name = $result['middle_name'];
        $this->last_name = $result['last_name'];
        $this->sex = (int)$result['sex'];
        $this->phone_number = $result[''];

        $this->address['id'] = (int)$result['address_id'];
        $this->address['country'] = $result['country'];
        $this->address['region'] = $result['region'];
        $this->address['district'] = $result['district'];
        $this->address['city'] = $result['city'];
        $this->address['street'] = $result['street'];
        $this->address['building'] = $result['building'];
        $this->address['flat'] = $result['flat'];

        $this->institute_name = $result['institute_name'];
        $this->institute_abbr = $result['institute_abbr'];
        $this->budget_form = $result['budget_form'];
        $this->edu_form_name = $result['edu_form_name'];
        $this->course = (int)$result['course'];
        $this->year = (int)$result['year'];
        $this->term = (float)$result['term'];
        $this->year_end = (int)$result['year_end'];

        $this->application_id = (int)$result['application_id'];
        $this->application_date = $result['application_date'];
        $this->application_document = $result['application_document'];
        $this->student_id = (int)$result['student_id'];
        $this->room_id = (int)$result['room_id'];
        $this->status_id = (int)$result['status_id'];
        $this->status_reason = $result['status_reason'];
        $this->distance_from_Moscow = (int)$result['distance_from_Moscow'];
        $this->is_paid = (int)$result['is_paid'];
        $this->note = $result['note'];
        $this->created_at = $result['created_at'];
        $this->updated_at = $result['updated_at'];
    }

    /**
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'sex' => $this->sex,
            'phone_number' => $this->phone_number,

            'address' => $this->address,

            'institute_name' => $this->institute_name,
            'institute_abbr' => $this->institute_abbr,
            'budget_form' => $this->budget_form,
            'edu_form_name' => $this->edu_form_name,
            'course' => $this->course,
            'year' => $this->year,
            'term' => $this->term,
            'year_end' => $this->year_end,

            'application_id' => $this->application_id,
            'application_date' => $this->application_date,
            'application_document' => $this->application_document,
            'student_id' => $this->student_id,
            'room_id' => $this->room_id,
            'status_id' => $this->status_id,
            'status_reason' => $this->status_reason,
            'distance_from_Moscow' => $this->distance_from_Moscow,
            'is_paid' => $this->is_paid,
            'note' => $this->note,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ));
    }

    /**
     * @return Result
     * @param array $param
     * @throws InvalidArgumentException
     */
    public function setInfo($param) {
        foreach ($param as $key => $value) {
            if (in_array($key, self::$available))
                if ($key == 'room_id') {
                    $room = new DormRoom($value, $this->db);
                    if (!$room->isAvailable())
                        throw new InvalidArgumentException("Невозможно заселить студента в комнату {$value}", 'В комнате нет места');

                }
            is_numeric($value) ? $this->$key = (int)$value : $this->$key = $value;
        }
        return $this->update();
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    private function update() {
        $q_set = 'UPDATE dorm_residents SET
            application_id = :application_id, application_date = :application_date,
            application_document = :application_document, student_id = :student_id,
            room_id = :room_id, status_id = :status_id, status_reason = :status_reason,
            distance_from_Moscow = :distance_from_Moscow, is_paid = :is_paid, note = :note
            WHERE id = :id';
        $p_set = $this->db->prepare($q_set);

        $query_param = array();
        foreach (self::$available as $key) {
            $query_param[':' . $key] = $this->$key;
        }
        $query_param[':id'] = $this->id;

        $p_set->execute($query_param);
        if ($p_set === FALSE)
            throw new DBQueryException('UPDATE_ERROR', $this->db, 'Извините, произошла ошибка');
        else
            return new Result(true);
    }

    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     */
    public static function search($db, $request) {
        $q_get = 'SELECT view_users.first_name, view_users.middle_name, view_users.last_name, users.sex, view_users.phone_number,
              view_users.course, view_users.institute_name, view_users.institute_abbr, view_users.budget_form, view_users.edu_form_name,
              dorm_residents.student_id, dorm_residents.room_id, dorm_residents.status_id, dorm_residents.is_paid
              FROM users
                  INNER JOIN view_users ON view_users.student_id = dorm_residents.student_id
                  INNER JOIN students ON students.user_id = users.id
                  INNER JOIN dorm_residents ON dorm_residents.student_id = students.id';
        $first = TRUE;
        $search = array();
        foreach ($request as $param => $value)
            if (in_array($param, self::$available)) {
                if ($first) {
                    if(is_numeric($value))
                        $q_get .= " WHERE view_users.{$param} = :{$param}";
                    else
                        $q_get .= " WHERE view_users.{$param} LIKE :{$param}";
                    $first = FALSE;
                } else
                    if(is_numeric($value))
                        $q_get .= " AND view_users.{$param} = :{$param}";
                    else
                        $q_get .= " AND view_users.{$param} LIKE :{$param}";
                $search[':' . $param] = $value;
            }
        $p_get = $db->prepare($q_get);
        $p_get->execute($search);
        if ($p_get === FALSE)
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else
            $result = $p_get->fetchAll();
            if( $result ){
                foreach( $result as &$row )
                    foreach( $row as &$value )
                        if(is_numeric($value))
                            $value = (int)$value;
                return new Result('true', '', $result);
            }
            else
                return new Result(false, 'Не найдено');
    }

    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public static function searchStudent($db, $request) {
        $available = array(
            'first_name', 'middle_name', 'last_name',
            'course', 'institute_name', 'institute_abbr', 'budget_form', 'edu_form_name',
        );
        $q_get = 'SELECT view_users.student_id, view_users.first_name, view_users.middle_name, view_users.last_name,
              view_users.course, view_users.institute_name, view_users.institute_abbr, view_users.budget_form, view_users.edu_form_name,
              users.registration_address_id
            FROM view_users
              INNER JOIN users ON users.id = view_users.id
            WHERE view_users.is_student = 1';

        $query_param = array();
        foreach ($request as $param => $value)
            if (in_array($param, $available)) {
                if(is_numeric($value))
                    $q_get .= " AND view_users.{$param} = :{$param}";
                else
                    $q_get .= " AND view_users.{$param} LIKE :{$param}";
                $query_param[':' . $param] = $value;
            }
        $p_get = $db->prepare($q_get);
        if(count($query_param))
            $p_get->execute($query_param);
        else
            throw new InvalidArgumentException;
        if ($p_get === FALSE)
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else{
            $result = $p_get->fetchAll();
            if( count( $result ) != 0)
                foreach( $result as &$row )
                    foreach( $row as &$value )
                        if(is_numeric($value))
                            $value = (int)$value;
            return new Result('true', '', $result);
        }
    }

    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     */
    public static function addResident($db, $request) {
        $q_set_start = 'INSERT INTO dorm_residents (';
        $q_set_columns = 'student_id, application_date, created_at';
        $q_set_middle = ') VALUES (';
        $q_set_values = ':student_id, :application_date, NOW()';
        $q_set_end = ')';
        $query_param = array();

        $required_count = 0;
        foreach ($request as $param => $value) {
            if (in_array($param, self::$available) and !in_array($param, self::$required)) {
                $q_set_columns .= ', ' . $param;
                $q_set_values .= ', :' . $param;
                $query_param[':' . $param] = $value;
            } elseif (in_array($param, self::$required)) {
                $query_param[':' . $param] = $value;
                $required_count += 1;
            }
            //elseif (!in_array($param, self::$required))
            //  throw new InvalidArgumentException;
            //из-за _url нельзя вбросить исключение
        }
        if ($required_count != count(self::$required)) throw new InvalidArgumentException;
        $q_set = $q_set_start . $q_set_columns . $q_set_middle . $q_set_values . $q_set_end;
        $p_set = $db->prepare($q_set);
        $p_set->execute($query_param);

        if ($p_set === FALSE) throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        return new Result(true);
    }

    /**
     * @return Result
     * @param PDO $db
     * @throws DBQueryException
     */
    public static function getStatuses($db) {
        $q_get = 'SELECT id, resident_status FROM dorm_residents_statuses ORDER BY id';
        $p_get = $db->prepare($q_get);
        $p_get->execute();
        if ($p_get === FALSE) throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');

        $result = $p_get->fetchAll();
        if( !$result ) throw new LogicException('No statuses in db');
        foreach( $result as &$row )
            $row['id'] = (int)$row['id'];
        return new Result(true, "", $result);
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStudentId() {
        return $this->student_id;
    }

    /**
     * @return mixed
     */
    public function getStatusReason() {
        return $this->status_reason;
    }

    /**
     * @return mixed
     */
    public function getStatusId() {
        return $this->status_id;
    }

    /**
     * @return mixed
     */
    public function getRoomId() {
        return $this->room_id;
    }

    /**
     * @return mixed
     */
    public function getDistanceFromMoscow() {
        return $this->distance_from_Moscow;
    }

    /**
     * @return mixed
     */
    public function getApplicationId() {
        return $this->application_id;
    }

    /**
     * @return mixed
     */
    public function getApplicationDocument() {
        return $this->application_document;
    }

    /**
     * @return mixed
     */
    public function getApplicationDate() {
        return $this->application_date;
    }

    /**additional_info
     * @return mixed
     */
    public function getIsPaid() {
        return $this->is_paid;
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        return $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getMiddleName() {
        return $this->middle_name;
    }

    /**
     * @return mixed
     */
    public function getNote() {
        return $this->note;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber() {
        return $this->phone_number;
    }

    /**
     * @return mixed
     */
    public function getSex() {
        return $this->sex;
    }

    /**
     * @return mixed
     */
    public function getTerm() {
        return $this->term;
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        return $this->first_name;
    }

    /**
     * @return mixed
     */
    public function getInstituteName() {
        return $this->institute_name;
    }

    /**
     * @return mixed
     */
    public function getCourse() {
        return $this->course;
    }
}
