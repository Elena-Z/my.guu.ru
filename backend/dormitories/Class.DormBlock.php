<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 26.02.2015
 * Time: 20:09
 */

class DormBlock {
    const MODULE_NAME = 'dormitories';
    private $db;
    private $id;
    private $number;
    private $building_id;
    private $floor;
    private $status_id;
    private $rooms = array();
    private $places_count;
    private $places_count_by_room = array();
    private $places_reserved_count;
    private $places_reserved_count_by_room = array();
    private $residents_count;
    private $residents_count_by_room = array();
    private $places_available_count;
    private $places_available_count_by_room = array();
    private $residents = array();
    private $created_at;
    private $updated_at;

    private static $available = array(
        'number', 'building_id', 'floor', 'status_id',
    );

    /**
     * @param $id
     * @param PDO $db
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public function __construct( $id, PDO $db ) {
        $this->id = (int)$id;
        $this->db = $db;
        $q_get_block = 'SELECT dorm_blocks.number, dorm_blocks.building_id, dorm_blocks.floor, dorm_blocks.status_id, dorm_blocks.created_at, dorm_blocks.updated_at FROM dorm_blocks WHERE id = :id';

        $q_get_rooms = 'SELECT id FROM dorm_rooms WHERE block_id = :id';

        $q_get_residents = 'SELECT id FROM dorm_residents WHERE room_id = :id';

        $q_get_counts = 'SELECT dorm_blocks.id as d_bl_id, SUM(dorm_rooms.places_count) as places_count,
              SUM(places_reserved_count) as places_reserved_count,
              SUM(
                  (SELECT COUNT(*)
                   FROM dorm_residents
                     INNER JOIN dorm_rooms ON dorm_rooms.id = dorm_residents.room_id
                     INNER JOIN dorm_blocks ON dorm_blocks.id = dorm_rooms.block_id
                   WHERE dorm_blocks.id = d_bl_id)
              ) as residents_count
            FROM dorm_rooms
              INNER JOIN dorm_blocks ON dorm_blocks.id = dorm_rooms.block_id
            WHERE dorm_rooms.id = :id
            GROUP BY dorm_blocks.id';

        $p_get_block = $db->prepare($q_get_block);
        $p_get_block->execute(array(':id' => $id));
        if ($p_get_block === FALSE) throw new DBQueryException('', $db);

        if($res = $p_get_block->fetch())
            foreach ($res as $key => $value)
                is_numeric($value) ? $this->$key = (float)$value : $this->$key = $value;
        else
            throw new InvalidArgumentException;

        $p_get_rooms = $db->prepare($q_get_rooms);
        $p_get_rooms->execute(array(':id' => $id));
        if ($p_get_rooms === FALSE) throw new DBQueryException('', $db);

        if($res = $p_get_rooms->fetchAll())
            $this->rooms = $res;
//        else
//            throw new LogicException;
        foreach( $this->rooms as $room_num )
            foreach( $room_num as $key => $value)
                if( $key == 'id') {
                    $room = new DormRoom($value, $db);
                    $this->places_count_by_room[$value] = $room->getPlacesCount();
                    $this->places_reserved_count_by_room[$value] = $room->getPlacesReservedCount();
                    $this->residents_count_by_room[$value] = $room->getResidentsCount();
                    $this->places_available_count_by_room = $room->getPlacesAvailable();
                }


        foreach( $this->rooms as $room_num )
            foreach( $room_num as $key => $value)
                if ($key == 'id'){
                    $p_get_residents = $db->prepare($q_get_residents);
                    $p_get_residents->execute(array(':id' => $value));
                    if ($p_get_residents === FALSE) throw new DBQueryException('', $db);

                    if( $residents_id = $p_get_residents->fetchAll() )
                        foreach( $residents_id as $id ){
                            $resident = new DormResident($id, $db);
                            $this->residents[] = array(
                                'id' => $resident->getId(),
                                'first_name' => $resident->getFirstName(),
                                'middle_name' => $resident->getMiddleName(),
                                'last_name' => $resident->getLastName(),
                                'sex' => $resident->getSex(),
                                'institute_name' => $resident->getInstituteName(),
                                'phone_number' => $resident->getPhoneNumber(),
                                'course' => $resident->getCourse(),
                                'application_document' => $resident->getApplicationDocument(),
                            );
                        }
                }

        $p_get_counts = $db->prepare($q_get_counts);
        $p_get_counts->execute(array(':id' => $id));
        if ($p_get_counts === FALSE) throw new DBQueryException('', $db);
        $res = $p_get_counts->fetch();
        foreach ($res as $key => $value)
            if ($key != 'd_bl_id') {
                is_numeric($value) ? $this->$key = (int)$value : $this->$key = $value;
            }
        $this->places_available_count = $this->places_count - $this->residents_count - $this->places_reserved_count;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBuildingId() {
        return $this->building_id;
    }

    /**
     * @return mixed
     */
    public function getFloor() {
        return $this->floor;
    }

    /**
     * @return mixed
     */
    public function getPlacesAvailable() {
        return $this->places_available_count;
    }

    /**
     * @return mixed
     */
    public function getPlacesReserved() {
        return $this->places_reserved_count;
    }

    /**
     * @return Result
     */
    public function getResidents() {
        return new Result('true', '', $this->residents);
    }

    /**
     * @return Result
     */
    public function getRooms() {
        return new Result('true', '', $this->rooms);
    }


    /**
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'number' =>$this->number,
            'building_id' => $this->building_id,
            'floor' => $this->floor,
            'status_id' => $this->status_id,
            'rooms' => $this->rooms,
            'places_count' => $this->places_count,
            'places_count_by_room' => $this->places_count_by_room,
            'places_reserved_count' => $this->places_reserved_count,
            'places_reserved_count_by_room' => $this->places_reserved_count_by_room,
            'residents_count' => $this->residents_count,
            'residents_count_by_room' => $this->residents_count_by_room,
            'places_available_count' => $this->places_available_count,
            'places_available_count_by_room' => $this->places_available_count_by_room,
            'residents' => $this->residents,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ));
    }

    /**
    * @return Result
    * @param array $param
    */
    public function setInfo( $param ) {
        foreach ( $param as $key => $value ){
            if( in_array($key, self::$available ))
                is_numeric($value) ? $this->$key = (int)$value : $this->$key = $value;
        }
        return $this->update();
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    private function update() {
        $q_set = 'UPDATE dorm_blocks SET building_id = :building_id, floor = :floor, number = :number, status_id = :status_id WHERE id = :id';
        $p_set = $this->db->prepare($q_set);

        $query_param = array();
        foreach (self::$available as $key){
            $query_param[':' . $key] = $this->$key;
        }
        $query_param[':id'] = $this->id;

        $p_set->execute($query_param);
        if( $p_set === FALSE ) throw new DBQueryException('UPDATE_ERROR', $this->db, 'Извините, произошла ошибка');
        return new Result(true);
    }

    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     */
    public static function search( $db, $request ){
        $q_get = 'SELECT * FROM dorm_blocks';
        $first = TRUE;
        $search = array();
        foreach ($request as $param => $value)
            if (in_array($param, self::$available)) {
                if($first) {
                    $q_get .= " WHERE {$param} = :{$param}";
                    $first = FALSE;
                }
                else
                    $q_get .= " AND {$param} = :{$param}";
                $search[':' . $param] = $value;
            }
        $p_get = $db->prepare($q_get);
        $p_get->execute($search);
        if( $p_get === FALSE )
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else{
            $result = $p_get->fetchAll();
            if( count( $result ) != 0){
                foreach( $result as &$row )
                    foreach( $row as &$value )
                        if(is_numeric($value))
                            $value = (int)$value;
                return new Result( 'true', '', $result );
            }
            else
                return new Result(false, 'Не найдено');
        }
    }

    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public static function addBlock($db, $request){
        $q_set = 'INSERT INTO dorm_blocks (number, building_id, floor, created_at)
            VALUES (:number, :building_id, :floor, NOW())';
        $p_set = $db->prepare($q_set);
        $query_param = array();

        $required_count = 0;
        foreach ($request as $param => $value)
            if (in_array($param, self::$available)) {
                $query_param[':' . $param] = $value;
                $required_count += 1;
            }
        if ($required_count != count(self::$available) ) throw new InvalidArgumentException;
        $p_set->execute($query_param);
        if( $p_set === FALSE )
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else
            return new Result(true);
    }

    /**
     * @param $db
     * @return Result
     * @throws DBQueryException
     */
    public static function getStatuses($db) {
        $q_get = 'SELECT id, room_status FROM dorm_rooms_statuses';
        $p_get = $db->prepare($q_get);
        $p_get->execute();
        if ($p_get === FALSE)
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else {
            $result = $p_get->fetchAll();
            if (count($result) != 0)
                foreach ($result as &$row)
                    foreach ($row as &$value)
                        if (is_numeric($value))
                            $value = (int)$value;
            return new Result(true, "", $result);
        }
    }
}