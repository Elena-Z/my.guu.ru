<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 18.03.2015
 * Time: 22:13
 */


/*
№ п/п;Дата подачи;ФИО;Пол;Институт;Курс;Телефон;Документ на заселение;Статус ;Основание;Основа обучения;Расстояние от Москвы (км);Место проживания;Особые отметки;Год поступления;Срок обучения;Год окончания;№ заявления;

$available = array(
            'first_name', 'middle_name', 'last_name',
            'course', 'institute_name', 'budget_form', 'edu_form_name',
        );
*/

//require_once "../core/bin/db.php";
global $ROOT_PATH;

if(!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $enc = 'utf-8') {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }
}
function ResidentsUpdate( $filename, $param )
{
    $status_types = array(
        'Заселён' => 1,
        'Заселен' => 1,
        'Нуждается' => 3,
        'Нарушитель' => 4,
        'Не пришел' => 5,
        'Отказ' => 2,
        'Отклонено'=> 2,
        'Отчислен' => 6,
        'Не пришла' => 5,
        'Не пришёл' => 5,
        'Не заселилась в установленный срок' => 5,
        'Иностранец' => 1,
    );
    if( $param )
        //2014-2015
        $index = array(
            'fio' => 2,

            'application_id' => 17,
            'application_date' => 1,
            'application_document' => 7,
            'status' => 8,
            'status_reason' => 9,
            'distance_from_Moscow' => 11,
            'note' => 13,

            'institute_abbr' => 4,
            'course' => 5,
            'budget_form' => 10,
        );
    else
        $index = array(
        'fio' => 2,

        'application_date' => 1,
        'application_document' => 7,
        'status' => 8,
        'status_reason' => 9,
        'note' => 13,

        'institute_abbr' => 4,
        'course' => 5,
        'budget_form' => 10,
        'edu_form_name' => 11,
    );


    global $ROOT_PATH;
    global $__db;
    $file = fopen( $ROOT_PATH."/backend/dormitories/" . $filename, "r");
    if( !$file ) return;
    $f_err = fopen($ROOT_PATH."/backend/dormitories/" . 'err_' . $filename, "w");
    $err = 0;
    $all = 0;
    $f_test = fopen($ROOT_PATH.'/backend/dormitories/test', "a");

    while (!feof($file)) {

        $rec = fgetcsv($file, 0, ';');
        //var_dump($rec);
        $fio = explode(' ', $rec[$index['fio']]);
        //var_dump($fio);
        $request = array();
        if( isset($fio[1]) )
            $request['first_name'] = ucfirst(strtolower(trim($fio[1])));
        if( isset($fio[2]) )
            $request['middle_name'] = ucfirst(strtolower(trim($fio[2])));
        if( isset($fio[0]) )
            $request['last_name'] = ucfirst(strtolower(trim($fio[0])));
        while( true ){
            $ab = DormResident::searchStudent($__db, $request );
            if( count( $ab->getData() ) != 1){
                //var_dump($request);
                //var_dump($ab->getData());
                if( isset( $rec[$index['course']] ) and !array_key_exists('course', $request)) {
                    $request['course'] = $rec[$index['course']];
                    continue;
                }
                elseif( isset( $rec[$index['institute_abbr']]) and !array_key_exists('institute_abbr', $request) ){
                    $request['institute_abbr'] = trim($rec[$index['institute_abbr']]);
                    continue;
                }/*
                elseif( isset( $rec[$index['budget_form']]) and !array_key_exists('budget_form', $request) ){
                    $request['budget_form'] = $rec[$index['budget_form']];
                    continue;
                }*/
                else{
                    //var_dump($rec);
                    fputcsv($f_err, $rec, ';');
                    $all++;
                    $err++;
                    break;
                }
            }
            else{
                if( $rec[$index['application_date']] !== ''){
                    $date = str_replace(',', '.', $rec[$index['application_date']]);
                    $date = explode('.', $date);
                    $date = $date[2] . '-' . $date[1] . '-' . $date[0];
                }
                else
                    $date = '2013-09-01';
                $app_id = ( array_key_exists('application_id', $index) ) ? (int)trim($rec[$index['application_id']]) : null;
                $distance = ( array_key_exists('distance_from_Moscow', $index) ) ? (int)trim($rec[$index['distance_from_Moscow']]) : null;
                $status_id = trim($rec[$index['status']]);
                $status_id = mb_ucfirst($status_id);
                $status_id = $status_types[$status_id];
                $params = array(
                    'student_id' => (int)$ab->getData()[0]['student_id'],
                    'application_id' => $app_id,
                    'application_date' => $date,
                    'application_document' => trim($rec[$index['application_document']]),
                    'status_id' => $status_id,
                    'status_reason' => trim($rec[$index['status_reason']]),
                    'distance_from_Moscow' => $distance,
                    'note' => trim($rec[$index['note']]),
                );
                //var_dump($params);
                DormResident::addResident($__db, $params);
                $all++;
                break;
            }
        }
    }
    fclose($file);
    fclose($f_err);

    fwrite( $f_test, $all);
    fwrite( $f_test, ' ');
    fwrite( $f_test, $err);
    fclose($f_test);
}


ResidentsUpdate( '2013-2014_not.csv', 0 );
ResidentsUpdate( '2013-2014_1.csv', 0 );
ResidentsUpdate( '2013-2014_2.csv', 0 );
ResidentsUpdate( '2014-2015.csv', 1 );