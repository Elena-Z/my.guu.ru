<?php

	try {
		if (!isset($_REQUEST['act'])) throw new InvalidArgumentException('Не указан метод для выполнения');

		$act = explode('.', $_REQUEST['act']);
		if (count($act) < 2) throw new InvalidArgumentException('Указано неправльное название метода');

		$http_method = $_SERVER['REQUEST_METHOD'];

		$class_name = $act[0];
		$method_name = $act[1];


		require_once "backend/core/bin/db.php";
		require_once "backend/core/users/Class.User.php";
		require_once "backend/core/users/Class.Staff.php";
		require_once "backend/core/users/Class.Student.php";

		$__user = new User($__db);

		$__page    = (isset($_REQUEST['page']))    ? (int) $_REQUEST['page']    : 0;
		$__length  = (isset($_REQUEST['length']))  ? (int) $_REQUEST['length']  : 10;
		$__user_id = (isset($_REQUEST['id'])) ? (int) $_REQUEST['id'] : 0;


		if (file_exists("backend/core/{$class_name}/{$class_name}.php")){
			require_once "backend/core/{$class_name}/{$class_name}.php";
		}else{
			require_once "backend/{$class_name}/{$class_name}.php";
		}
	}catch(Exception $e){
		$result = new Result(false, 'Ошибка! '. $e->getMessage());
	}


	if ((isset($result) && $result instanceof Result) || (isset($class_name) && $class_name == 'suggests')){
		echo $result;
	}else{
		echo new Result(false, 'Упс. Сервер не вернул никаких данных');
	}