# ALTER TABLE timetable_subjects ADD has_project BOOLEAN DEFAULT 0 NULL;
#
# DROP TABLE brs_project_marks;
# DROP TABLE brs_project_features;
# DROP TABLE brs_projects;
#
# CREATE TABLE `brs_project_features` (
#   `id`                   INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#   `timetable_subject_id` INT(11) UNSIGNED NOT NULL,
#   `name`                 TEXT,
#   `description`          TEXT,
#   `group_number`         INT(3) UNSIGNED  NOT NULL,
#   `max_rating`           INT(11)          NOT NULL,
#   `status`               TINYINT(1)       NOT NULL DEFAULT '1',
#   `created_at`           DATETIME         NOT NULL,
#   `updated_at`           TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   CONSTRAINT `brs_project_features_ibfk_1` FOREIGN KEY (`timetable_subject_id`) REFERENCES `timetable_subjects` (`id`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# CREATE TABLE `brs_project_marks` (
#   `id`          INT(11) UNSIGNED    NOT NULL AUTO_INCREMENT,
#   `feature_id`  INT(11) UNSIGNED    NOT NULL,
#   `student_id`  INT(11) UNSIGNED    NOT NULL,
#   `chance_type` ENUM('1', '2', '3') NOT NULL DEFAULT '1',
#   `chance_date` DATETIME                     DEFAULT NULL,
#   `created_at`  DATETIME            NOT NULL,
#   `updated_at`  TIMESTAMP           NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `feature_id` (`feature_id`, `student_id`, `chance_type`),
#   CONSTRAINT `brs_project_marks_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `brs_project_features` (`id`),
#   CONSTRAINT `brs_project_marks_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# CREATE TABLE brs_projects_themes (
#   `id`                 INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#   timetable_subject_id INT(11) UNSIGNED NOT NULL,
#   `student_id`         INT(11) UNSIGNED NOT NULL,
#   theme                TEXT,
#   `created_at`         DATETIME         NOT NULL,
#   `updated_at`         TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY (`timetable_subject_id`, `student_id`),
#   FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# CREATE TABLE `dorm_rooms_statuses` (
#   `id`          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
#   `room_status` VARCHAR(255)     NOT NULL DEFAULT '',
#   `created_at`  DATETIME         NOT NULL,
#   `updated_at`  TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `room_status` (`room_status`)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
# ALTER TABLE `dorm_rooms` DROP `status`;
# ALTER TABLE `dorm_rooms` ADD `status_id` INT(11) UNSIGNED NULL
# AFTER places_reserved_count;
# ALTER TABLE `dorm_rooms` ADD CONSTRAINT `dorm_rooms_fk_2` FOREIGN KEY (`status_id`) REFERENCES `dorm_rooms_statuses` (`id`);
#
# ALTER TABLE `dorm_blocks` ADD `status_id` INT(11) UNSIGNED NULL
# AFTER floor;
# ALTER TABLE `dorm_blocks` ADD CONSTRAINT `dorm_blocks_fk_2` FOREIGN KEY (`status_id`) REFERENCES `dorm_rooms_statuses` (`id`);
#
# ALTER TABLE brs_project_marks ADD mark FLOAT DEFAULT NULL  NULL;
#
# ALTER TABLE brs_control_events ADD FOREIGN KEY (professor_id) REFERENCES staff (id);
#
# ALTER TABLE users_roles ADD main_role BOOLEAN DEFAULT TRUE  NOT NULL;
#
# DROP VIEW view_users;
#
# CREATE VIEW `view_users` AS
#   SELECT
#     `users`.`id`                                  AS `id`,
#     `users`.`first_name`                          AS `first_name`,
#     `staff`.`1c_id`                               AS `1c_id`,
#     `users`.`ad_login`                            AS `ad_login`,
#     `users`.`middle_name`                         AS `middle_name`,
#     `users`.`last_name`                           AS `last_name`,
#     `roles`.`name`                                AS `role`,
#     `roles`.`ou_id`                               AS `ou_id`,
#     `users`.`active`                              AS `active`,
#     if(isnull(`staff`.`id`), 0, 1)                AS `is_staff`,
#     if(isnull(`students`.`id`), 0, 1)             AS `is_student`,
#     if(isnull(`view_professors`.`user_id`), 0, 1) AS `is_professor`,
#     `students`.`begin_study`                      AS `begin_study`,
#     `students`.`id`                               AS `student_id`,
#     `students`.`personal_file`                    AS `personal_file`,
#     `students`.`phone_number`                     AS `phone_number`,
#     `view_edu_groups`.`academic_department_id`    AS `academic_department_id`,
#     `view_edu_groups`.`edu_specialty_id`          AS `edu_specialty_id`,
#     `view_edu_groups`.`profile`                   AS `profile`,
#     `view_edu_groups`.`profile_code`              AS `profile_code`,
#     `view_edu_groups`.`program`                   AS `program`,
#     `view_edu_groups`.`program_code`              AS `program_code`,
#     `view_edu_groups`.`institute_id`              AS `institute_id`,
#     `view_edu_groups`.`course`                    AS `course`,
#     `view_edu_groups`.`edu_form_id`               AS `edu_form_id`,
#     `view_edu_groups`.`edu_program_id`            AS `edu_program_id`,
#     `view_edu_groups`.`group`                     AS `group`,
#     `view_edu_groups`.`year`                      AS `year`,
#     `view_edu_groups`.`semester`                  AS `semester`,
#     `view_edu_groups`.`edu_form_name`             AS `edu_form_name`,
#     `view_edu_groups`.`edu_form_code`             AS `edu_form_code`,
#     `view_edu_groups`.`edu_qualification_id`      AS `edu_qualification_id`,
#     `view_edu_groups`.`edu_speciality_name`       AS `edu_speciality_name`,
#     `view_edu_groups`.`edu_speciality_code`       AS `edu_speciality_code`,
#     `view_edu_groups`.`edu_qualification_name`    AS `edu_qualification_name`,
#     `view_edu_groups`.`edu_qualification_code`    AS `edu_qualification_code`,
#     `view_edu_groups`.`institute_code`            AS `institute_code`,
#     `view_edu_groups`.`institute_abbr`            AS `institute_abbr`,
#     `view_edu_groups`.`institute_name`            AS `institute_name`,
#     `view_edu_groups`.`edu_group_id`              AS `edu_group_id`,
#     `semesters_info`.`budget_form`                AS `budget_form`,
#     `semesters_info`.`enrollment_date`            AS `enrollment_date`,
#     `staff`.`id`                                  AS `staff_id`,
#     `view_professors`.`professor_id`              AS `professor_id`
#   FROM ((((((((`users`
#     LEFT JOIN `staff` ON ((`users`.`id` = `staff`.`user_id`))) LEFT JOIN `users_roles`
#       ON ((`users`.`id` = `users_roles`.`user_id` AND users_roles.main_role = 1))) LEFT JOIN `roles`
#       ON ((`roles`.`id` = `users_roles`.`role_id`))) LEFT JOIN `organizational_units`
#       ON ((`organizational_units`.`id` = `roles`.`ou_id`))) LEFT JOIN `students`
#       ON ((`students`.`user_id` = `users`.`id`))) LEFT JOIN `view_professors`
#       ON ((`view_professors`.`user_id` = `users`.`id`))) LEFT JOIN `semesters_info`
#       ON ((`students`.`id` = `semesters_info`.`student_id`))) LEFT JOIN `view_edu_groups`
#       ON ((`view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`)))
#   WHERE users_roles.active = 1 OR students.id IS NOT NULL;
#
#
# DROP VIEW view_professors;
#
# CREATE VIEW `view_professors` AS
#   SELECT DISTINCT
#     `users`.`id` AS `user_id`,
#     `users`.`id` AS `professor_id`
#   FROM ((`staff`
#     JOIN `memberof` ON ((`memberof`.`user_id` = `staff`.`user_id`))) JOIN `users`
#       ON ((`users`.`id` = `staff`.`user_id`)))
#   WHERE (`memberof`.`group_id` = 2 OR `memberof`.`group_id` = 3);
#
#
# ALTER TABLE staff ADD courses_info TEXT DEFAULT NULL NULL;
# ALTER TABLE staff ADD education_info TEXT DEFAULT NULL NULL;
#
#
# CREATE TABLE sync_staff(
#   id INT(11) UNSIGNED AUTO_INCREMENT,
#   start_time DATETIME NULL,
#   finish_time DATETIME NULL,
#   errors_count INT UNSIGNED DEFAULT 0,
#   `created_at`  DATETIME         NOT NULL,
#   `updated_at`  TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (id)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;
#
#
# CREATE TABLE science_verifications(
#   id INT(11) UNSIGNED AUTO_INCREMENT,
#   verified BOOLEAN DEFAULT FALSE,
#   staff_id INT(11) UNSIGNED NOT NULL,
#   period_id INT(11) UNSIGNED NOT NULL,
#   verified_by_staff_id INT(11) UNSIGNED NOT NULL,
#   `created_at`  DATETIME         NOT NULL,
#   `updated_at`  TIMESTAMP        NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
#   PRIMARY KEY (id),
#   UNIQUE KEY (staff_id, period_id),
#   FOREIGN KEY (staff_id) REFERENCES staff(id),
#   FOREIGN KEY (period_id) REFERENCES science_periods(id),
#   FOREIGN KEY (verified_by_staff_id) REFERENCES staff(id)
# )
#   ENGINE = InnoDB
#   DEFAULT CHARSET = utf8;;
#
#ABOVE IS DONE ON 01.04.2015

ALTER TABLE groups ADD only_manually BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE memberof ADD added_manually BOOLEAN DEFAULT FALSE  NOT NULL;
ALTER TABLE academic_departments ADD additional_name TEXT DEFAULT NULL NULL;

UPDATE academic_departments
SET additional_name = 'Кафедра государственного и муниципального управления'
WHERE id = 18;
UPDATE academic_departments
SET additional_name = 'Кафедра мировой экономики'
WHERE id = 139;
UPDATE academic_departments
SET additional_name = 'Кафедра упр-ния в сфере культуры,кино,телевидения и инд.развлечений'
WHERE id = 121;
UPDATE academic_departments
SET additional_name = 'Кафедра управления инновациями'
WHERE id = 152;
UPDATE academic_departments
SET additional_name = 'Кафедра экономики труда и управления социально-экономическими отнош.'
WHERE id = 149;
UPDATE academic_departments
SET additional_name = 'Кафедра управления природопользованием и экологической безопасностью'
WHERE id = 14;

ALTER TABLE timetable CHANGE room room VARCHAR(50);
ALTER TABLE timetable MODIFY room VARCHAR(50) DEFAULT NULL;
ALTER TABLE timetable MODIFY COLUMN room VARCHAR(50) NULL;

ALTER TABLE subjects MODIFY COLUMN name VARCHAR(250) NOT NULL;

DROP VIEW view_users;
CREATE VIEW `view_users` AS
  SELECT DISTINCT
    `users`.`id`                                  AS `id`,
    `users`.`first_name`                          AS `first_name`,
    `staff`.`1c_id`                               AS `1c_id`,
    `users`.`ad_login`                            AS `ad_login`,
    `users`.`middle_name`                         AS `middle_name`,
    `users`.`last_name`                           AS `last_name`,
    `roles`.`name`                                AS `role`,
    `roles`.`ou_id`                               AS `ou_id`,
    `users`.`active`                              AS `active`,
    if(isnull(`staff`.`id`), 0, 1)                AS `is_staff`,
    if(isnull(`students`.`id`), 0, 1)             AS `is_student`,
    if(isnull(`view_professors`.`user_id`), 0, 1) AS `is_professor`,
    `students`.`begin_study`                      AS `begin_study`,
    `students`.`id`                               AS `student_id`,
    `students`.`personal_file`                    AS `personal_file`,
    `students`.`phone_number`                     AS `phone_number`,
    `view_edu_groups`.`academic_department_id`    AS `academic_department_id`,
    `view_edu_groups`.`edu_specialty_id`          AS `edu_specialty_id`,
    `view_edu_groups`.`profile`                   AS `profile`,
    `view_edu_groups`.`profile_code`              AS `profile_code`,
    `view_edu_groups`.`program`                   AS `program`,
    `view_edu_groups`.`program_code`              AS `program_code`,
    `view_edu_groups`.`institute_id`              AS `institute_id`,
    `view_edu_groups`.`course`                    AS `course`,
    `view_edu_groups`.`edu_form_id`               AS `edu_form_id`,
    `view_edu_groups`.`edu_program_id`            AS `edu_program_id`,
    `view_edu_groups`.`group`                     AS `group`,
    `view_edu_groups`.`year`                      AS `year`,
    `view_edu_groups`.`semester`                  AS `semester`,
    `view_edu_groups`.`edu_form_name`             AS `edu_form_name`,
    `view_edu_groups`.`edu_form_code`             AS `edu_form_code`,
    `view_edu_groups`.`edu_qualification_id`      AS `edu_qualification_id`,
    `view_edu_groups`.`edu_speciality_name`       AS `edu_speciality_name`,
    `view_edu_groups`.`edu_speciality_code`       AS `edu_speciality_code`,
    `view_edu_groups`.`edu_qualification_name`    AS `edu_qualification_name`,
    `view_edu_groups`.`edu_qualification_code`    AS `edu_qualification_code`,
    `view_edu_groups`.`institute_code`            AS `institute_code`,
    `view_edu_groups`.`institute_abbr`            AS `institute_abbr`,
    `view_edu_groups`.`institute_name`            AS `institute_name`,
    `view_edu_groups`.`edu_group_id`              AS `edu_group_id`,
    `semesters_info`.`budget_form`                AS `budget_form`,
    `semesters_info`.`enrollment_date`            AS `enrollment_date`,
    `staff`.`id`                                  AS `staff_id`,
    `view_professors`.`professor_id`              AS `professor_id`
  FROM `users`
    LEFT JOIN `staff` ON `users`.`id` = `staff`.`user_id` LEFT JOIN `users_roles`
      ON `users`.`id` = `users_roles`.`user_id` LEFT JOIN `roles`
      ON `roles`.`id` = `users_roles`.`role_id` LEFT JOIN `organizational_units`
      ON `organizational_units`.`id` = `roles`.`ou_id` LEFT JOIN `students`
      ON `students`.`user_id` = `users`.`id` LEFT JOIN `view_professors`
      ON `view_professors`.`user_id` = `users`.`id` LEFT JOIN `semesters_info`
      ON `students`.`id` = `semesters_info`.`student_id` LEFT JOIN `view_edu_groups`
      ON `view_edu_groups`.`edu_group_id` = `semesters_info`.`edu_group_id`
  WHERE (`users_roles`.`active` = 1 AND users_roles.main_role = 1) OR (`students`.`id` IS NOT NULL);