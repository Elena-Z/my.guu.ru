function InstituteStudents(_url){

	var _page = Page(_url),
		ALL_INPUTS_TO_FILL = 13,
		agreements = [
			{text: 'Не согласен', 'icon_class': 'cancel', 'btn_class': 'warning'},
			{text: 'Согласен', 'icon_class': 'check', 'btn_class': 'success'}
		]; // вызвать конструктор родителя, получить родительский объект;

	_page.settings = {
		url: '',
		data: {

		}
	};

	var _super_after_open = _page.afterOpen;

	_page.normalizeData = function(res){

	};


	function filterGroups(filter_type){
		var $table = $('#students-table'),
			$students = $table.find('.student-line');

		$students.show();
		if (filter_type == 'not_full'){
			$students.each(function(){
				var $this = $(this);
				if ($this.find('td.warning').length == 0){
					$this.hide();
				}
			});
		}else if (filter_type == 'full'){
			$students.each(function(){
				var $this = $(this);
				if ($this.find('td.info, td.success').length == 0){
					$this.hide();
				}
			});
		}
	}

	function sendErrorRequest(){
		$.ajax({
			url: '/api/institute/my/error',
			type: 'POST',
			data: {
				description: $('.description').val(),
				edu_group_id: $('.select2.groups').select2('val')
			},
			success: function(res){
				if (res.status == 1){
					$('.send-error-modal').modal('hide');
				}
				showNotifier(res);
			}
		});
	}

	_page.afterOpen = function(){

		$('.select2.forms,.select2.courses').on('change', function(){
			var course = $('select.courses').val(),
				program = $('select.forms').val();
			if (isNaN(parseInt(course)) || isNaN(parseInt(program))){
				return;
			}
			$.ajax({
				url: '/api/institute/my/student_groups/' + course + '/' + program,
				success: function(res){
					if (!res.status) return;
					var groups_opts = ['<option></option>'],
						$select = $('select.groups');
					$('.btn-show-group')
						.removeClass('active');
					res.data.forEach(function(value){
						if (value.program != null){
							value.normal_name = value.program + ' ' + value.course + '-' + value.group;
						}else if (value.profile != null){
							value.normal_name = value.profile + ' ' + value.course + '-'  + value.group;
						}else{
							value.normal_name = value.training_direction + ' ' + value.course + '-'  + value.group;
						}
						groups_opts.push(tmpl('students-group-option', value).get(0).outerHTML);
					});

					$select
						.select2('destroy')
						.html(groups_opts.join(''))
						.select2()
						.select2('enable')
						.on('change', function(e){
							$.ajax({
								url: '/api/institute/my/students_in_group/' + e.val,
								success: function(res){
									$('.btn-show-group').removeClass('active');
									var $st_table = $('#students-table').find('tbody'),
										$item;
									$st_table
										.empty();
									res.data.forEach(function(value, index){
										var required = ['last_name', 'first_name', 'birth_date', 'series',
											'number', 'issue_date' ,'issue_place', 'registration_address',
											'residence_address'],
											additional = ['doc_type_id', 'snils', 'email', 'phone_number'];
										value.list_number = index + 1;
										value.update_date = new Date(value.user_updated_at).getReadableText('genitive', true);
										value.birth_date = moment(new Date(value.birth_date)).format('DD/MM/YYYY');
										if (value.issue_date != '' && value.issue_date != null){
											value.issue_date = moment(new Date(value.issue_date)).format('DD/MM/YYYY');
										}
										value.filled = 0;


										value.agreement_text = !value.agreement ? agreements[0].text : agreements[1].text;
										value.agreement_icon_class = !value.agreement ? agreements[0].icon_class : agreements[1].icon_class;
										value.agreement_class = !value.agreement ? agreements[0].btn_class : agreements[1].btn_class;


										value.agreement_int = value.agreement ? 1 : 0;
										value.doc_type_id = (value.doc_type_id == null) ? value.doc_type_id : value.doc_type_id;
										required.forEach(function(attr_name){
											value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
										});

										additional.forEach(function(attr_name){
											value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
										});

										if (value.filled >= required.length && value.filled < ALL_INPUTS_TO_FILL){
											value.filled_class = 'info';
										}else if(value.filled == ALL_INPUTS_TO_FILL){ //13 - всего полей
											value.filled_class = 'success';
										}else{
											value.filled_class = 'warning';
										}

										value.filled_text = value.filled + '/' + ALL_INPUTS_TO_FILL;

										$item = tmpl('student-line', value);


										$item.find('.edit-student-info-btn').data('value', value).on('click', function(){
											var $this = $(this),
												value =  $this.data('value'),
												$modal_body = tmpl('edit-student-info', value);
											$modal_body.find('.select2.document').select2().select2('val', value.doc_type_id);

											if($.isFunction($.fn.datepicker)){
												$modal_body.find(".datepicker").each(function(i,el){
													var $this=$(el),opts={
														format: attrDefault($this,'format','dd/mm/yyyy'),
														startDate: attrDefault($this,'startDate',''),
														endDate: attrDefault($this,'endDate',''),
														daysOfWeekDisabled: attrDefault($this,'disabledDays',''),
														startView: attrDefault($this,'startView', 0),
														rtl:rtl()
													},
														$n=$this.next(),
														$p=$this.prev();
													$this.datepicker(opts);
													if($n.is('.input-group-addon')&&$n.has('a')){
														$n.on('click',function(ev){
															ev.preventDefault();$this.datepicker('show');
														});
													}
													if($p.is('.input-group-addon')&&$p.has('a')){
														$p.on('click',function(ev){
															ev.preventDefault();
															$this.datepicker('show');
														});
													}});
											}

											$modal_body.find('.copy-from-registration').on('click', function(){
												$modal_body
													.find('.residence')
													.each(function(){
														var $this = $(this);
														debugger;
														$this.val($modal_body.find('.registration.' + $this.data('type')).val());
													})
											});
											$modal_body.find('.student-agreement-btn').on('click', function(){
												var $this = $(this);
												if ($this.data('agreement') == 1){
													$this
														.removeClass('btn-success')
														.addClass('btn-warning')
														.html('<span>' + agreements[0].text + '</span><i class="entypo-' + agreements[0].icon_class +'"></i>')
														.data('agreement', 0)
												}else{
													$this
														.removeClass('btn-warning')
														.addClass('btn-success')
														.html('<span>' + agreements[1].text + '</span><i class="entypo-' + agreements[1].icon_class +'"></i>')
														.data('agreement', 1);
												}
											});
											$modal_body.find('.save-and-send').on('click', function(){

												var send_data = {
													user_id: value.user_id,
													student_id: value.user_id,
													doc_type_id: $modal_body.find('.document').select2('val'),
													agreement: $modal_body.find('.student-agreement-btn').hasClass('btn-success') ? 1 : 0
												}, new_value = {};


												$modal_body.find('input').each(function(){
													send_data[this.name] = this.value;
												});



												new_value = $.extend(value, send_data);

												send_data.issue_date = send_data.issue_date ? moment(send_data.issue_date, 'DD/MM/YYYY').format('YYYY-MM-DD') : '0000-00-00';
												send_data.birth_date = send_data.birth_date ? moment(send_data.birth_date, 'DD/MM/YYYY').format('YYYY-MM-DD') : '0000-00-00';


												new_value.country = send_data['registration-country'];
												new_value.region = send_data['registration-region'];
												new_value.district = send_data['registration-district'];
												new_value.city = send_data['registration-city'];
												new_value.street = send_data['registration-street'];
												new_value.building = send_data['registration-building'];
												new_value.flat = send_data['registration-flat'];


												new_value.res_country = send_data['residence-country'];
												new_value.res_region = send_data['residence-region'];
												new_value.res_district = send_data['residence-district'];
												new_value.res_city = send_data['residence-city'];
												new_value.res_street = send_data['residence-street'];
												new_value.res_building = send_data['residence-building'];
												new_value.res_flat = send_data['residence-flat'];

												new_value.agreement_text = !send_data.agreement ? agreements[0].text : agreements[1].text;
												new_value.agreement_icon_class = !send_data.agreement ? agreements[0].icon_class : agreements[1].icon_class;
												new_value.agreement_class = !send_data.agreement ? agreements[0].btn_class : agreements[1].btn_class;


												$.ajax({
													url: '/api/institute/my/student/' + value.user_id,
													type: 'POST',
													data: send_data,
													success: function(res){
														if (res.status == 1){
															var $st = $('.student-' + new_value.user_id);
															$('.edit-student-passport-info').modal('hide');
															new_value.filled = 0;
															required.forEach(function(attr_name){
																new_value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
															});

															additional.forEach(function(attr_name){
																new_value.filled += (value[attr_name] == null || value[attr_name] == '') ? 0 : 1;
															});

															if (value.filled >= required.length && value.filled < ALL_INPUTS_TO_FILL){
																value.filled_class = 'info';
															}else if(value.filled == ALL_INPUTS_TO_FILL){ //13 - всего полей
																value.filled_class = 'success';
															}else{
																value.filled_class = 'warning';
															}

															value.filled_text = value.filled + '/' + ALL_INPUTS_TO_FILL;
															$st.find('.update-date').text(new Date().getReadableText('genitive', true));
															$st.find('.filled')
																.text(new_value.filled+'/'+ALL_INPUTS_TO_FILL)
																.removeClass()
																.addClass('filled ' +  value.filled_class);
															$st.find('.agreement-td').html('<span>' + new_value.agreement_text
															+ '</span><i class="entypo-'+new_value.agreement_icon_class
																+'"></i>');
															$this.data('value', new_value);
														}
														showNotifier(res);
													}
												})
											});


											$('.edit-student-passport-info')
												.empty()
												.append($modal_body)
												.removeClass('hidden')
												.modal('show');

										});
										$st_table.append($item);
									});
								}
							});
						});
				}
			});
		});

		$('.btn-show-group').on('click', function(){
			var $btn = $(this);
			$btn.addClass('active');
			$btn.siblings().removeClass('active');
			filterGroups($btn.data('filter-type'))
		});

		$('.send-error-message').on('click', function(){
			var $modal_body = tmpl('error-modal', {});

			$modal_body.find('.send-error').on('click', sendErrorRequest);

			$('.send-error-modal')
				.empty()
				.append($modal_body)
				.removeClass('hidden')
				.modal('show');

		});
		_super_after_open.call(this);
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}