function Canteens(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '',
		data: {}
	};


	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){

		function renderMealsView(meals){
			var $meals_view = $('.meals-view'),
				counter = 0;
			$meals_view.find('.meals').each(function(){
				$row = $(this).empty();
				while ($row.find('.meal-item').length < 4 && meals[counter]){
					//tmpl('meal-item', meals[counter], $row);
					//console.log(meals[counter].meal_name);
					tmpl('meal-item', {
						meal_name: meals[counter].meal_name,
						price: meals[counter].price,
						img_num: counter + 1
					}, $row);
					counter++;
				}
			})
		}

		function updateMealsList(canteen_id, meal_group_id){
			canteen_id = isNaN(parseInt(canteen_id)) == false ? parseInt(canteen_id) : $('.canteen-item.active').data('canteen-id');
			meal_group_id = isNaN(parseInt(meal_group_id)) == false ? parseInt(meal_group_id) : $('.meal-group.active').data('group-id');
			$.ajax({
				url: '/api/canteens/' + canteen_id + '/meal_group/' + meal_group_id,
				success: function(res){
					$('.meals-view').data('meals', res.data.meals);
					$('.last-update-time').text('данные актуальны на ' + res.data.canteen.canteen_update_time);
					renderMealsView(res.data.meals);
				}
			})
		}

		$('.canteen-item').each(function(){
			var $this = $(this);
			$this.off('mouseup').on('mouseup', function(){
				var canteen_id = $this.data('canteen-id');
				$this.addClass('active').siblings().removeClass('active');
				$('.meal-groups')
					.removeClass('hidden');
				updateMealsList(canteen_id);
			})
		});
		$('.meal-group').off('mouseup').on('mouseup', function(){
			var meal_group_id = $(this).data('group-id');
			updateMealsList(null, meal_group_id);
		});
		_super_after_open.call(this);
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}