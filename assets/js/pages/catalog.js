function Catalog(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект;


	_page.settings = {
		url: '/api/users/stars',
		data: {}
	};
	var _super_after_open = _page.afterOpen;

	_page.normalizeData = function(res){
		var _html = $('<div>');
		res.data.forEach(function(value){
			value.sticker = value.is_student ? $('<span class="sticker label label-info">Студент</span>') : $('<span class="sticker label label-default">Сотрудник</span>');
			_html.append(tmpl('user-item', value));
		});
		return {stars_html: _html};
	};


	_page.afterOpen = function(){
		var $search_i = $('.search-input'),
			is_active = false,
			$members = $('.members');
		$search_i.on('keyup', function(e){
			if (e.keyCode == 13){
				is_active = false;
			}
			if (!is_active) {
				is_active = true;
				$.ajax({
					url: 'api/users/search/?q=' + this.value + '&group=' + $('[name=filter-users]:checked').val(),
					success: function(res) {
						$members.empty();
						res.data.forEach(function(elem,i){
							res.data[i].sticker = elem.is_student ? $('<span class="sticker label label-info">Студент</span>') : $('<span class="sticker label label-default">Сотрудник</span>');
						});
						tmpl('user-item', res.data, $members, 'append');
						is_active = false;
					}
				});
			}
		}).on('keydown', function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		$('.btn-filter-profiles').on('click', function(){
			$search_i.keyup();
		});
		_super_after_open.call(this);
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}