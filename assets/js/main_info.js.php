<?php
require_once '../../backend/core/bin/db.php';
require_once '../../backend/core/users/Class.User.php';
$user = new User($__db);
?>

window.getUserInfo = function(){ return <?=json_encode($user->getAuthInfo())?>;};

window.getBRSInfo = function(){ return <?=json_encode(SUM::getCurrentSemesterInfo($__db))?>;};
